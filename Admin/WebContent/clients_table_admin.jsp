<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashSet"%>
<%@page import="java.util.Set"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="model.*"%>

<jsp:useBean id="clientList" scope="request" class="model.ClientList" />


<div class="row" id="searchBar">
	<div class="col-md-3">
		<h2>Clients</h2>
	</div>
	<div class="col-md-9">
		<div id="searchDiv" style="margin-top: 20px">
			<p style="float: left; padding-top: 6px;">Search for:</p>
			<form style="float: left; padding-left: 20px;">
				<input type=text> <select class="selectpicker"
					style="margin-left: 10px">
					<option value="">Select filter</option>
					<option value="CNO" selected="selected">Client #</option>
					<option value="CN">Client Name</option>
					<option value="AD">Address</option>
				</select>
				<button style="margin-left: 10px;" class="btn btn-default">Search!</button>
			</form>
		</div>
	</div>
</div>

<div class="table-scrollbar">
	<table class="table table-stripped table-bordered text-center">
		<thead>
			<tr>
				<th><a href= "sortClient?mode=clientID&ascendent=true">Client ID</a></th>
				<th><a href= "sortClient?mode=name&ascendent=true">Name</a></th>
				<th><a href= "sortClient?mode=email&ascendent=true">Email</a></th>
				<th><a href= "sortClient?mode=address&ascendent=true">Address</a></th>
				<th>tools</th>
			</tr>
		</thead>
		<tbody>
			<%
			session.setAttribute("clientList", clientList);
				for (Client client : clientList) {
					String name="";
					name += client.getFirstName();
					name += ", " + client.getSecondName();
			%>
			<tr>
				<td><%=client.getId()%></td>
				<td><%=name %></td>
				<td><%=client.getEmail() %></td>
				<td><%=client.getAddress() %></td>
				<td> <a href="#delete_client_admin" data-toggle="modal">
						<button class="btn btn-default">
							<span class="glyphicon glyphicon-trash"></span> Delete
						</button>
				</a></td>
			</tr>
			<%
				}
			%>
		</tbody>
	</table>
</div>


<jsp:include page="delete_client_admin.jsp" />
