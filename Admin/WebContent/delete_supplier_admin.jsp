<div class = "modal fade" id = "delete_supplier_admin" role = "dialog">
                    <div class = "modal-dialog">
                        <div class = "modal-content">
                        <form class = "form-horizontal" method="POST" action="deleteSupplier">
                            <div class = "modal-header">
                                <h4>Delete supplier</h4>
                            </div>
                            <div class = "modal-body">
                                <p>Are you sure you want to delete the supplier?</p>
                            </div>
                            <div class = "modal-footer">
                                <a class = "btn btn-default" data-dismiss = "modal">Cancel</a>
                                <button class = "btn btn-primary" type = "submit">Delete</button>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>