<div class = "modal fade" id = "discount_product_admin" role = "dialog">
                    <div class = "modal-dialog">
                        <div class = "modal-content">
                        <form class = "form-horizontal" method="POST" action="discountProduct">
                            <div class = "modal-header">
                                <h4>Discount product</h4>
                            </div>
                            <div class = "modal-body">
                                Do you want to set the minimal price for this product?
                            </div>
                            <div class = "modal-footer">
                                <a class = "btn btn-default" data-dismiss = "modal">Cancel</a>
                                <button class = "btn btn-primary" type = "submit">Discount!</button>
                            </div>
                            </form>
                        </div>
                    </div>
</div>
