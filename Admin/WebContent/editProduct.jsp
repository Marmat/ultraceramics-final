<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashSet"%>
<%@page import="java.util.Set"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ page import="model.*"%>
<jsp:useBean id="productsuppliedList" scope="request" class="model.ProductsuppliedList" />
<%
	Productsupplied productSuppliedToEdit = (Productsupplied) session.getAttribute("productSuppliedToEdit");
%>

<html>
<head>
<title>Ultraceramics</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/styles.css" rel="stylesheet">
<link href="css/simple-sidebar.css" rel="stylesheet">
</head>
<body>

	<jsp:include page="/header.jsp" />

	<%
			String supplierID = (String) request.getParameter("supplierID");
			int ID = Integer.parseInt(supplierID);
			if(productSuppliedToEdit!=null) {
				
		
						%>
						
							<div class = "modal-dialog">
								<div class = "modal-content">
									<form class = "form-horizontal" method="POST" action="doEditProduct.html">
										<input id="productID" name="productID" type="hidden" value= <%=productSuppliedToEdit.getProduct().getIdProduct() %> >
										<input id="supplierID" name="supplierID" type="hidden" value= <%=productSuppliedToEdit.getSupplier().getId() %> >
										<input id="category" name="category" type="hidden" value= <%=productSuppliedToEdit.getProduct().getCategory().getName() %> >
										<div class = "modal-header">
											<h4>Edit product</h4>
										</div>
										<div class = "modal-body">
										
											<div class = "form-group">
												<div class = "col col-md-8 col-sm-8 col-xs-6">
													<label for = "description">Description:</label><br>
			                                        <p class="list-group-item"><%=productSuppliedToEdit.getProduct().getDescription() %></p>
												</div>
											</div>
											
											<div class = "form-group">
												<label for = "productName" class = "col-md-2 control-label">Product name:</label>
												<div class = "col-md-10">
													<p class="list-group-item"><%=productSuppliedToEdit.getProduct().getName() %></p>
												</div>
											</div>
									   
											<div class = "form-group">
												<label for = "productType" class = "col-md-2 control-label">Product type:</label>
												<div class = "col-md-4">
													<div class="list-group">
									                    <p class="list-group-item"><%=productSuppliedToEdit.getProduct().getCategory().getName() %></p>
									                </div>
												</div>
												<label for = "minPrice" class = "col-md-2 control-label">Min price:</label>
												<div class = "col-md-4">
													<p class="list-group-item"><%=productSuppliedToEdit.getMinPrice() %></p>
												</div>
											</div>
											
											<div class = "form-group">
												<label for = "price" class = "col-md-2 control-label">Price:</label>
												<div class = "col-md-4">
													<input id="sellPrice" name="sellPrice" placeholder=<%=productSuppliedToEdit.getSellPrice() %> class="form-control input-md" type="text">
												</div>
												<label for = "maxPrice" class = "col-md-2 control-label">Max price:</label>
												<div class = "col-md-4">
													<p class="list-group-item"><%=productSuppliedToEdit.getMaxPrice() %></p>
												</div>
											</div>
											
											<div class = "form-group">
												<label for = "quantity" class = "col-md-2 control-label">Quantity:</label>
												<div class = "col-md-4">
													<p class="list-group-item"><%=productSuppliedToEdit.getQuantity() %></p>
												</div>
											</div>
	
									   
										</div>
										<div class = "modal-footer">
											<% String link = "category.html?category="+productSuppliedToEdit.getProduct().getCategory().getName(); %>
											<a href = "products" class = "btn btn-default" data-dismiss = "modal">Cancel</a>    
											<button class = "btn btn-primary" type = "submit">Save</button>
										</div>
									</form>
								</div>
							</div>
						
						<%
						}	
		%>
					
					<script
		src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="js/bootstrap.js"></script>
</body>
</html>