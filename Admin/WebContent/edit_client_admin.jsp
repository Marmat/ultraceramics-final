<div class = "modal fade" id = "edit_client_admin" role = "dialog">					
					<div class = "modal-dialog">
							<div class = "modal-content">
								<form class = "form-horizontal" action="editClient" method="POST">
									<input type = "hidden" value = "register">
									<div class = "modal-header">
										<h4>Edit client</h4>
									</div>
									<div class = "modal-body">
										
										<div class = "form-group">
											<label for = "firstName" class = "col-lg-2 control-label">First name:</label>
											<div class = "col-lg-4">
												<input type = "text" class = "form-control" id = "firstName" placeholder = "John">
											</div>
											<label for = "lastName" class = "col-lg-2 control-label">Last name:</label>
											<div class = "col-lg-4">
												<input type = "text" class = "form-control" id = "lastName" placeholder = "Smith">
											</div>
										</div>
								   
										<div class = "form-group">
											<label for = "clientNumber" class = "col-lg-2 control-label">Client number:</label>
											<div class = "col-lg-4">
												<div class="list-group">
								                    <p class="list-group-item">1648863</p>
								                </div>
											</div>
											<label for = "clientEmail" class = "col-lg-2 control-label">Client email</label>
											<div class = "col-lg-4">
												<div class="list-group">
								                    <p class="list-group-item">client@example.com</p>
								                </div>
											</div>
										</div>
										
										<div class = "form-group">
											<label for = "clientAddress" class = "col-lg-2 control-label">Address:</label>
											<div class = "col-lg-10">
												<input type = "text" class = "form-control" id = "clientAddress" placeholder = "Calle de Some Street 10, Madrid">
											</div>
										</div>
								   
									</div>
									<div class = "modal-footer">
										<a class = "btn btn-default" data-dismiss = "modal">Cancel</a>    
										<button class = "btn btn-primary" type = "submit" >Save</button>
									</div>
								</form>
							</div>
					</div>
</div>					