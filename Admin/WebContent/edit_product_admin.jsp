<div class = "modal fade" id = "edit_product_admin" role = "dialog">					
					<div class = "modal-dialog">
							<div class = "modal-content">
								<form class = "form-horizontal" method="POST" action="editProduct">
									<input type = "hidden" value = "register">
									<div class = "modal-header">
										<h4>Edit product</h4>
									</div>
									<div class = "modal-body">
									
										<div class = "form-group">
											<div class = "col col-md-4 col-sm-4 col-xs-6">
												<a href = "#"><img src = "img/star.jpg" alt = "bowling" class = "pull-left img-thumbnail" /></a>
											</div>
											<div class = "col col-md-8 col-sm-8 col-xs-6">
												<label for = "description">Description:</label><br>
		                                        <textarea class = "form-control" rows = "4" placeholder = "Description of a 'Ceramic vase', blah, blah, blah..."></textarea>
											</div>
										</div>
										
										<div class = "form-group">
											<label for = "productName" class = "col-lg-2 control-label">Product name:</label>
											<div class = "col-lg-10">
												<input type = "text" class = "form-control" id = "productName" placeholder = "Ceramic vase">
											</div>
										</div>
								   
										<div class = "form-group">
											<label for = "productNumber" class = "col-lg-2 control-label">Product number:</label>
											<div class = "col-lg-4">
												<div class="list-group">
								                    <p class="list-group-item">16863</p>
								                </div>
											</div>
											<label for = "supplierNumber" class = "col-lg-2 control-label">Supplier number</label>
											<div class = "col-lg-4">
												<div class="list-group">
								                    <p class="list-group-item">135</p>
								                </div>
											</div>
										</div>
										
										<div class = "form-group">
											<label for = "price" class = "col-lg-2 control-label">Price:</label>
											<div class = "col-lg-4">
												<input type = "text" class = "form-control" id = "price" placeholder = "50 EUR">
											</div>
											<label for = "minPrice" class = "col-lg-2 control-label">Min price:</label>
											<div class = "col-lg-4">
												<input type = "text" class = "form-control" id = "minPrice" placeholder = "35 EUR">
											</div>
										</div>
									   
										
										
										<div class = "form-group">
											<div class = "col-lg-2 control-label"></div>
											<div class = "col-lg-4">
												<a href = "#discount_product_admin" data-toggle="modal"><button>Discount</button></a>
											</div>
											<label for = "maxPrice" class = "col-lg-2 control-label">Max price:</label>
											<div class = "col-lg-4">
												<input type = "text" class = "form-control" id = "maxPrice" placeholder = "100 EUR">
											</div>
										</div>
										

								   
									</div>
									<div class = "modal-footer">
										<a class = "btn btn-default" data-dismiss = "modal">Cancel</a>    
										<button class = "btn btn-primary" type = "submit">Save</button>
									</div>
								</form>
							</div>
					</div>
					
</div>					

<jsp:include page="discount_product_admin.jsp" />