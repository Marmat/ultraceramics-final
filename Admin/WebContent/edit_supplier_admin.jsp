<div class = "modal fade" id = "edit_supplier_admin" role = "dialog">					
					<div class = "modal-dialog">
							<div class = "modal-content">
								<form class = "form-horizontal" method="POST" action="editSupplier">
									<input type = "hidden" value = "register">
									<div class = "modal-header">
										<h4>Edit supplier</h4>
									</div>
									<div class = "modal-body">
										
										<div class = "form-group">
											<label for = "supplierName" class = "col-lg-2 control-label">Supplier name:</label>
											<div class = "col-lg-10">
												<input type = "text" class = "form-control" id = "productName" placeholder = "Spanish Ceramics">
											</div>
										</div>
								   
										<div class = "form-group">
											<label for = "supplierNumber" class = "col-lg-2 control-label">Supplier number</label>
											<div class = "col-lg-4">
												<div class="list-group">
								                    <p class="list-group-item">135</p>
								                </div>
											</div>
											<label for = "supplierEmail" class = "col-lg-2 control-label">Email:</label>
											<div class = "col-lg-4">
												<div class="list-group">
								                    <p class="list-group-item">name@example.com</p>
								                </div>
											</div>
										</div>
										
										
								   
									</div>
									<div class = "modal-footer">
										<a class = "btn btn-default" data-dismiss = "modal">Cancel</a>    
										<button class = "btn btn-primary" type = "submit">Save</button>
									</div>
								</form>
							</div>
					</div>
</div>					