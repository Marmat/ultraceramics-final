<%@page import="java.util.Calendar"%>
<%@page import="model.Product"%>
<%@page import="model.Productsupplied"%>
<%@page import="db.ProductManager"%>
<%@page import="model.PurchasePK"%>
<%@page import="java.util.Date"%>
<%@page import="model.Purchase"%>
<%@page import="java.util.List"%>
<%@page import="model.Order"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html>
<head>


<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<jsp:useBean id="orderList" scope="request" class="model.OrderList" />

<title>Ultraceramics</title>

</head>

<body>

	<jsp:include page="header.jsp"></jsp:include>

	<!-- Page Content -->
	<div class="container">

		<div class="row">
			<div class="col-md-2">
				<div class="row">
					<jsp:include page="searchOrder.jsp"></jsp:include>
				</div>
			</div>
		
			<%
				int total=0;
				ProductManager pm = new ProductManager();
				for (Order order : orderList) {
					List<Purchase> purchaseList = order.getPurchases();
					Date orderDate = order.getDate();
					String shipAdress = order.getShipAddress();
			%>	
			<div class="col-md-8">
			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			<div class="panel panel-default">
			<div class="panel-heading" role="tab" id="heading<%=order.getOrderID()%>">
				<h4 class="panel-title">
					<a data-toggle="collapse" data-partent="accordion" href="#collapse<%=order.getOrderID()%>" aria-expanded="false" aria-controls="collapse<%=order.getOrderID()%>">
						Order: <%=order.getDate()%>
					</a>
				</h4>
			</div>
			<div id="collapse<%=order.getOrderID()%>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<%=order.getOrderID()%>">
			<div class="panel-body">
			<div class="col-md-12 panel height:500px">
				<div class="panel-body" style="overflow: auto;">
					<p>Order details:</p>
					Date of the order:
					<%=orderDate.getDate() + "/" + orderDate.getMonth()
						+ "/" + orderDate.getYear()%>
					Shipping address:
					<%=shipAdress%>
					<div class="table-scrollbar">
						<table class="table table-stripped">
							<thead>
								<tr>
									<th>Product</th>
									<th>Supplier</th>
									<th>Amount</th>
									<th>Price</th>
								</tr>
							</thead>
							<tbody>
								<%
									for (Purchase purchase : purchaseList) {
											PurchasePK key = purchase.getId();
											int productID = key.getProductID();
											int supplierID = key.getSupplierID();
											Productsupplied productSupplied = pm.retriveProductFromIDs(
													productID, supplierID);
											Product product = productSupplied.getProduct();
											String supplierName = productSupplied.getSupplier()
													.getName();
								%>

								<tr>
									<td><%=product.getName()%></td>
									<td><%=supplierName%></td>
									<td><%=purchase.getQuantity()%></td>
									<td><%=purchase.getPrice()%>
								</tr>

								<%
									}
								%>

							</tbody>
						</table>
					</div>
					Total price:
					<%=order.getDiscountedPrice()%>
				</div>
			</div>
			</div>
			</div>
			</div>
			</div>
			</div>
			
			<%
				total += order.getDiscountedPrice();
				}
			%>




	
	</div>
	</div>
	<div class="container">
	<div class="row">
	<div class="col-md-2 col-md-offset-2">
	<span style="color: #D7DF01;" ><b> Total Earnings Month: <%=total %></b></span>
	</div>
	</div>
	</div>
	<jsp:include page="footer.jsp"></jsp:include>
</body>

</html>
