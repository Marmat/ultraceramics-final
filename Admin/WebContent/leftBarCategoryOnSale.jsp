<%@page import="model.Category"%>
<%@page import="java.util.List"%>
<%@page import="db.CategoryManager"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Ultraceramics</title>
</head>
<body>

            <div >
                <p class="lead">Categories:</p>
                <div class="list-group">
                
                <%
                	CategoryManager manager = new CategoryManager();
                	List<Category> categories = manager.getCategoryList();
                	for(Category category : categories){
                		String name = category.getName().toString();
                		String link = "categoryOnSale?category="+name;
                %>
                	
                    <a href=<%=link %> class="list-group-item" style="text-align:left"><%=name%></a>
                     <% 	} %>
                </div>
            </div>

</body>
</html>