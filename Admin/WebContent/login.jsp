<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/styles.css" rel="stylesheet">
<title>Administrator Login-Ultraceramics</title>
</head>

<body>
<div class="modal-dialog">
	<div class="modal-content">
		<form class="form-horizontal" method="POST" action="doLogin">
			<div class="modal-header">
				<h4>Administrator panel control</h4>
			</div>
			<div class="modal-body">
			<% 
				Boolean sessionError = false;
				try{
					sessionError = (Boolean) session.getAttribute("error");
				} catch(NullPointerException ex) {
					sessionError=false;
				}
				if(sessionError==null){
					sessionError=false;
				}
				if(sessionError){
			%>
				<p> Error </p>
			<% 
				}
			%>
				<div class="form-group">
					<label for="sign_in-login" class="col-lg-3 control-label">Login:</label>
					<div class="col-lg-9">
						<input type="text" name="username" class="form-control" id="sign_in-login" placeholder="j_smith">
					</div>
				</div>

				<div class="form-group">
					<label for="sign_in-password" class="col-lg-3 control-label">Password:</label>
					<div class="col-lg-9">
						<input type="password" name="password" class="form-control" id="sign_in-password" placeholder="password">
					</div>
				</div>

			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" type="submit">Sign in</button>
			</div>
		</form>
	</div>
</div>

<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="js/bootstrap.js"></script>
<jsp:include page="/footer.jsp" />
</body>
</html>