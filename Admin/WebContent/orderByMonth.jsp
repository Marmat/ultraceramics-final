<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="model.OrderList"%>
<%@page import="java.util.Calendar"%>
<%@page import="model.Product"%>
<%@page import="model.Productsupplied"%>
<%@page import="db.ProductManager"%>
<%@page import="model.PurchasePK"%>
<%@page import="java.util.Date"%>
<%@page import="model.Purchase"%>
<%@page import="java.util.List"%>
<%@page import="model.Order"%>
<jsp:useBean id="orderList" class="model.OrderList" scope="request"></jsp:useBean>

<body>
<jsp:include page="header.jsp"></jsp:include>
<div class="row">
	<div class="col-md-2 col-md-push-1">
		<div class="panel" style="margin-left:10px">
			<div class="panel-body">
				<div class="row">
					<form method="POST" action="orderByMonth">
						Month <input type="text" id="month" name="month" /> Year <input
							type="text" id="year" name="year" />
						<button class="btn btn-primary" type="submit">Search</button>
					</form>
					<a class="btn btn-success" href="#">See suppliers</a>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-8 col-md-push-1">
		<% if(orderList.size()>0){
			int month = (Integer) request.getAttribute("month");
			int year = (Integer) request.getAttribute("year");
		%>
			<p> Month: <%=month %>  Year: <%=year %> </p>
			<br>
		<%
		}
		%>
		<%
			ProductManager pm = new ProductManager();
			for (Order order : orderList) {
				List<Purchase> purchaseList = order.getPurchases();
				Date orderDate = order.getDate();
				String shipAdress = order.getShipAddress();
		%>
		<div class="col-md-12">

			<div class="panel-group" id="accordion" role="tablist"
				aria-multiselectable="true">
				<div class="panel panel-default">
					<div class="panel-heading" role="tab"
						id="heading<%=order.getOrderID()%>">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-partent="accordion"
								href="#collapse<%=order.getOrderID()%>" aria-expanded="false"
								aria-controls="collapse<%=order.getOrderID()%>"> Order: <%=order.getDate()%>
							</a>
						</h4>
					</div>
					<div id="collapse<%=order.getOrderID()%>"
						class="panel-collapse collapse" role="tabpanel"
						aria-labelledby="heading<%=order.getOrderID()%>">
						<div class="panel-body">
							<div class="col-md-12 panel height:500px">
								<div class="panel-body" style="overflow: auto;">
									<p>Order details:</p>
									Date of the order:
									<%=orderDate.getDate() + "/" + orderDate.getMonth()
						+ "/" + orderDate.getYear()%>
									Shipping address:
									<%=shipAdress%>
									<div class="table-scrollbar">
										<table class="table table-stripped">
											<thead>
												<tr>
													<th>Product</th>
													<th>Supplier</th>
													<th>Amount</th>
													<th>Price</th>
												</tr>
											</thead>
											<tbody>
												<%
													for (Purchase purchase : purchaseList) {
															PurchasePK key = purchase.getId();
															int productID = key.getProductID();
															int supplierID = key.getSupplierID();
															Productsupplied productSupplied = pm.retriveProductFromIDs(
																	productID, supplierID);
															Product product = productSupplied.getProduct();
															String supplierName = productSupplied.getSupplier()
																	.getName();
												%>

												<tr>
													<td><%=product.getName()%></td>
													<td><%=supplierName%></td>
													<td><%=purchase.getQuantity()%></td>
													<td><%=purchase.getPrice()%>
												</tr>

												<%
													}
												%>

											</tbody>
										</table>
									</div>
									Total price:
									<%=order.getDiscountedPrice()%>
									<p style="color: red">
										Earning from this order:
										<%=order.getAdminEarnings()%>€
									</p>
									<%
										if (order.getDiscount() != null) {
									%>
									<p style="color: green">
										Discounted of
										<%=order.calculateDiscount()%>€ with the discount card
										<%=order.getDiscount().getDiscountCode()%>
									</p>
									<%
										}
									%>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<%
			}
		%>





	</div>
	</div>
		<% if(orderList.size()>0){
			int month = (Integer) request.getAttribute("month");
			int year = (Integer) request.getAttribute("year");
		%>
			<div class="row">
			<div class="col-md-2 col-md-push-3">
			<a class="btn btn-ms btn-primary" href="doReconciliation?month=<%=month %>&year=<%=year %>">Reconciliate month</a>
			</div>
			</div>
		<%
		}
		%>
<jsp:include page="footer.jsp"></jsp:include>
</body>