<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashSet"%>
<%@page import="java.util.Set"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="model.*"%>

<jsp:useBean id="supplierList" scope="request" class="model.SupplierList" />
<jsp:useBean id="product" scope="request" class="model.Product" />
<jsp:useBean id="productsuppliedList" scope="request"
	class="model.ProductsuppliedList" />

<div class="row" id="searchBar">
	<div class="col-md-3">
		<h2>Suppliers selling: <%=productsuppliedList.get(0).getProduct().getName() %></h2>
	</div>
	<div class="col-md-9">
		<div id="searchDiv" style="margin-top: 20px">
			<p style="float: left; padding-top: 6px;">Search
					supplier:</p>
				<form style="float: left; padding-left: 20px;">
					<input type=text> 
					<button style="margin-left: 10px;" class="btn btn-primary">Search!</button>
				</form>
			</div>
		</div>
</div>		
		
	<div class="table-scrollbar" >
		<table class="table table-stripped table-bordered text-center">
			<thead>
				<tr>
					<th>Supplier</th>
					<th>Name</th>
					<th>Email</th>
					<th>Stock</th>
					<th>Sell Price</th>
					<th>Tools</th>
					
				</tr>
			</thead>
			<tbody>
				<%
					for (Productsupplied ps : productsuppliedList) {
						String supplierLink = "suppProducts?supplierID="
								+ ps.getSupplier().getId();
				%>
				<tr>
					<td><a href= <%=supplierLink %>><%=ps.getSupplier().getId() %></a></td>
					<td><a href= <%=supplierLink %>> <%=ps.getSupplier().getName() %></a></td>
					<td> <%=ps.getSupplier().getEmail()%></td>
					<td> <%=ps.getQuantity() %></td>
					<td><%=ps.getSellPrice() %><span class = "glyphicon glyphicon-euro"></span></td>
					<%
					String link = "?productID=" + ps.getProduct().getIdProduct() + "&supplierID=" + ps.getSupplier().getId();
					String deleteLink = "doDeleteProduct.html" + link;
					%>
					<td><a href=<%=deleteLink %> data-toggle="modal">
							<button class="btn btn-default" >
								<span class="glyphicon glyphicon-trash"></span> 
								Delete
								
							</button>
						</a> </td>
				</tr>
				<%
					}
				%>
			</tbody>
		</table>
	</div>


<jsp:include page="delete_product_admin.jsp" />