<html>
<head>
<title>Ultraceramics</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/styles.css" rel="stylesheet">
<link href="css/simple-sidebar.css" rel="stylesheet">
</head>
<body>

	<jsp:include page="/header.jsp" />

	<div class="container">
		<div class="row">

			<div class="col col-md-2">
				<jsp:include page="left_menu_products_on_sale_admin.jsp" />
			</div>
			<div class="col col-md-10">
				<jsp:include page="products_discounted_table_admin.jsp" />
			</div>

		</div>
	</div>
	<jsp:include page="/footer.jsp" />
	<script
		src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="js/bootstrap.js"></script>

</body>
</html>