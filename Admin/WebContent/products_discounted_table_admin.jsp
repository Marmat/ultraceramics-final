<%@page import="db.ProductManager"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashSet"%>
<%@page import="java.util.Set"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="model.*"%>

<jsp:useBean id="productsuppliedList" scope="request"
	class="model.ProductsuppliedList" />
	
<div class="row" id="searchBar">
	<div class="col-md-3">
		<h2>On sale</h2>
	</div>
	<div class="col-md-9">
		<div id="searchDiv" style="margin-top: 20px">
			<p style="float: left; padding-top: 6px;">Search
					for:</p>
				<form style="float: left; padding-left: 20px;">
					<input type=text> 
					<select class="selectpicker"
						style="margin-left: 10px">
						<option value="">Select filter</option>
						<option value="CNO" selected="selected">Client #</option>
						<option value="CN">Client Name</option>
						<option value="AD">Address</option>
					</select> 
					<button style="margin-left: 10px;" class="btn btn-default">Search!</button>
				</form>
			</div>
		</div>
</div>		
		
	<div class="table-scrollbar" >
		<table class="table table-stripped table-bordered text-center">
			<thead>
				<tr>
					<th><a href= "sortOnSale?mode=productID&ascendent=true">Product ID</a></th>
					<th><a href= "sortOnSale?mode=name&ascendent=true">Name</a></th>
					<th><a href= "sortOnSale?mode=supplierID&ascendent=true">Supplier ID</a></th>
					<th><a href= "sortOnSale?mode=price&ascendent=true">Price</a></th>
					<th><a href= "sortOnSale?mode=quantity&ascendent=true">Stock</a></th>
					<th>Tools</th>
				</tr>
			</thead>
			<tbody>
				<%
					session.setAttribute("productsuppliedList", productsuppliedList);
						for(Productsupplied ps : productsuppliedList){
							String link = "deleteDiscount?productID="+ ps.getId().getProductID();
							link += "&supplierID=" + ps.getId().getSupplierID();
							String productLink = "prodSuppliers?productID="
									+ ps.getId().getProductID();
							String supplierLink = "suppProducts?supplierID="
									+ ps.getId().getSupplierID();
							
						
				%>
					<tr>
					<td><a href=<%=productLink%>><%=ps.getId().getProductID()%></a></td>
					<td><a href=<%=productLink%>><%=ps.getProduct().getName() %></a></td>
					<td><a href=<%=supplierLink%>><%=ps.getId().getSupplierID()%></a></td>
					<td><%=ps.getSellPrice() %><span class="glyphicon glyphicon-euro"></span></td>
					<td><%=ps.getQuantity() %></td>
					<td><a href=<%=link %>
						data-toggle="modal"><button class="btn btn-default">
								<span class="glyphicon glyphicon-tag"></span> Delete discount!
							</button></a></td>
			
				</tr>
				<%
					}
				%>
			</tbody>
		</table>
	</div>

<jsp:include page="delete_product_discount_admin.jsp" />