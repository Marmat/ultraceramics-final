<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.HashMap"%>
<%@page import="db.ProductManager"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashSet"%>
<%@page import="java.util.Set"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="model.*"%>

<jsp:useBean id="productList" scope="request" class="model.ProductList" />
<jsp:useBean id="productsuppliedList" scope="request"
	class="model.ProductsuppliedList" />
	
<div class="row" id="searchBar">
	<div class="col-md-3">
		<h2>Products</h2>
	</div>
	<div class="col-md-9">
		<div id="searchDiv" style="margin-top: 20px">
			<p style="float: left; padding-top: 6px;">Search
					for:</p>
				<form style="float: left; padding-left: 20px;">
					<input type=text> 
					<select class="selectpicker"
						style="margin-left: 10px">
						<option value="">Select filter</option>
						<option value="CNO" selected="selected">Client #</option>
						<option value="CN">Client Name</option>
						<option value="AD">Address</option>
					</select> 
					<button style="margin-left: 10px;" class="btn btn-default">Search!</button>
				</form>
			</div>
		</div>
</div>		
		
	<div class="table-scrollbar" >
		<table class="table table-stripped table-bordered text-center">
			<thead>
				<tr>
					<th><a href= "sort?mode=productID&ascendent=true">Product ID</a></th>
					<th><a href= "sort?mode=name&ascendent=true">Name</a></th>
					<th><a href= "sort?mode=suppliers&ascendent=true"># of Suppliers</a></th>
					<th><a href= "sort?mode=price&ascendent=true">Price Range</a></th>
					<th><a href= "sort?mode=stock&ascendent=true">Stock</a></th>
					<th>Tools</th>
					<th>Profit Margin</th>
				</tr>
			</thead>
			<tbody>
			
			
				<%
		if (productList.size()!=0) {
			
			session.setAttribute("productList", productList);
			
			for (Product product : productList) {
		
				List<Productsupplied> supplied = new ArrayList<Productsupplied>();
				supplied = product.getProductsupplieds();
				if (!supplied.isEmpty()){
				double minimumPrice = Integer.MAX_VALUE;
				
				String quantity = null;
				double maxPrice = 0;
				int stock = 0;
				Set<Supplier> suppliers = new HashSet<Supplier>();  
				for (Productsupplied ps : supplied) {
					stock += Integer.parseInt(ps.getQuantity());
					double price = Double.parseDouble(ps.getSellPrice());
					if (price < minimumPrice) {
						minimumPrice = price;
					}
					if ( price > maxPrice) {
						maxPrice = price;
					}
					suppliers.add(ps.getSupplier());
					
				}
				quantity = stock + "";
				String range = "" + minimumPrice + "-" + maxPrice + "";
				
				if (suppliers.size()==1)
					range = minimumPrice + "";

				String productLink = "prodSuppliers?productID="
						+ product.getIdProduct();
		
	%>
				<tr>
					<td><a href=<%=productLink%>><%=product.getIdProduct()%></a></td>
					<td><a href=<%=productLink%>><%=product.getName()%></a></td>
					<td><a href=<%=productLink%>><%=supplied.size()%></a></td>
					<td><%=range%> <span class="glyphicon glyphicon-euro"></span></td>
					<td><%=quantity %></td>
					<td>
						<%
									String link = "?productID=" + product.getIdProduct() + "&supplierID=" + product.getProductsupplieds().get(0).getSupplier().getId(); //+ bestSupplier.getId();
									String editLink = "editProduct.html" + link;
									String discountLink = "doDiscountProduct.html" + link;
								%>
						<a href=<%=editLink %> data-toggle="modal">
							<button class="btn btn-default ">
								<span class="glyphicon glyphicon-edit"></span>
								Edit
							</button>
						</a> 
<%-- 						<a href=<%=deleteLink %> data-toggle="modal"> --%>
<!-- 							<button class="btn btn-default" > -->
<!-- 								<span class="glyphicon glyphicon-trash"></span>  -->
<!-- 								Delete -->
								
<!-- 							</button> -->
<!-- 						</a>  -->
						<a href=<%=discountLink %> data-toggle="modal">
							<button class="btn btn-default">
								<span class="glyphicon glyphicon-tag"></span> 
								Discount
								
							</button>
						</a>
					</td>
							<td>
					<div class="row">
				
					<form class = "form-horizontal" method="POST" action="doEditPercentageProduct">
						<div class = "col-md-7">
							<div class ="input-group">
								<input id="productID" name="productID" type="hidden" value= <%=product.getIdProduct() %> >
								<input id ="percentage" name ="percentage" type = "text" class = "form-control input-md"  placeholder=<%=product.getProductsupplieds().get(0).getPercentage() %>>
								<span class="input-group-addon">%</span>
							</div>
						
						</div>
					
						<div class = "col-md-5 col-md-pull-1">

							<button class="btn btn-default" type ="submit">
								<span class="glyphicon glyphicon-barcode"></span>
								Set %
							</button>
						</div>
						</form>	
					</div>
					<div class ="row">
					<form class = "form-horizontal" method="POST" action="doEditFixedRateProduct">
						<div class = "col-md-7">
					<div class ="input-group">
					<input id="productID" name="productID" type="hidden" value= <%=product.getIdProduct() %> >
					<input id="fixedRate" name="fixedRate" type = "text" class = "form-control input-md"  placeholder=<%=product.getProductsupplieds().get(0).getFixedRate() %>>
					<span class="input-group-addon"><span class="glyphicon glyphicon-euro"></span></span>
					</div>
					
					</div>
					
					<div class = "col-md-5 col-md-pull-1">

							<button class="btn btn-default" type="submit">
								<span class="glyphicon glyphicon-euro"></span>
								Set fix
							</button>
					</div>	
					</form>
					</div>
					</td>
				</tr>
			<%
		}
	}
	} else {
		session.setAttribute("productsuppliedList", productsuppliedList);
		System.out.println("##### yes you are here");
		LinkedHashMap<Product, List<Productsupplied>> map = new LinkedHashMap<Product, List<Productsupplied>>();
		
		System.out.println("PSSSSS "+productsuppliedList.size());
		for(Productsupplied ps : productsuppliedList){
			Product prod = ps.getProduct();
			if(map.get(prod)==null){
				List<Productsupplied> newList = new ArrayList<Productsupplied>();
				newList.add(ps);
				map.put(prod, newList);
				System.out.println("NEWLIST"+prod);
			}else{
				List<Productsupplied> list = map.get(prod);
				list.add(ps);
				map.put(prod, list);
				System.out.println("ADDED"+prod);
			}
			
		}
		
		for(Product p : map.keySet()){
			double minimumPrice = Integer.MAX_VALUE;			
			String quantity = null;
			double maxPrice = 0;
			int stock = 0;
			List<Productsupplied> supplied = map.get(p);
			Set<Supplier> suppliers = new HashSet<Supplier>();
			 
			for (Productsupplied ps : supplied) {
				stock += Integer.parseInt(ps.getQuantity());
				double price = Double.parseDouble(ps.getSellPrice());
				if (price < minimumPrice) {
					minimumPrice = price;
				}
				if ( price > maxPrice) {
					maxPrice = price;
				}
				suppliers.add(ps.getSupplier());				
				
			}
			System.out.println("----------Product : " + p.getIdProduct() + "has "+supplied.size()+ " suppliers");
			quantity = stock + "";
			String range = "" + minimumPrice + "-" + maxPrice + "";
			
			if (suppliers.size()==1)
				range = minimumPrice + "";

			String productLink = "prodSuppliers?productID="
					+ p.getIdProduct();

			
		
		
			%>
			<tr>
					<td><a href=<%=productLink%>><%=p.getIdProduct()%></a></td>
					<td><a href=<%=productLink%>><%=p.getName()%></a></td>
					<td><a href=<%=productLink%>><%=supplied.size()%></a></td>
					<td><%=range%> <span class="glyphicon glyphicon-euro"></span></td>
					<td><%=quantity %></td>
					<td>
						<%
									String link = "?productID=" + p.getIdProduct() + "&supplierID=" + p.getProductsupplieds().get(0).getSupplier().getId(); //+ bestSupplier.getId();
									String editLink = "editProduct.html" + link;
									String deleteLink = "doDeleteProduct.html" + link;
									String discountLink = "doDiscountProduct.html" + link;
								%>
						<a href=<%=editLink %> data-toggle="modal">
							<button class="btn btn-default ">
								<span class="glyphicon glyphicon-edit"></span>
								Edit
							</button>
						</a> 
						<a href=<%=deleteLink %> data-toggle="modal">
							<button class="btn btn-default" >
								<span class="glyphicon glyphicon-trash"></span> 
								Delete
								
							</button>
						</a> 
						<a href=<%=discountLink %> data-toggle="modal">
							<button class="btn btn-default">
								<span class="glyphicon glyphicon-tag"></span> 
								Discount
								
							</button>
						</a>
					</td>
							<td>
					<div class="row">
				
					<form class = "form-horizontal" method="POST" action="doEditPercentageProduct">
						<div class = "col-md-7">
							<div class ="input-group">
								<input id="productID" name="productID" type="hidden" value= <%=p.getIdProduct() %> >
								<input id ="percentage" name ="percentage" type = "text" class = "form-control input-md"  placeholder=<%=p.getProductsupplieds().get(0).getPercentage() %>>
								<span class="input-group-addon">%</span>
							</div>
						
						</div>
					
						<div class = "col-md-5 col-md-pull-1">

							<button class="btn btn-default" type ="submit">
								<span class="glyphicon glyphicon-barcode"></span>
								Set %
							</button>
						</div>
						</form>	
					</div>
					<div class ="row">
					<form class = "form-horizontal" method="POST" action="doEditFixedRateProduct">
						<div class = "col-md-7">
					<div class ="input-group">
					<input id="productID" name="productID" type="hidden" value= <%=p.getIdProduct() %> >
					<input id="fixedRate" name="fixedRate" type = "text" class = "form-control input-md"  placeholder=<%=p.getProductsupplieds().get(0).getFixedRate() %>>
					<span class="input-group-addon"><span class="glyphicon glyphicon-euro"></span></span>
					</div>
					
					</div>
					
					<div class = "col-md-5 col-md-pull-1">

							<button class="btn btn-default" type="submit">
								<span class="glyphicon glyphicon-euro"></span>
								Set fix
							</button>
					</div>	
					</form>
					</div>
					</td>
				</tr>
				<%
			}
		}
				%>
			</tbody>
		</table>
	</div>


<jsp:include page="edit_product_admin.jsp" />
<jsp:include page="delete_product_admin.jsp" />
<jsp:include page="discount_product_admin.jsp" />