<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashSet"%>
<%@page import="java.util.Set"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="model.*"%>

<jsp:useBean id="supplierList" scope="request" class="model.SupplierList" />
<jsp:useBean id="product" scope="request" class="model.Product" />
<jsp:useBean id="productsuppliedList" scope="request"
	class="model.ProductsuppliedList" />
	
	<div class="row" id="searchBar">
	<div class="col-md-3">
		<h2>Products sold by: <%=productsuppliedList.get(0).getSupplier().getName() %></h2>
	</div>
	<div class="col-md-9">
		<div id="searchDiv" style="margin-top: 20px">
			<p style="float: left; padding-top: 6px;">Search
					product:</p>
				<form style="float: left; padding-left: 20px;">
					<input type=text> 
					<button style="margin-left: 10px;" class="btn btn-primary">Search!</button>
				</form>
			</div>
		</div>
</div>		
		
	<div class="table-scrollbar" >
		<table class="table table-stripped table-bordered text-center">
			<thead>
				<tr>
					<th>Prod ID</th>
					<th>Prod Name</th>
					<th>Stock</th>
					<th>Sell Price</th>
				</tr>
			</thead>
			<tbody>
				<%
					for (Productsupplied ps : productsuppliedList) {
						String productLink = "prodSuppliers?productID="
								+ ps.getProduct().getIdProduct();
				%>
				<tr>
					<td><a href= <%=productLink%>><%=ps.getProduct().getIdProduct()%></a></td>
					<td><a href= <%=productLink%>> <%=ps.getProduct().getName()%></a></td>
					<td><%=ps.getQuantity()%> units</td>
					<td><%=ps.getSellPrice()%><span class = "glyphicon glyphicon-euro"></span></td>
				</tr>
				<%
					}
				%>
			</tbody>
		</table>
	</div>


<jsp:include page="edit_product_admin.jsp" />
<jsp:include page="delete_product_admin.jsp" />
<jsp:include page="discount_product_admin.jsp" />