<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashSet"%>
<%@page import="java.util.Set"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="model.*"%>

<jsp:useBean id="supplierList" scope="request" class="model.SupplierList" />

	
<div class="row" id="searchBar">
	<div class="col-md-3">
		<h2>Suppliers</h2>
	</div>
	<div class="col-md-9">
		<div id="searchDiv" style="margin-top: 20px">
			<p style="float: left; padding-top: 6px;">Search for:</p>
			<form style="float: left; padding-left: 20px;">
				<input type=text> <select class="selectpicker"
					style="margin-left: 10px">
					<option value="">Select filter</option>
					<option value="CNO" selected="selected">Supplier #</option>
					<option value="CN">Supplier Name</option>
					<option value="AD">Email Address</option>
				</select>
				<button style="margin-left: 10px;" class="btn btn-default">Search!</button>
			</form>
		</div>
	</div>
</div>

<div class="table-scrollbar">
	<table class="table table-stripped table-bordered text-center">
		<thead>
			<tr>
				<th><a href= "sortSup?mode=supplierID&ascendent=true">SupplierID</a></th>
				<th><a href= "sortSup?mode=name&ascendent=true">Name</a></th>
				<th><a href= "sortSup?mode=email&ascendent=true">email</a></th>
				<th>tools</th>
				<th>Profit Margin</th>
			</tr>
		</thead>
		<tbody>
			<%
			session.setAttribute("supplierList", supplierList);
				for (Supplier supplier : supplierList) {
					String supplierLink = "suppProducts?supplierID="
							+ supplier.getId();
			%>
			<tr>
				<td><a href=<%=supplierLink %>><%=supplier.getId()%></a></td>
				<td><a href=<%=supplierLink %>><%=supplier.getName() %></a></td>
				<td><%=supplier.getEmail() %></td>
				
				<td> <a href="#delete_supplier_admin" data-toggle="modal">
						<button class="btn btn-default">
							<span class="glyphicon glyphicon-trash"></span> Delete
						</button>
				</a></td>
				<td>
					<div class="row">
					<form class = "form-horizontal" method="POST" action="doEditPercentageSupplier">
					<div class = "col-md-5">
					<div class ="input-group">
						<input id="supplierID" name="supplierID" type="hidden" value= <%=supplier.getId() %> >
						<input id ="percentage" name ="percentage" type = "text" class = "form-control input-md" >
						<span class="input-group-addon">%</span>
					</div>
					
					</div>
					
					<div class = "col-md-6 col-md-pull-1">

							<button class="btn btn-default " type ="submit">
								<span class="glyphicon glyphicon-barcode"></span>
								Set percentage
							</button>
					</div>	
					</form>
					</div>
					<div class ="row">
					<form class = "form-horizontal" method="POST" action="doEditFixedRateSupplier">
						<div class = "col-md-5">
					<div class ="input-group">
					<input id="supplierID" name="supplierID" type="hidden" value= <%=supplier.getId() %> >
					<input id="fixedRate" name="fixedRate" type = "text" class = "form-control input-md">
					<span class="input-group-addon"><span class="glyphicon glyphicon-euro"></span></span>
					</div>
					
					</div>
					
					<div class = "col-md-6 col-md-pull-1" type="submit">

							<button class="btn btn-default ">
								<span class="glyphicon glyphicon-euro"></span>
								Set fixed rate
							</button>
					</div>	
					</form>
					</div>
					</td>
			</tr>
			<%
				}
			%>
		</tbody>
	</table>
</div>

<jsp:include page="delete_supplier_admin.jsp" />