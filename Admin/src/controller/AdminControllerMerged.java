package controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AdminController
 */
@WebServlet({ "/AdminControllerMerged", "/edited", "/doLogin", "/editClient",
		"/editProduct", "/editSupplier", "/discountProduct",
		"/doDeleteProduct.html", "/deleteClient", "/deleteSupplier", "/logout",
		"/deleteDiscount", "/products", "/onSale", "/suppliers", "/clients",
		"/prodSuppliers", "/suppProducts", "/category", "/changePage",
		"/editProduct.html", "/doEditProduct.html", "/doDiscountProduct.html",
		"/historyadmin", "/doEditPercentageProduct", "/doEditFixedRateProduct",
		"/categoryOnSale", "/doEditFixedRateSupplier",
		"/doEditPercentageSupplier", "/sort", "/sortSup", "/sortClient",
		"/sortOnSale", "/orderByMonth","/doReconciliation", "/supplierReconciliation" })
public class AdminControllerMerged extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Map<String, RequestHandler> pageMap;
	private String[] navigationPages = { "/edited", "/doLogin", "/editClient",
			"/editProduct", "/editSupplier", "/discountProduct",
			"/doDeleteProduct.html", "/deleteClient", "/deleteSupplier",
			"/logout", "/deleteDiscount", "/products", "/onSale", "/suppliers",
			"/clients", "/login", "/prodSuppliers", "/suppProducts",
			"/category", "/changePage", "/editProduct.html",
			"/doEditProduct.html", "/doDiscountProduct.html", "/historyadmin",
			"/doEditPercentageProduct", "/doEditFixedRateProduct",
			"/categoryOnSale", "/doEditFixedRateSupplier",
			"/doEditPercentageSupplier", "/sort", "/sortSup", "/sortClient",
			"/sortOnSale", "/orderByMonth","/doReconciliation", "/supplierReconciliation" };

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AdminControllerMerged() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		mapPages();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String path = request.getServletPath();
		if (path != null) {
			RequestHandler handler = pageMap.get(path);
			if (handler != null) {
				String view = handler.handleRequest(request, response);
				if (view != null) {
					RequestDispatcher rdp = request.getRequestDispatcher(view);
					rdp.forward(request, response);
				}
			} else {
				/* Error page */
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String path = request.getServletPath();
		if (path != null) {
			RequestHandler handler = pageMap.get(path);
			if (handler != null) {
				String view = handler.handleRequest(request, response);
				if (view != null) {
					RequestDispatcher rdp = request.getRequestDispatcher(view);
					rdp.forward(request, response);
				}
			} else {
				/* Error page */
			}
		}
	}

	private void mapPages() {
		pageMap = new HashMap<String, RequestHandler>();
		for (String page : navigationPages) {
			pageMap.put(page, new MergedHandler());
		}
	}

}
