package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sun.xml.ws.commons.ha.HaContext;

import model.Order;
import model.OrderList;
import model.ProductList;
import model.Product;
import model.Productsupplied;
import model.ProductsuppliedList;
import model.Purchase;
import model.Supplier;
import db.ProductManager;
import model.ClientList;
import db.ClientManager;
import db.OrderManagerAdmin;
import db.ReconciliationManager;
import db.SupplierManager;
import model.SupplierList;

public class MergedHandler implements RequestHandler {
	private HttpSession session;

	@Override
	public String handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		session = request.getSession(true);
		String path = request.getServletPath();

		if (path.equals("/doLogin")) {
			return login(request.getParameter("username"),
					request.getParameter("password"));
		}
		if (checkLogin()) {
			switch (path) {
			case "/editClient":
				return editClient();
			case "/editSupplier":
				return editSupplier();
			case "/editProduct.html":
				return editProduct(request, response);
			case "/doEditProduct.html":
				return doEditProduct(request, response);
			case "/discountProduct":
				return discountProduct(true);
			case "/doDeleteProduct.html":
				return doDeleteProduct(request, response);
			case "/deleteClient":
				return deleteClient();
			case "/deleteSupplier":
				return deleteSupplier();
			case "/doDiscountProduct.html":
				return doDiscountProduct(request, response);
			case "/deleteDiscount":
				return discountProduct(false);
			case "/logout":
				return doLogout();

			case "/onSale":
				loadProductsOnSale(request, response);
				return "/productsOnSale.jsp";
			case "/products":
				loadAllProducts(request, response);
				return "/products.jsp";
			case "/suppliers":
				loadAllSuppliers(request, response);
				return "/suppliers.jsp";
			case "/clients":
				loadAllClients(request, response);
				return "/clients.jsp";
			case "/login":
				return "/login.jsp";
			case "/suppProducts":
				loadAllProdSupp(request, response); // All products of a
													// supplier
				return "/suppProd.jsp";
			case "/prodSuppliers":
				loadAllSuppProd(request, response); // All suppliers for a
													// product
				return "/prodSupp.jsp";
			case "/category":
				return manageCategory(request);
			case "/categoryOnSale":
				return manageCategoryOnSale(request);
			case "/historyadmin":
				return historyAdmin(request);
			case "/doEditPercentageProduct":
				return doEditPercentageProduct(request, response);
			case "/doEditFixedRateProduct":
				return doEditFixedRateProduct(request, response);
			case "/doEditPercentageSupplier":
				return doEditPercentageSupplier(request, response);
			case "/doEditFixedRateSupplier":
				return doEditFixedRateSupplier(request, response);
			case "/sort":
				return sort(request);
			case "/sortSup":
				return sortSup(request);
			case "/sortClient":
				return sortClient(request);
			case "/sortOnSale":
				return sortOnSale(request);
			case "/orderByMonth":
				return getOrderByMonth(request);
			case "/doReconciliation":
				return doReconciliation(request);
			case "/supplierReconciliation":
				return getSuppliersReconciliation(request);
			}
		} else {
			return "/login.jsp";
		}

		return null;
	}

	private String doReconciliation(HttpServletRequest request) {
		ReconciliationManager manager = new ReconciliationManager();
		int month = Integer.parseInt(request.getParameter("month"));
		int year = Integer.parseInt(request.getParameter("year"));
		manager.reconciliateMonth(month, year);
		return "/orderByMonth?month=" + month + "&year=" + year;
	}

	private String sort(HttpServletRequest request) {
		ProductManager pm = new ProductManager();
		ProductsuppliedList productList = (ProductsuppliedList) session
				.getAttribute("productsuppliedList");
		if (productList == null) {
			productList = new ProductsuppliedList();
			ProductList asd = (ProductList) session.getAttribute("productList");

			for (Product p : asd) {
				List<Productsupplied> a = p.getProductsupplieds();
				System.out.println("----------a size = " + a.size());

				for (Productsupplied ps : a) {
					productList.add(ps);
				}
			}
		}
		System.out
				.println("----------productList size = " + productList.size());
		String mode = request.getParameter("mode");
		String ascendendParameter = request.getParameter("ascendent");
		boolean ascendent = ascendendParameter.equals("true");
		request.setAttribute("productsuppliedList",
				pm.orderProducts(productList, mode, ascendent));
		return "/products.jsp";
	}

	private String sortOnSale(HttpServletRequest request) {
		ProductManager pm = new ProductManager();
		ProductsuppliedList productList = (ProductsuppliedList) session
				.getAttribute("productsuppliedList");

		String mode = request.getParameter("mode");
		String ascendendParameter = request.getParameter("ascendent");
		boolean ascendent = ascendendParameter.equals("true");
		request.setAttribute("productsuppliedList",
				pm.orderProducts(productList, mode, ascendent));
		return "/productsOnSale.jsp";
	}

	private String sortSup(HttpServletRequest request) {
		SupplierManager sm = new SupplierManager();
		SupplierList supplierList = (SupplierList) session
				.getAttribute("supplierList");
		System.out.println("----------productList size = "
				+ supplierList.size());
		String mode = request.getParameter("mode");
		String ascendendParameter = request.getParameter("ascendent");
		boolean ascendent = ascendendParameter.equals("true");
		request.setAttribute("supplierList",
				sm.orderSuppliers(supplierList, mode, ascendent));
		return "/suppliers.jsp";
	}

	private String sortClient(HttpServletRequest request) {
		ClientManager cm = new ClientManager();
		ClientList clientList = (ClientList) session.getAttribute("clientList");

		String mode = request.getParameter("mode");
		String ascendendParameter = request.getParameter("ascendent");
		boolean ascendent = ascendendParameter.equals("true");
		request.setAttribute("clientList",
				cm.orderClients(clientList, mode, ascendent));
		return "/clients.jsp";
	}

	private String doEditPercentageProduct(HttpServletRequest request,
			HttpServletResponse response) {
		// TODO Auto-generated method stub

		String percentage = (String) request.getParameter("percentage");
		String productID = (String) request.getParameter("productID");
		if (!percentage.trim().equals("")) {
			ProductManager pm = new ProductManager();
			pm.setPercentageProduct(productID, percentage);
		}
		return "/products";
	}

	private String doEditFixedRateProduct(HttpServletRequest request,
			HttpServletResponse response) {
		// TODO Auto-generated method stub

		String fixedRate = (String) request.getParameter("fixedRate");
		String productID = (String) request.getParameter("productID");
		if (!fixedRate.trim().equals("")) {
			ProductManager pm = new ProductManager();
			pm.setFixedRateProduct(productID, fixedRate);
		}
		return "/products";
	}

	private String doEditPercentageSupplier(HttpServletRequest request,
			HttpServletResponse response) {
		// TODO Auto-generated method stub

		String percentage = (String) request.getParameter("percentage");
		String supplierID = (String) request.getParameter("supplierID");
		if (!percentage.trim().equals("")) {
			ProductManager pm = new ProductManager();
			pm.setPercentageSupplier(supplierID, percentage);
		}
		return "/suppliers";
	}

	private String doEditFixedRateSupplier(HttpServletRequest request,
			HttpServletResponse response) {
		// TODO Auto-generated method stub

		String fixedRate = (String) request.getParameter("fixedRate");
		String supplierID = (String) request.getParameter("supplierID");
		if (!fixedRate.trim().equals("")) {
			ProductManager pm = new ProductManager();
			pm.setFixedRateSupplier(supplierID, fixedRate);
		}
		return "/suppliers";
	}

	private String historyAdmin(HttpServletRequest request) {
		OrderManagerAdmin om = new OrderManagerAdmin();
		OrderList orders = om.getAllOrders();
		request.setAttribute("orderList", orders);
		return "historyadmin.jsp";
	}

	private boolean checkLogin() {
		if (session.getAttribute("username") == null) {
			return false;
		}
		return true;
	}

	private String doLogout() {
		session.setAttribute("username", null);
		return "/login.jsp";
	}

	private String deleteSupplier() {
		// DBQUERY
		return "/suppliers.jsp";
	}

	private String deleteClient() {
		// DBQUERY
		return "/clients.jsp";
	}

	private String login(String username, String password) {
		if ((password.length() == 0) || (username.length() == 0)) {
			session.setAttribute("error", true);
			return "/login.jsp";
		} else {

			session.setAttribute("username", username);
			return "/products";
		}
	}

	private String editClient() {
		// DBQUERY
		return "/clients.jsp";
	}

	private String editSupplier() {
		// DBQUERY
		return "/suppliers.jsp";
	}

	private String discountProduct(boolean discount) {
		// DBQUERY
		if (discount) {
			return "/products.jsp";
		} else {
			return "/productsOnSale.jsp";
		}
	}

	private void loadProductsOnSale(HttpServletRequest request,
			HttpServletResponse response) {
		ProductManager manager = new ProductManager();
		ProductsuppliedList productsuppliedList = manager
				.retriveProductsOnSale();
		request.setAttribute("productsuppliedList", productsuppliedList);

	}

	private void loadAllProducts(HttpServletRequest request,
			HttpServletResponse response) {
		ProductManager manager = new ProductManager();
		ProductList productList = manager.retriveProducts();
		request.setAttribute("productList", productList);

	}

	private void loadAllClients(HttpServletRequest request,
			HttpServletResponse response) {
		ClientManager manager = new ClientManager();
		ClientList clientList = manager.retriveClients();
		request.setAttribute("clientList", clientList);

	}

	private void loadAllSuppliers(HttpServletRequest request,
			HttpServletResponse response) {
		SupplierManager manager = new SupplierManager();
		SupplierList supplierList = manager.retriveSuppliers();
		request.setAttribute("supplierList", supplierList);

	}

	private void loadAllSuppProd(HttpServletRequest request,
			HttpServletResponse response) {

		int productID = Integer.parseInt((request.getParameter("productID")));
		SupplierManager manager = new SupplierManager();
		ProductsuppliedList supplierList = manager
				.retriveSuppliersFromProductID(productID);
		request.setAttribute("productsuppliedList", supplierList);

	}

	private void loadAllProdSupp(HttpServletRequest request,
			HttpServletResponse response) {

		int supplierID = Integer.parseInt((request.getParameter("supplierID")));
		ProductManager manager = new ProductManager();
		ProductsuppliedList productList = manager
				.retriveProductsFromSupplierID(supplierID);
		request.setAttribute("productsuppliedList", productList);

	}

	private String manageCategory(HttpServletRequest request) {
		String categoryName = (String) request.getParameter("category");
		ProductManager manager = new ProductManager();
		ProductList productList = manager
				.retriveProductsFromCategory(categoryName);
		request.setAttribute("productList", productList);
		return "/products.jsp";
	}

	private String manageCategoryOnSale(HttpServletRequest request) {
		String categoryName = (String) request.getParameter("category");
		ProductManager manager = new ProductManager();
		ProductsuppliedList productsuppliedList = manager
				.retriveProductsOnSaleCategory(categoryName);
		request.setAttribute("productsuppliedList", productsuppliedList);
		return "/productsOnSale.jsp";
	}

	private String doDeleteProduct(HttpServletRequest request,
			HttpServletResponse response) {
		System.out.println("\n\n#########  doDeleteProduct  ##########\n\n");
		String productID = (String) request.getParameter("productID");
		String supplierID = (String) request.getParameter("supplierID");

		ProductManager pm = new ProductManager();
		Productsupplied productsupplied = pm.retriveProductToEdit(productID,
				supplierID);

		String category = productsupplied.getProduct().getCategory().getName();
		System.out.println("\n\n#########  category: " + category
				+ "  ##########\n\n");

		pm.deleteProductsupplied(supplierID, productID);
		System.out
				.println("\n\n#########  END doDeleteProduct  ##########\n\n");
		return "/products";
	}

	private String editProduct(HttpServletRequest request,
			HttpServletResponse response) {
		String productID = (String) request.getParameter("productID");
		String supplierID = (String) request.getParameter("supplierID");
		session.setAttribute("productID", productID);
		ProductManager pm = new ProductManager();
		Productsupplied product = pm
				.retriveProductToEdit(productID, supplierID);
		session.setAttribute("productSuppliedToEdit", product);
		return "/editProduct.jsp";
	}

	private String doEditProduct(HttpServletRequest request,
			HttpServletResponse response) {
		System.out.println("\n\n#########  doEditProduct  ##########\n\n");
		String sellPrice = (String) request.getParameter("sellPrice");
		String productID = (String) request.getParameter("productID");
		String supplierID = (String) request.getParameter("supplierID");
		if (!sellPrice.trim().equals("")) {
			System.out.println("\n\n#########  sellPrice: " + sellPrice
					+ "  ##########\n\n");
			System.out.println("\n\n#########  productID: " + productID
					+ "  ##########\n\n");
			System.out.println("\n\n#########  supplierID: " + supplierID
					+ "  ##########\n\n");

			ProductManager pm = new ProductManager();
			pm.editProductSuppliedSellPrice(supplierID, productID, sellPrice);
			System.out
					.println("\n\n#########  END doEditProduct  ##########\n\n");
		}
		return "/products";
	}

	private String doDiscountProduct(HttpServletRequest request,
			HttpServletResponse response) {
		System.out.println("\n\n#########  doEditProduct  ##########\n\n");
		String quantity = (String) request.getParameter("sellPrice");
		String productID = (String) request.getParameter("productID");
		String supplierID = (String) request.getParameter("supplierID");

		System.out.println("\n\n#########  sellPrice: " + quantity
				+ "  ##########\n\n");
		System.out.println("\n\n#########  productID: " + productID
				+ "  ##########\n\n");
		System.out.println("\n\n#########  supplierID: " + supplierID
				+ "  ##########\n\n");

		ProductManager pm = new ProductManager();
		pm.editProductSuppliedDiscount(supplierID, productID, true);
		System.out.println("\n\n#########  END doEditProduct  ##########\n\n");
		return "/products";
	}

	private String getOrderByMonth(HttpServletRequest request) {
		int month;
		int year;
		try {
			month = Integer.parseInt(request.getParameter("month"));
			year = Integer.parseInt(request.getParameter("year"));
			OrderManagerAdmin orderManager = new OrderManagerAdmin();
			OrderList orderList = orderManager.getOrderInMonth(month, year);
			request.setAttribute("orderList", orderList);
			request.setAttribute("month", month);
			request.setAttribute("year", year);
		} catch (NumberFormatException e) {

		}
		return "/orderByMonth.jsp";
	}

	private String getSuppliersReconciliation(HttpServletRequest request) {
		int month;
		int year;
		try {
			month = Integer.parseInt(request.getParameter("month"));
			year = Integer.parseInt(request.getParameter("year"));
			SupplierManager sm = new SupplierManager();
			SupplierList supplierList = sm.retriveSuppliers();
			OrderManagerAdmin om = new OrderManagerAdmin();
			Map<Supplier,OrderList> map= new HashMap<Supplier, OrderList>();
			for (Supplier s : supplierList) {
				OrderList orderList = new OrderList();
				List<Purchase> purchaseList = om.getMonthPurchasesOfSupplier(
						s.getId(), month, year);
				for(Purchase p : purchaseList){
					Order order = new Order();
					Order purchaseOrder = p.getOrder();
					order.setOrderID(purchaseOrder.getOrderID());
					order.setClient(order.getClient());
					order.setDate(purchaseOrder.getDate());
					order.setOrderCode(purchaseOrder.getOrderCode());
					order.setBillingAddres(purchaseOrder.getBillingAddres());
					order.setDiscount(purchaseOrder.getDiscount());
					order.setShipAddress(purchaseOrder.getShipAddress());
					order.setPurchases(new ArrayList<Purchase>());
					int index = -1;
					for(int i=0;i<orderList.size();i++){
						Order o = orderList.get(i);
						if(o.getOrderID()==order.getOrderID()){
							index = i;
						}
					}
					if(index!=-1){
						Order finded = orderList.get(index);
						finded.addPurchas(p);
						System.out.println("fided");
					} else {
						order.addPurchas(p);
						orderList.add(order);
					}
				}
				System.out.println("########LIST"+orderList.size());
				map.put(s, orderList);
			}
			request.setAttribute("suppliersMap", map);
		} catch (NumberFormatException e) {

		}
		return "/orderBySupplier.jsp";
	}

}
