package db;

import java.util.Comparator;
import java.util.List;
import java.util.Properties;
import java.util.TreeSet;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import model.Client;
import model.ClientList;


import model.Supplier;
import model.SupplierList;

import org.eclipse.persistence.config.PersistenceUnitProperties;


public class ClientManager {
	private Properties properties;
	private String persistanceUnit = "Model";
	EntityManagerFactory emf;

	public ClientManager() {
		setupProperties();
	}
	
	public ClientList retriveClients() {
		EntityManager em = emf.createEntityManager();
		String queryText = "SELECT c FROM Client c";
		Query selectQuery = em.createQuery(queryText);
		List<?> result;
		result = selectQuery.getResultList();
		ClientList returnList = new ClientList();
		for (Object o : result) {
			returnList.add((Client) o);
		}
		em.close();
		return returnList;
	}
	

	private void setupProperties() {
		properties = new Properties();
		/*
		 * properties.put(PersistenceUnitProperties.CLASSLOADER, this.getClass()
		 * .getClassLoader());
		 */
		/*properties.put(PersistenceUnitProperties.CLASSLOADER,
				this.getClass().getClassLoader());
		properties.put(PersistenceUnitProperties.ECLIPSELINK_PERSISTENCE_UNITS,
				persistanceUnit);*/
		emf = Persistence.createEntityManagerFactory(persistanceUnit,
				properties);
	}
	
	public ClientList orderClients(ClientList clientList, String mode, boolean ascendent){
		Comparator<Client> comparator;
		switch (mode) {
		case "name":
			comparator = new CompareNames(ascendent);
			break;
		case "clientID":
			comparator = new CompareClientID(ascendent);
			break;
		case "email":
			comparator = new CompareEmail(ascendent);
			break;
		case "address":
			comparator = new CompareAddress(ascendent);
			break;
		default:
			comparator = new CompareNames(ascendent);
		}
		TreeSet<Client> orderedSet = new TreeSet<Client>(comparator);
		orderedSet.addAll(clientList);
		ClientList returnList = new ClientList();
		returnList.addAll(orderedSet);
		return returnList;
	}
	
private class CompareNames implements Comparator<Client>{
		
		private boolean ascendent;
		
		public CompareNames(boolean ascendent){
			this.ascendent = ascendent;
		}

		@Override
		public int compare(Client o1,
				Client o2) {
			int compare = o1.getSecondName().toUpperCase().compareTo(o2.getSecondName().toUpperCase());
			
			if (!ascendent) {
				compare = -compare;
			}
			return compare;
		}
	}
private class CompareClientID implements Comparator<Client>{
	
	private boolean ascendent;
	
	public CompareClientID(boolean ascendent){
		this.ascendent = ascendent;
	}

	@Override
	public int compare(Client o1,
			Client o2) {
		int compare = o1.getId()-o2.getId();
		
		if (!ascendent) {
			compare = -compare;
		}
		return compare;
	}
}

private class CompareEmail implements Comparator<Client>{
	
	private boolean ascendent;
	
	public CompareEmail(boolean ascendent){
		this.ascendent = ascendent;
	}

	@Override
	public int compare(Client o1,
			Client o2) {
		int compare = o1.getEmail().toUpperCase().compareTo(o2.getEmail().toUpperCase());
		
		if (!ascendent) {
			compare = -compare;
		}
		return compare;
	}
}
private class CompareAddress implements Comparator<Client>{
	
	private boolean ascendent;
	
	public CompareAddress(boolean ascendent){
		this.ascendent = ascendent;
	}

	@Override
	public int compare(Client o1,
			Client o2) {
		int compare = o1.getAddress().toUpperCase().compareTo(o2.getAddress().toUpperCase());
		
		if (!ascendent) {
			compare = -compare;
		}
		return compare;
	}
}

}
