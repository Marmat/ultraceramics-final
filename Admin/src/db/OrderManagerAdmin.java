package db;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import sun.security.krb5.internal.crypto.dk.ArcFourCrypto;
import model.OrderList;
import model.Order;
import model.Purchase;
import model.Supplier;
import model.SupplierList;

public class OrderManagerAdmin {
	private Properties properties;
	private String persistanceUnit = "Model";
	EntityManagerFactory emf;

	public OrderManagerAdmin() {
		setupProperties();
	}

	public OrderList getOrdersOfClient(int clientID) {
		EntityManager em = emf.createEntityManager();
		Query query = em.createQuery("SELECT o FROM Order o WHERE o.client.id="
				+ clientID + " ORDER BY o.date");
		List<?> queryResult = query.getResultList();
		if (queryResult.size() == 0) {
			return null;
		}
		OrderList orders = new OrderList();
		for (Object o : queryResult) {
			Order order = (Order) o;
			orders.add(order);
		}
		em.close();
		return orders;
	}

	public OrderList getAllOrders() {
		EntityManager em = emf.createEntityManager();
		Query query = em.createQuery("SELECT o FROM Order o");
		List<?> queryResult = query.getResultList();
		if (queryResult.size() == 0) {
			return null;
		}
		OrderList orders = new OrderList();
		for (Object o : queryResult) {
			Order order = (Order) o;
			orders.add(order);
		}
		return orders;
	}

	public List<Purchase> getMonthPurchasesOfSupplier(int supplierID,
			int month, int year) {
		int lastDay;
		switch (month) {
		case 4:
		case 6:
		case 9:
		case 11:
			lastDay = 30;
			break;
		case 2:
			if (year % 4 == 0) {
				lastDay = 29;
			} else {
				lastDay = 28;
			}
			break;
		default:
			lastDay = 31;
		}
		EntityManager em = emf.createEntityManager();
		String stringQuery = "SELECT p FROM Purchase p JOIN p.productsupplied ps,p.order o,ps.supplier s WHERE s.id="
				+ supplierID
				+ " AND o.date>='"
				+ year
				+ "-"
				+ month
				+ "-"
				+ "1"
				+ "' AND o.date<='"
				+ year
				+ "-"
				+ month
				+ "-"
				+ lastDay
				+ "'";
		Query query = em.createQuery(stringQuery);
		System.out.println("####QUERY#####");
		System.out.println(stringQuery);
		List<?> queryResult = query.getResultList();
		if (queryResult.size() == 0) {
			return null;
		}
		List<Purchase> purchaseList = new ArrayList<Purchase>();
		for (Object o : queryResult) {
			Purchase purchase = (Purchase) o;
			purchaseList.add(purchase);
			System.out.println(purchase.getId());
		}
		em.close();
		return purchaseList;

	}
	
	public OrderList getOrderInMonth(int month,int year){
		int lastDay;
		switch (month) {
		case 4:
		case 6:
		case 9:
		case 11:
			lastDay = 30;
			break;
		case 2:
			if (year % 4 == 0) {
				lastDay = 29;
			} else {
				lastDay = 28;
			}
			break;
		default:
			lastDay = 31;
		}
		EntityManager em = emf.createEntityManager();
		String stringQuery = "SELECT o FROM Order o WHERE o.date>='"
				+ year
				+ "-"
				+ month
				+ "-"
				+ "1"
				+ "' AND o.date<='"
				+ year
				+ "-"
				+ month
				+ "-"
				+ lastDay
				+ "'";
		Query query = em.createQuery(stringQuery);
		System.out.println("####QUERY#####");
		System.out.println(stringQuery);
		List<?> queryResult = query.getResultList();
		if (queryResult.size() == 0) {
			return null;
		}
		OrderList orderList = new OrderList();
		for (Object o : queryResult) {
			Order order = (Order) o;
			orderList.add(order);
		}
		em.close();
		return orderList;
	}

	public Map<Supplier, List<Purchase>> monthlyPurchasesFromSuppliers(
			int month, int year) {

		Map<Supplier, List<Purchase>> supplierMap = new HashMap<Supplier, List<Purchase>>();
		OrderManagerAdmin om = new OrderManagerAdmin();
		SupplierManager sm = new SupplierManager();

		SupplierList supplierList = sm.retriveSuppliers();
		for (Supplier s : supplierList) {
			System.out.println("######## supplierID=" + s.getId());
			if (om.getMonthPurchasesOfSupplier(s.getId(), month, year) != null) {
				List<Purchase> purchases = om.getMonthPurchasesOfSupplier(
						s.getId(), month, year);

				supplierMap.put(s, purchases);
				System.out.println(purchases);
			} else {
				System.out
						.println("###################################################YES I AM THE ERROR THAT YOU FINALY SOLVED YOU ARE A GENIUS!!!");
			}
		}

		return supplierMap;
	}


	private void setupProperties() {
		properties = new Properties();
		/*
		 * properties.put(PersistenceUnitProperties.CLASSLOADER, this.getClass()
		 * .getClassLoader());
		 */
		/*
		 * properties.put(PersistenceUnitProperties.CLASSLOADER,
		 * this.getClass().getClassLoader());
		 * properties.put(PersistenceUnitProperties
		 * .ECLIPSELINK_PERSISTENCE_UNITS, persistanceUnit);
		 */
		emf = Persistence.createEntityManagerFactory(persistanceUnit,
				properties);
	}
}
