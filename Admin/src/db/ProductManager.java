package db;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Properties;
import java.util.TreeSet;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.JoinColumn;
import javax.persistence.Persistence;
import javax.persistence.Query;

import model.GenericProductInterface;
import model.Product;
import model.ProductList;
import model.ProductsuppliedList;
import model.Productsupplied;
import model.ProductsuppliedPK;

import org.eclipse.persistence.config.PersistenceUnitProperties;

public class ProductManager {
	private Properties properties;
	private String persistanceUnit = "Model";
	EntityManagerFactory emf;

	public ProductManager() {

		setupProperties();
	}

	public ProductList retriveProducts() {
		EntityManager em = emf.createEntityManager();
		String queryText = "SELECT p FROM Product p " ;//WHERE p.active = true
		Query selectQuery = em.createQuery(queryText);		
		List<?> result;
		result = selectQuery.getResultList();		
		ProductList returnList = new ProductList();		
		for (Object o : result) {						
			returnList.add((Product) o);
		}
		em.close();
		return returnList;
	}
	
	public ProductsuppliedList retriveProductsOnSale() {
		EntityManager em = emf.createEntityManager();
		String queryText = "SELECT ps FROM Productsupplied ps WHERE ps.onSale = true AND ps.active = true";
		Query selectQuery = em.createQuery(queryText);		
		List<?> result;

		result = selectQuery.getResultList();		
		ProductsuppliedList returnList = new ProductsuppliedList();		
		for (Object o : result) {						
			returnList.add((Productsupplied) o);
		}
		em.close();
		return returnList;
	}
	
	public ProductsuppliedList retriveProductsOnSaleCategory(String category) {
		EntityManager em = emf.createEntityManager();
		String queryText = "SELECT ps FROM Productsupplied ps WHERE ps.onSale = true AND ps.active = true AND ps.product.category.name = '"			
				+ category + "'";	
		Query selectQuery = em.createQuery(queryText);		
		List<?> result;

		result = selectQuery.getResultList();		
		ProductsuppliedList returnList = new ProductsuppliedList();		
		for (Object o : result) {						
			returnList.add((Productsupplied) o);
		}
		em.close();
		return returnList;
	}

	public ProductList retriveProductsFromCategory(String categoryName) {
		EntityManager em = emf.createEntityManager();
		String queryText = "SELECT p FROM Product p WHERE p.category.name = '"			
				+ categoryName + "'";														
		List<?> result;																	
		Query selectQuery = em.createQuery(queryText);
		result = selectQuery.getResultList();
		ProductList returnList = new ProductList();
		System.out.println(returnList.size());
		for (Object o : result) {
			returnList.add((Product) o);
		}
		em.close();
		return returnList;
	}
	
	public Productsupplied retriveProductToEdit(String productID, String  supplierID){
		ProductsuppliedPK key = new ProductsuppliedPK();
		key.setProductID(Integer.parseInt(productID));
		key.setSupplierID(Integer.parseInt(supplierID));
		
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		
		Productsupplied productsuppliedToEdit = em.find(Productsupplied.class, key);
		
		em.getTransaction().commit();
		em.close();
		return productsuppliedToEdit;
	}
	
	public boolean deleteProductsupplied(String supplierID, String productID) {
		ProductsuppliedPK key = new ProductsuppliedPK();
		key.setProductID(Integer.parseInt(productID));
		key.setSupplierID(Integer.parseInt(supplierID));
		
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		
		Productsupplied productsuppliedToDelete = em.find(Productsupplied.class, key);
		productsuppliedToDelete.setActive(false);
		
		em.getTransaction().commit();
		em.close();
		return true;
	}
	
	public boolean editProductSuppliedSellPrice(String supplierID, String productID, String quantity) {
		System.out.println("\n\n#########  editProductSuppliedSellPrice_ProductManager  ##########\n\n");
		ProductsuppliedPK key = new ProductsuppliedPK();
		key.setProductID(Integer.parseInt(productID));
		key.setSupplierID(Integer.parseInt(supplierID));
		
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		
		Productsupplied productsuppliedToEdit = em.find(Productsupplied.class, key);
		productsuppliedToEdit.setSellPrice(quantity);
		
		em.getTransaction().commit();
		em.close();
		System.out.println("\n\n#########  END editProductSuppliedSellPrice_ProductManager  ##########\n\n");
		return true;
	}
	
	public boolean editProductSuppliedDiscount(String supplierID, String productID, Boolean quantity) {
		System.out.println("\n\n#########  editProductSuppliedDiscount_ProductManager  ##########\n\n");
		ProductsuppliedPK key = new ProductsuppliedPK();
		key.setProductID(Integer.parseInt(productID));
		key.setSupplierID(Integer.parseInt(supplierID));
		
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		
		Productsupplied productsuppliedToEdit = em.find(Productsupplied.class, key);
		productsuppliedToEdit.setOnSale(quantity);
		if(quantity == true){
			productsuppliedToEdit.setSellPrice(productsuppliedToEdit.getMinPrice());
		}else{
			productsuppliedToEdit.setSellPrice(productsuppliedToEdit.getMaxPrice());
		}
		
		em.getTransaction().commit();
		em.close();
		System.out.println("\n\n#########  END editProductSuppliedDiscount_ProductManager  ##########\n\n");
		return true;
	}

	public Productsupplied retriveProductFromIDs(int productID, int supplierID) {
		EntityManager em = emf.createEntityManager();
		ProductsuppliedPK key = new ProductsuppliedPK();		
		key.setProductID(productID);
		key.setSupplierID(supplierID);
		Productsupplied product = em.find(Productsupplied.class, key);
		return product;
	}

	public ProductsuppliedList searchProductAnd(String productName,
			String supplierName, String category) {
		EntityManager em = emf.createEntityManager();
		String queryText = "SELECT p FROM Product p";
		System.out.println(productName+"  "+supplierName+"  "+category);
		if (productName != null && supplierName != null && category != "any") {
			queryText = "SELECT ps FROM Productsupplied ps JOIN  ps.product p, ps.supplier s WHERE p.category.name = '"
					+ category + "' AND s.name='"+supplierName+"' AND p.name='"+productName+"'";
		}

		List<?> result;
		Query selectQuery = em.createQuery(queryText);
		result = selectQuery.getResultList();
		ProductsuppliedList returnList = new ProductsuppliedList();
		System.out.println(result.size());
		for (Object o : result) {
			returnList.add((Productsupplied) o);
		}
		em.close();
		return returnList;

	}

	public ProductsuppliedList searchProductOr(String productName, String supplierName,
			String category) {
		return null;

	}
	
	public ProductsuppliedList retriveProductsFromSupplierID(int supplierID) {
		EntityManager em = emf.createEntityManager();
		String queryText = "SELECT p FROM Productsupplied p WHERE p.supplier.id = '"
				+ supplierID + "'";
		List<?> result;
		Query selectQuery = em.createQuery(queryText);
		result =  selectQuery.getResultList();
		
		ProductsuppliedList returnList = new ProductsuppliedList();
	
		System.out.println(returnList.size());
		for (Object o : result) {
			returnList.add((Productsupplied) o);
		}
		em.close();
		return returnList;
	}
	
	public boolean setPercentageProduct( String productID, String percentage) {
		EntityManager em = emf.createEntityManager();
		String queryText = "SELECT ps FROM Productsupplied ps WHERE ps.product.idProduct = '"
				+ productID + "' AND ps.active = true";
		List<?> result;
		Query selectQuery = em.createQuery(queryText);
		result =  selectQuery.getResultList();
		
		ProductsuppliedList returnList = new ProductsuppliedList();
	
		for ( Object o : result){
			Productsupplied ps = (Productsupplied) o;
			String supplier = ps.getSupplier().getId() + "";
	
			setPercentageProductSupplied(productID, supplier , percentage);
		}
		em.close();
		return true;
	}
	
	public boolean setPercentageSupplier( String supplierID, String percentage) {
		EntityManager em = emf.createEntityManager();
		String queryText = "SELECT ps FROM Productsupplied ps WHERE ps.supplier.id = '"
				+ supplierID + "' AND ps.active = true";
		List<?> result;
		Query selectQuery = em.createQuery(queryText);
		result =  selectQuery.getResultList();
		
		ProductsuppliedList returnList = new ProductsuppliedList();
	
		for ( Object o : result){
			Productsupplied ps = (Productsupplied) o;
			String product = ps.getProduct().getIdProduct() + "";
	
			setPercentageProductSupplied(product, supplierID , percentage);
		}
		em.close();
		return true;
	}
	
	public boolean setPercentageProductSupplied( String productID, String supplierID, String percentage) {
		try{
			ProductsuppliedPK key = new ProductsuppliedPK();
		
		key.setProductID(Integer.parseInt(productID));
		key.setSupplierID(Integer.parseInt(supplierID));
		
		EntityManager em = emf.createEntityManager();
		EntityTransaction et = em.getTransaction();
		et.begin();
		
		Productsupplied productsuppliedToEdit = em.find(Productsupplied.class, key);
		System.out.println("###################################Percentage was :" + productsuppliedToEdit.getPercentage());
		productsuppliedToEdit.setPercentage(Integer.parseInt(percentage));
		productsuppliedToEdit.setFixedRate(0);
		
		et.commit();
		em.close();
		}
		catch(Exception e){}
		
		
		return true;
	}

	public boolean setFixedRateProduct( String productID, String fixedRate) {
		EntityManager em = emf.createEntityManager();
		String queryText = "SELECT ps FROM Productsupplied ps WHERE ps.product.idProduct = '"
				+ productID + "' AND ps.active = true";
		List<?> result;
		Query selectQuery = em.createQuery(queryText);
		result =  selectQuery.getResultList();
		
		
		//Get all the productsupplied with productID, for each productsupplied set percentage and fixedRate=null
		for ( Object o : result){
			Productsupplied ps = (Productsupplied) o;
			String supplier = ps.getSupplier().getId() + "";
	
			setFixedRateProductSupplied(productID, supplier , fixedRate);
		}
		em.close();
		return true;
	}
	
	public boolean setFixedRateSupplier( String supplierID, String fixedRate) {
		EntityManager em = emf.createEntityManager();
		String queryText = "SELECT ps FROM Productsupplied ps WHERE ps.supplier.id = '"
				+ supplierID + "' AND ps.active = true";
		List<?> result;
		Query selectQuery = em.createQuery(queryText);
		result =  selectQuery.getResultList();
		
		for ( Object o : result){
			Productsupplied ps = (Productsupplied) o;
			String product = ps.getProduct().getIdProduct() + "";
	
			setFixedRateProductSupplied(product, supplierID , fixedRate);
		}
		em.close();
		return true;
	}
	
	public boolean setFixedRateProductSupplied( String productID, String supplierID, String fixedRate) {
		try{
			ProductsuppliedPK key = new ProductsuppliedPK();
		
		key.setProductID(Integer.parseInt(productID));
		key.setSupplierID(Integer.parseInt(supplierID));
		
		EntityManager em = emf.createEntityManager();
		EntityTransaction et = em.getTransaction();
		et.begin();
		
		Productsupplied productsuppliedToEdit = em.find(Productsupplied.class, key);
		System.out.println("###################################Fixed Rate was :" + productsuppliedToEdit.getPercentage());
		productsuppliedToEdit.setPercentage(0);
		productsuppliedToEdit.setFixedRate(Integer.parseInt(fixedRate));
		
		et.commit();
		em.close();
		}
		catch(Exception e){}
		
		
		return true;
	}
	private void setupProperties() {
		properties = new Properties();
		/*
		 * properties.put(PersistenceUnitProperties.CLASSLOADER, this.getClass()
		 * .getClassLoader());
		 */
		/*
		 * properties.put(PersistenceUnitProperties.CLASSLOADER,
		 * this.getClass().getClassLoader());
		 * properties.put(PersistenceUnitProperties
		 * .ECLIPSELINK_PERSISTENCE_UNITS, persistanceUnit);
		 */
		emf = Persistence.createEntityManagerFactory(persistanceUnit,
				properties);
	}
	
	public int getStock(Product product){
		List<Productsupplied> supplied = new ArrayList<Productsupplied>();
		supplied = product.getProductsupplieds();
		int stock = 0;
		for (Productsupplied ps : supplied) {
			stock += Integer.parseInt(ps.getQuantity());
		}
		return stock;
	}
	
	public ProductsuppliedList orderProducts(ProductsuppliedList productList, String mode, boolean ascendent){
		Comparator<Productsupplied> comparator;
		switch (mode) {
		case "name":
			comparator = new CompareNames(ascendent);
			break;
		case "price":
			comparator = new ComparePrice(ascendent);
			break;
		case "productID":
			comparator = new CompareProductID(ascendent);
			break;
		case "suppliers":
			comparator = new CompareSupplierNum(ascendent);
			break;
		case "quantity":
			comparator = new CompareQuantity(ascendent);
			break;
		case "stock":
			comparator = new CompareStock(ascendent);
			break;
		default:
			comparator = new CompareNames(ascendent);
		}
		TreeSet<Productsupplied> orderedSet = new TreeSet<Productsupplied>(comparator);
		orderedSet.addAll(productList);
		System.out.println("ORDERED:");
		for(GenericProductInterface product : orderedSet){
			System.out.println(product.getProductName());
		}
		ProductsuppliedList returnList = new ProductsuppliedList();
		returnList.addAll(orderedSet);
		System.out.println("ORDERED:");
		for(GenericProductInterface product : returnList){
			System.out.println(product.getProductName());
		}
		return returnList;
	}
	

private class CompareNames implements Comparator<Productsupplied>{
		
		private boolean ascendent;
		
		public CompareNames(boolean ascendent){
			this.ascendent = ascendent;
		}

		@Override
		public int compare(Productsupplied o1,
				Productsupplied o2) {
			int compare = o1.getProductName().toUpperCase().compareTo(o2.getProductName().toUpperCase());
			if(compare==0){
				return new CompareSupplier(ascendent).compare(o1, o2);
			}
			if (!ascendent) {
				compare = -compare;
			}
			return compare;
		}
	}
	
	private class ComparePrice implements Comparator<Productsupplied>{
		private boolean ascendent;
		
		public ComparePrice(boolean ascendent){
			this.ascendent = ascendent;
		}
		@Override
		public int compare(Productsupplied o1, Productsupplied o2) {
			int compare = Integer.parseInt(o1.getSellPrice()) - Integer.parseInt(o2.getSellPrice());
			if(compare==0){
				return new CompareNames(ascendent).compare(o1, o2);
			}
			if(!ascendent){
				compare = - compare;
			}
			return compare;
		}
		
	}
	private class CompareSupplier implements Comparator<Productsupplied>{
		private boolean ascendent;
		
		public CompareSupplier(boolean ascendent){
			this.ascendent = ascendent;
		}
		@Override
		public int compare(Productsupplied o1, Productsupplied o2) {
			int compare = o1.getSupplier().getId() - o2.getSupplier().getId();
//			if(compare==0){
//				return new CompareNames(ascendent).compare(o1, o2);
//			}
			if(!ascendent){
				compare = - compare;
			}
			return compare;
		}
		
	}
	
	private class CompareProductID implements Comparator<Productsupplied>{
		
		private boolean ascendent;
		
		public CompareProductID(boolean ascendent){
			this.ascendent = ascendent;
		}

		@Override
		public int compare(Productsupplied o1,
				Productsupplied o2) {
			int compare = o1.getProduct().getIdProduct() - o2.getProduct().getIdProduct();
			if(compare==0){
				return new ComparePrice(ascendent).compare(o1, o2);
			}
			if (!ascendent) {
				compare = -compare;
			}
			return compare;
		}
	}
	
	private class CompareSupplierNum implements Comparator<Productsupplied>{
		
		private boolean ascendent;
		
		public CompareSupplierNum(boolean ascendent){
			this.ascendent = ascendent;
		}

		@Override
		public int compare(Productsupplied o1,
				Productsupplied o2) {
			int compare = o1.getProduct().getProductsupplieds().size() - o2.getProduct().getProductsupplieds().size();
			if(compare==0){
				return new CompareNames(ascendent).compare(o1, o2);
			}
			if (!ascendent) {
				compare = -compare;
			}
			return compare;
		}
	}
	
	
	private class CompareStock implements Comparator<Productsupplied>{
			
			private boolean ascendent;
			
			public CompareStock(boolean ascendent){
				this.ascendent = ascendent;
			}
	
			@Override
			public int compare(Productsupplied o1,
					Productsupplied o2) {
				int compare = getStock(o1.getProduct()) - getStock(o2.getProduct());
				if(compare==0){
					return new CompareNames(ascendent).compare(o1, o2);
				}
				if (!ascendent) {
					compare = -compare;
				}
				return compare;
			}
		}
	
	private class CompareQuantity implements Comparator<Productsupplied>{
		
		private boolean ascendent;
		
		public CompareQuantity(boolean ascendent){
			this.ascendent = ascendent;
		}

		@Override
		public int compare(Productsupplied o1,
				Productsupplied o2) {
			int compare = Integer.parseInt(o1.getQuantity()) - Integer.parseInt(o2.getQuantity());
			if(compare==0){
				return new CompareNames(ascendent).compare(o1, o2);
			}
			if (!ascendent) {
				compare = -compare;
			}
			return compare;
		}
	}

}