package db;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import model.Order;
import model.OrderList;
import model.Purchase;
import model.Supplier;

public class ReconciliationManager {
	private static final String targetURL = "http://localhost:8080/bank/bankRequest/bankService/";

	public void reconciliateMonth(int month, int year) {
		OrderManagerAdmin orderManager = new OrderManagerAdmin();
		Map<Supplier, List<Purchase>> supplierPurchasesMap = orderManager
				.monthlyPurchasesFromSuppliers(month, year);  
		Map<Supplier, Double> supplierIncomeMap = new HashMap<Supplier, Double>();
		double ultraceramicsIncome = 0;
		for (Supplier s : supplierPurchasesMap.keySet()) {
			double totalMonthlyIncome = 0;
			List<Purchase> purchases = supplierPurchasesMap.get(s);
			for (Purchase p : purchases) {
				double price = p.getPrice().doubleValue();
				double newIncome = price;
				int percentage =p.getPercentage();
				int fixed = p.getFixedRate();
				System.out.println("p"+percentage+" "+fixed);
				// calculate fixed rate or the other thing
				if (p.getPercentage() != 0) {
					newIncome = newIncome * (p.getPercentage()) / 100;
				} else if (p.getFixedRate() != 0) {
					newIncome = p.getFixedRate();
				}
				ultraceramicsIncome += newIncome;
				double supplierIncome = price - newIncome;
				totalMonthlyIncome = totalMonthlyIncome + supplierIncome;
				// end of calculations
			}
			supplierIncomeMap.put(s, totalMonthlyIncome);
		}
		
		double businessIncome = 0;
		OrderList orderList = orderManager.getOrderInMonth(month, year);
		for(Order o : orderList){
			businessIncome += o.getAdminEarnings();
		}

		try {
			URL targetUrl = new URL(targetURL+"supplierUpdate");
			for (Supplier s : supplierIncomeMap.keySet()) {
				HttpURLConnection httpConnection = (HttpURLConnection) targetUrl
						.openConnection();
				httpConnection.setDoOutput(true);
				httpConnection.setRequestMethod("POST");
				httpConnection.setRequestProperty("Content-Type",
						"application/json");
				String input = "{\"supplierID\":" + s.getId()
						+ ",\"amount\":" + supplierIncomeMap.get(s) + ",\"month\":" + month + ",\"year\":" + year + "}";
				System.out.println(input);
				OutputStream outputStream = httpConnection.getOutputStream();
				outputStream.write(input.getBytes());
				outputStream.flush();
				BufferedReader responseBuffer = new BufferedReader(
						new InputStreamReader(httpConnection.getInputStream()));
				System.out.println(responseBuffer.readLine());
				httpConnection.disconnect();
			}
			HttpURLConnection httpConnection = (HttpURLConnection) targetUrl
					.openConnection();
			httpConnection.setDoOutput(true);
			httpConnection.setRequestMethod("POST");
			httpConnection.setRequestProperty("Content-Type",
					"application/json");
			targetUrl = new URL(targetURL+"businessReconciliation");
			httpConnection = (HttpURLConnection) targetUrl.openConnection();
			httpConnection.setDoOutput(true);
			httpConnection.setRequestMethod("POST");
			httpConnection.setRequestProperty("Content-Type",
					"application/json");
			String input = "{\"amount\":" + businessIncome + ",\"month\":" + month + ",\"year\":" + year + "}";
			System.out.println(input);
			OutputStream outputStream = httpConnection.getOutputStream();
			outputStream.write(input.getBytes());
			outputStream.flush();
			BufferedReader responseBuffer = new BufferedReader(
					new InputStreamReader(httpConnection.getInputStream()));
			System.out.println(responseBuffer.readLine());
			httpConnection.disconnect();

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
