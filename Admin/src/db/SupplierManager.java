package db;

import java.util.Comparator;
import java.util.List;
import java.util.Properties;
import java.util.TreeSet;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import model.*;

import org.eclipse.persistence.config.PersistenceUnitProperties;


public class SupplierManager {
	private Properties properties;
	private String persistanceUnit = "Model";
	EntityManagerFactory emf;

	public SupplierManager() {
		setupProperties();
	}
	
	public SupplierList retriveSuppliers() {
		EntityManager em = emf.createEntityManager();
		String queryText = "SELECT s FROM Supplier s";
		Query selectQuery = em.createQuery(queryText);
		List<?> result;
		result = selectQuery.getResultList();
		SupplierList returnList = new SupplierList();
		for (Object o : result) {
			returnList.add((Supplier) o);
		}
		em.close();
		return returnList;
	}
	
	public ProductsuppliedList retriveSuppliersFromProductID(int productID) {
		EntityManager em = emf.createEntityManager();
		String queryText = "SELECT p FROM Productsupplied p WHERE p.product.idProduct = '"
				+ productID + "'";
		List<?> result;
		Query selectQuery = em.createQuery(queryText);
		result =  selectQuery.getResultList();
		
		ProductsuppliedList returnList = new ProductsuppliedList();
	
		System.out.println(returnList.size());
		for (Object o : result) {
			returnList.add((Productsupplied) o);
		}
		em.close();
		return returnList;
	}
	

	private void setupProperties() {
		properties = new Properties();
		/*
		 * properties.put(PersistenceUnitProperties.CLASSLOADER, this.getClass()
		 * .getClassLoader());
		 */
		/*properties.put(PersistenceUnitProperties.CLASSLOADER,
				this.getClass().getClassLoader());
		properties.put(PersistenceUnitProperties.ECLIPSELINK_PERSISTENCE_UNITS,
				persistanceUnit);*/
		emf = Persistence.createEntityManagerFactory(persistanceUnit,
				properties);
	}
	
	public SupplierList orderSuppliers(SupplierList supplierList, String mode, boolean ascendent){
		Comparator<Supplier> comparator;
		switch (mode) {
		case "name":
			comparator = new CompareNames(ascendent);
			break;
		case "supplierID":
			comparator = new CompareSupplierID(ascendent);
			break;
		case "email":
			comparator = new CompareEmail(ascendent);
			break;
		default:
			comparator = new CompareNames(ascendent);
		}
		TreeSet<Supplier> orderedSet = new TreeSet<Supplier>(comparator);
		orderedSet.addAll(supplierList);
		SupplierList returnList = new SupplierList();
		returnList.addAll(orderedSet);
		return returnList;
	}
	
private class CompareNames implements Comparator<Supplier>{
		
		private boolean ascendent;
		
		public CompareNames(boolean ascendent){
			this.ascendent = ascendent;
		}

		@Override
		public int compare(Supplier o1,
				Supplier o2) {
			int compare = o1.getName().toUpperCase().compareTo(o2.getName().toUpperCase());
			
			if (!ascendent) {
				compare = -compare;
			}
			return compare;
		}
	}
private class CompareSupplierID implements Comparator<Supplier>{
	
	private boolean ascendent;
	
	public CompareSupplierID(boolean ascendent){
		this.ascendent = ascendent;
	}

	@Override
	public int compare(Supplier o1,
			Supplier o2) {
		int compare = o1.getId()-o2.getId();
		
		if (!ascendent) {
			compare = -compare;
		}
		return compare;
	}
}

private class CompareEmail implements Comparator<Supplier>{
	
	private boolean ascendent;
	
	public CompareEmail(boolean ascendent){
		this.ascendent = ascendent;
	}

	@Override
	public int compare(Supplier o1,
			Supplier o2) {
		int compare = o1.getEmail().compareTo(o2.getEmail());
		
		if (!ascendent) {
			compare = -compare;
		}
		return compare;
	}
}

}