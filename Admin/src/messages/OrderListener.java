package messages;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.eclipse.persistence.config.PersistenceUnitProperties;

import model.Discount;
import model.Order;
import model.Productsupplied;
import model.ProductsuppliedPK;
import model.Purchase;

@MessageDriven(mappedName="jms/OrdersQ")
public class OrderListener implements MessageListener{
	
	private Properties properties;
	private String persistanceUnit = "Model";
	private EntityManagerFactory emf;
	
	public OrderListener() {
		setupProperties();
	}
	
	@Override
	public void onMessage(Message arg0) {
		ObjectMessage message = (ObjectMessage) arg0;
		try {
			System.out.println("SENDING ORDER ADMIN################");
			Order order = (Order) message.getObject();
			EntityManager em = emf.createEntityManager();
			EntityTransaction et = em.getTransaction();
			et.begin();
			System.out.println(order.getOrderID());
			em.persist(order);
			List<Purchase> purchases = order.getPurchases();
			for(Purchase p:purchases){
				Productsupplied ps = p.getProductsupplied();
				ProductsuppliedPK pk = new ProductsuppliedPK();
				pk.setProductID(ps.getProduct().getIdProduct());
				pk.setSupplierID(ps.getSupplier().getId());
				Productsupplied realPs = em.find(Productsupplied.class,pk);
				int newQuantity = (Integer.parseInt(realPs.getQuantity())-p.getQuantity());
				realPs.setQuantity(newQuantity+"");
				em.persist(p);
			}
			if(order.getDiscount()!=null){
				Discount discount = order.getDiscount();
				discount.setOrder(order);
			}
			et.commit();
			em.close();
			
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void setupProperties() {
		properties = new Properties();
		/*
		 * properties.put(PersistenceUnitProperties.CLASSLOADER, this.getClass()
		 * .getClassLoader());
		 * 
		 * /*properties.put(PersistenceUnitProperties.CLASSLOADER,
		 * this.getClass().getClassLoader());
		 */
		properties.put(PersistenceUnitProperties.ECLIPSELINK_PERSISTENCE_UNITS,
				persistanceUnit);
		emf = Persistence.createEntityManagerFactory(persistanceUnit,
				properties);
	}

}
