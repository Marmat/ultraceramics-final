package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the paymenttype database table.
 * 
 */
@Entity
@NamedQuery(name="Paymenttype.findAll", query="SELECT p FROM Paymenttype p")
public class Paymenttype implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private String info;

	private String type;

	//bi-directional many-to-one association to Order
	@OneToMany(mappedBy="paymenttype")
	private List<Order> orders;

	public Paymenttype() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getInfo() {
		return this.info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<Order> getOrders() {
		return this.orders;
	}

	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}

	public Order addOrder(Order order) {
		getOrders().add(order);
		order.setPaymenttype(this);

		return order;
	}

	public Order removeOrder(Order order) {
		getOrders().remove(order);
		order.setPaymenttype(null);

		return order;
	}

}