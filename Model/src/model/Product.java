package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the product database table.
 * 
 */
@Entity
@NamedQuery(name="Product.findAll", query="SELECT p FROM Product p")
public class Product implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idProduct;

	private boolean active;

	private String description;

	private String name;

	//bi-directional many-to-one association to Category
	@ManyToOne
	@JoinColumn(name="CategoryID")
	private Category category;

	//bi-directional many-to-one association to Productsuplied
	@OneToMany(mappedBy="product")
	private List<Productsuplied> productsuplieds;

	//bi-directional many-to-many association to Client
	@ManyToMany
	@JoinTable(
		name="subscription"
		, joinColumns={
			@JoinColumn(name="ProductID")
			}
		, inverseJoinColumns={
			@JoinColumn(name="ClientID")
			}
		)
	private List<Client> clients;

	public Product() {
	}

	public int getIdProduct() {
		return this.idProduct;
	}

	public void setIdProduct(int idProduct) {
		this.idProduct = idProduct;
	}

	public boolean getActive() {
		return this.active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Category getCategory() {
		return this.category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public List<Productsuplied> getProductsuplieds() {
		return this.productsuplieds;
	}

	public void setProductsuplieds(List<Productsuplied> productsuplieds) {
		this.productsuplieds = productsuplieds;
	}

	public Productsuplied addProductsuplied(Productsuplied productsuplied) {
		getProductsuplieds().add(productsuplied);
		productsuplied.setProduct(this);

		return productsuplied;
	}

	public Productsuplied removeProductsuplied(Productsuplied productsuplied) {
		getProductsuplieds().remove(productsuplied);
		productsuplied.setProduct(null);

		return productsuplied;
	}

	public List<Client> getClients() {
		return this.clients;
	}

	public void setClients(List<Client> clients) {
		this.clients = clients;
	}

}