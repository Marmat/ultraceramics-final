package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the productsuplied database table.
 * 
 */
@Entity
@NamedQuery(name="Productsuplied.findAll", query="SELECT p FROM Productsuplied p")
public class Productsuplied implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private ProductsupliedPK id;

	private String maxPrice;

	private String minPrice;

	private byte[] onSale;

	private String quantity;

	private String sellPrice;

	//bi-directional many-to-one association to Supplier
	@ManyToOne
	@JoinColumn(name="SupplierID")
	private Supplier supplier;

	//bi-directional many-to-one association to Product
	@ManyToOne
	@JoinColumn(name="ProductID")
	private Product product;

	//bi-directional many-to-one association to Purchase
	@OneToMany(mappedBy="productsuplied")
	private List<Purchase> purchases;

	public Productsuplied() {
	}

	public ProductsupliedPK getId() {
		return this.id;
	}

	public void setId(ProductsupliedPK id) {
		this.id = id;
	}

	public String getMaxPrice() {
		return this.maxPrice;
	}

	public void setMaxPrice(String maxPrice) {
		this.maxPrice = maxPrice;
	}

	public String getMinPrice() {
		return this.minPrice;
	}

	public void setMinPrice(String minPrice) {
		this.minPrice = minPrice;
	}

	public byte[] getOnSale() {
		return this.onSale;
	}

	public void setOnSale(byte[] onSale) {
		this.onSale = onSale;
	}

	public String getQuantity() {
		return this.quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getSellPrice() {
		return this.sellPrice;
	}

	public void setSellPrice(String sellPrice) {
		this.sellPrice = sellPrice;
	}

	public Supplier getSupplier() {
		return this.supplier;
	}

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}

	public Product getProduct() {
		return this.product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public List<Purchase> getPurchases() {
		return this.purchases;
	}

	public void setPurchases(List<Purchase> purchases) {
		this.purchases = purchases;
	}

	public Purchase addPurchas(Purchase purchas) {
		getPurchases().add(purchas);
		purchas.setProductsuplied(this);

		return purchas;
	}

	public Purchase removePurchas(Purchase purchas) {
		getPurchases().remove(purchas);
		purchas.setProductsuplied(null);

		return purchas;
	}

}