package model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the purchase database table.
 * 
 */
@Entity
@NamedQuery(name="Purchase.findAll", query="SELECT p FROM Purchase p")
public class Purchase implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private PurchasePK id;

	private BigDecimal price;

	private int quantity;

	//bi-directional many-to-one association to Order
	@ManyToOne
	@JoinColumn(name="orderID")
	private Order order;

	//bi-directional many-to-one association to Productsuplied
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name="productID", referencedColumnName="ProductID"),
		@JoinColumn(name="supplierID", referencedColumnName="SupplierID")
		})
	private Productsuplied productsuplied;

	public Purchase() {
	}

	public PurchasePK getId() {
		return this.id;
	}

	public void setId(PurchasePK id) {
		this.id = id;
	}

	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public int getQuantity() {
		return this.quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Order getOrder() {
		return this.order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public Productsuplied getProductsuplied() {
		return this.productsuplied;
	}

	public void setProductsuplied(Productsuplied productsuplied) {
		this.productsuplied = productsuplied;
	}

}