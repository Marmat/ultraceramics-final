package model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the purchase database table.
 * 
 */
@Embeddable
public class PurchasePK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(insertable=false, updatable=false)
	private int orderID;

	@Column(insertable=false, updatable=false)
	private int supplierID;

	@Column(insertable=false, updatable=false)
	private int productID;

	public PurchasePK() {
	}
	public int getOrderID() {
		return this.orderID;
	}
	public void setOrderID(int orderID) {
		this.orderID = orderID;
	}
	public int getSupplierID() {
		return this.supplierID;
	}
	public void setSupplierID(int supplierID) {
		this.supplierID = supplierID;
	}
	public int getProductID() {
		return this.productID;
	}
	public void setProductID(int productID) {
		this.productID = productID;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof PurchasePK)) {
			return false;
		}
		PurchasePK castOther = (PurchasePK)other;
		return 
			(this.orderID == castOther.orderID)
			&& (this.supplierID == castOther.supplierID)
			&& (this.productID == castOther.productID);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.orderID;
		hash = hash * prime + this.supplierID;
		hash = hash * prime + this.productID;
		
		return hash;
	}
}