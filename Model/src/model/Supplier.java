package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the supplier database table.
 * 
 */
@Entity
@NamedQuery(name="Supplier.findAll", query="SELECT s FROM Supplier s")
public class Supplier implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private boolean active;

	private String address;

	private String email;

	private String name;

	private String password;

	//bi-directional many-to-one association to Productsuplied
	@OneToMany(mappedBy="supplier")
	private List<Productsuplied> productsuplieds;

	public Supplier() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean getActive() {
		return this.active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<Productsuplied> getProductsuplieds() {
		return this.productsuplieds;
	}

	public void setProductsuplieds(List<Productsuplied> productsuplieds) {
		this.productsuplieds = productsuplieds;
	}

	public Productsuplied addProductsuplied(Productsuplied productsuplied) {
		getProductsuplieds().add(productsuplied);
		productsuplied.setSupplier(this);

		return productsuplied;
	}

	public Productsuplied removeProductsuplied(Productsuplied productsuplied) {
		getProductsuplieds().remove(productsuplied);
		productsuplied.setSupplier(null);

		return productsuplied;
	}

}