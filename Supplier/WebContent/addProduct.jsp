<%@page import="model.Category"%>
<%@page import="java.util.List"%>
<%@page import="db.CategoryManager"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class = "modal fade" id = "add_product_supplier" role = "dialog">					
					<div class = "modal-dialog">
							<div class = "modal-content">
								<form class = "form-horizontal" method="POST" action="doAddProduct.html">
									<div class = "modal-header">
										<h4>Add product</h4>
									</div>
									<div class = "modal-body">
									
										<div class = "form-group">
											<div class = "col col-md-4 col-sm-4 col-xs-6">
												<a href = "#"><img src = "images/Prueba2.jpg" alt = "bowling" class = "pull-left img-thumbnail" /></a>
												<br>
												<br>
												<button  class = "btn btn-deafult">Upload </button>
											</div>
											<div class = "col col-md-8 col-sm-8 col-xs-6">
												<label for = "description">Description:</label><br>
		                                        <textarea id = "description" name="description" class = "form-control" rows = "4" placeholder = "Description of a 'Ceramic vase', blah, blah, blah..."></textarea>
											</div>
										</div>
										
										<div class = "form-group">
											<label for = "name" class = "col-lg-2 control-label">Product name:</label>
											<div class = "col-lg-10">
												<input id="name" name="name" class="form-control input-md" type="text">
											</div>
										</div>
								   
										<div class = "form-group">
											<label for = "minPrice" class = "col-md-2 control-label">Min price:</label>
											<div class = "col-md-4">
												<input id="minPrice" name="minPrice" class="form-control input-md" type="text">
											</div>
											<label for = "maxPrice" class = "col-md-2 control-label">Max price:</label>
											<div class = "col-md-4">
												<input id="maxPrice" name="maxPrice" class="form-control input-md" type="text">
											</div>
										</div>
										
										<div class = "form-group">
											<label for = "quantity" class = "col-md-2 control-label">Quantity:</label>
											<div class = "col-md-4">
												<input id="quantity" name="quantity" class="form-control input-md" type="text">
											</div>
											
										</div>
										
										<div class = "form-group">
											<label for = "productType" class = "col-lg-2 control-label">Product type:</label>
											<div class = "col-lg-4">
												<select class="selectpicker" style="margin-top:10px" id="category" name="category">
														<option value="" selected="selected">Select category</option>
													<%
														CategoryManager manager = new CategoryManager();
									                	List<Category> categories = manager.getCategoryList();
									                	for(Category category : categories){
									                		String name = category.getName().toString();
									                %>
									                	<option value=<%=name %> ><%=name %></option>
									                     <% 	} %>
														
														
												</select> 
											</div>
											
										</div>
										

								   
									</div>
									<div class = "modal-footer">
										<a class = "btn btn-default" data-dismiss = "modal">Cancel</a>    
										<button class = "btn btn-primary" type = "submit">Save</button>
									</div>
								</form>
							</div>
					</div>
					
</div>					