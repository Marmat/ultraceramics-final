<div class="modal fade" id="modalContact" role="dialog" tabindex="-1" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h1 class="text-left">Contact</h1>
			</div>
			<div class="modal-body">
					<p><b>If you encounter any trouble or wish to contact us, we provide the following information:</b></p>
					<p>Email: ultraceramicsmail@something.com</p>
				  	<p>Phone number: 999999999</p>
			</div>
		</div>
	</div>
</div>
