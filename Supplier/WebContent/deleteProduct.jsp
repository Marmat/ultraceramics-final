<div class="modal fade" id="delete_product_supplier" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<form class="form-horizontal" method="POST"
				action="deleteProduct.html">
				<div class="modal-header">
					<h4>Delete product</h4>
				</div>
				<div class="modal-body">
					<p>Are you sure you want to delete the product?</p>
				</div>
				<div class="modal-footer">
					<a class="btn btn-default" data-dismiss="modal">Cancel</a>
					<button class="btn btn-primary" type="submit">Delete</button>
				</div>
			</form>
		</div>
	</div>
</div>