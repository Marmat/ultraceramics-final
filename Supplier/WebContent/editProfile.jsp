<%@page import="model.Supplier"%>
<%
	Supplier user = (Supplier) session.getAttribute("user");

	String string = user.getAddress();
	string = string.replaceAll(" ", "_");
	String[] parts = string.split(",");
	String address = null;
	String zip = null;
	String city = null;
	if(parts.length==3){
		 address = parts[0];
		 zip = parts[1];
		 city = parts[2];
	}
	
%>
<!--Edit Profile modal-->
<div class="modal fade" id="modalProfile" role="dialog" tabindex="-1" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h1 class="text-left">Edit Profile</h1>
			</div>
			<div class="modal-body">


				<form id="editProfile" class="form-horizontal editProfile" action="editProfile.html" method="POST">
					<!-- This should be a POST but as we dont save anything it is not added -->
  						<input id="oldAddress" name="oldAddress" type="hidden" value= <%=address %> >
  						<input id="oldCity" name="oldCity" type="hidden" value= <%=city %> >
  						<input id="oldZip" name="oldZip" type="hidden" value= <%=zip %> >

					<fieldset>

						<!-- Name-->
						<div class="form-group">
							<label class="col-md-3  control-label" for="Name">Name</label>
							<div class="col-md-8">
								<input name="name" class="form-control" type="text"
									placeholder=<%=user.getName()%> />
							</div>
						</div>

						<!-- Email-->
						<div class="form-group">
							<label class="col-md-3  control-label" for="email1">New
								Email</label>
							<div class="col-md-8">
								<input id="email1" name="email1"
									placeholder=<%=user.getEmail()%> class="form-control input-md"
									type="text">
							</div>
						</div>

						<!-- Email Verification-->
						<div class="form-group">
							<label class="col-md-3  control-label" for="email2">Verify
								your new email</label>
							<div class="col-md-8 ">
								<input id="email2" name="email2" placeholder="Verify your new email"
									class="form-control input-md" type="text">
							</div>
						</div>

						<!-- Password input-->
						<div class="form-group">
							<label class="col-md-3 control-label" for="passwordinput1">New
								Password</label>
							<div class="col-md-8">
								<input id="passwordinput1" name="passwordinput1"
									placeholder="Select your new password"
									class="form-control input-md" type="password">
							</div>
						</div>

						<!-- Password verification-->
						<div class="form-group">
							<label class="col-md-3 control-label" for="passwordinput2">Verify
								your new password</label>
							<div class="col-md-8">
								<input id="passwordinput2" name="passwordinput2" placeholder="Verify your new password"
									class="form-control input-md" type="password">
							</div>
						</div>

					</fieldset>

					<fieldset>


						<legend>
							<small>Address Information</small>
						</legend>

						<!-- Address-->
						<div class="form-group">
							<label class="col-md-3 col-md-pull-1 control-label" for="address">Address</label>
							<div class="col-md-9 col-md-pull-1">
								<input id="address" name="address" class="form-control"	type="text" placeholder=<%=address %> />  
									<span class="help-block">Please enter your address</span>
							</div>
						</div>

						<!-- City-->
						<div class="form-group">
							<label class="col-md-2 control-label" for="city">City</label>
							<div class="col-md-4">
								<input id="city" name="city" placeholder=<%=city %>
									class="form-control input-md" type="text"> <span
									class="help-block">Please enter the city where you live</span>
							</div>


							<!-- ZIP-->

							<label class="col-md-3 col-md-pull-1 control-label" for="zip">ZIP
								Code</label>
							<div class="col-md-3 col-md-pull-1 ">
								<input id="zip" name="zip" placeholder=<%=zip %>
									class="form-control input-md" type="text"> <span
									class="help-block">Please enter your zip code</span>
							</div>
						</div>

					</fieldset>




					<!-- Password verify changes-->
					<div class="form-group">
						<label class="col-md-3 control-label" for="passwordorg">To apply any change please enter your actual password</label>
						<div class="col-md-8">
							<input id="originalPassword" name="originalPassword" placeholder="Enter your actual password" class="form-control input-md" type="password">
						</div>
					</div>
					
					<div class="modal-footer">
						<!-- Button (Double) -->
		
						<div class="col-md-8">
							<button id="save" name="save" type="submit" class="btn btn-primary">Save</button>
							<button id="cancel" name="cancel" class="btn btn-inverse"
								data-dissmis="modal">Cancel</button>
						</div>
					</div>

				</form>
			</div>

			
		</div>




	</div>


</div>

