<%@page import="model.Category"%>
<%@page import="java.util.List"%>
<%@page import="db.CategoryManager"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div>
<br>
<br>
<br>
	<div class="list-group">
                    <a href="subscriptions.html?onSale=true" class="list-group-item" style="text-align:left; background-color:#003366; color:#FFFFFF ">On Sale</a>
                    <a id="all" href="subscriptions.html?category=all" class="list-group-item" data-toggle="modal" style="text-align:left">All Products</a>
                    <br>
                    <%
                	CategoryManager manager = new CategoryManager();
                	List<Category> categories = manager.getCategoryList();
                	//List<Category> categories = (List<Category>) session.getAttribute("categories");
                	for(Category category : categories){
                		String name = category.getName().toString();
                		String link = "subscriptions.html?category="+name;
                %>
                	
                    <a href=<%=link %> class="list-group-item" style="text-align:left"><%=name%></a>
                     <% 	} %>
                     
                    <br>
	</div>
</div>

<jsp:include page="addProduct.jsp"></jsp:include>