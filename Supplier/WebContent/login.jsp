<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Ultraceramics</title>

<!-- Bootstrap Core CSS 
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/shop-homepage.css" rel="stylesheet">
    <!-- Cart CSS -->
    <link href="css/cart.css" rel="stylesheet">
    
     <link href="css/bootstrap.css" rel="stylesheet">
</head>
	<body>
	<jsp:include page="header_login.jsp"></jsp:include>
	
<!--login modal-->

  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <h1 class="text-center">Supplier Login</h1>
          <p id="demo"></p>
      </div>
      <div class="modal-body">
          <form class="form col-md-12 center-block" method="POST" action="doLogin.html">
            <div class="form-group">
              <input name="username" type="text" class="form-control input-lg" placeholder="Email">
            </div>
            <div class="form-group">
              <input name="password" type="password" class="form-control input-lg" placeholder="Password">
            </div>
            <div class="form-group">
              <button class="btn btn-primary btn-lg btn-block" type="submit" >Sign In</button>
            </div>
          </form>
      </div>
      <div class="modal-footer"></div>
      
  
  </div>
</div>

<jsp:include page="footer.jsp"></jsp:include>



	<!-- script references -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>	
	
	</body>
</html>