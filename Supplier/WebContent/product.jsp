<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="css/bootstrap.css" rel="stylesheet">
</head>
<body>
<jsp:include page="header.jsp"></jsp:include>
	
         <div class= "row" style="margin-left:40px;">   	
             <div class="col-md-9 jumbotron">
            
            	<div class= "row">
            		<div class = "span">
            			<div class="col-md-5">
            			<img src="images/Prueba.jpeg" alt="" class="panel">
            			</div>	
         
            			<div class="col-md-3">
            				<h3>Product Name</h3>
            			</div>
            			<div class="col-md-4">
            				<h3>Product Type: #Type1</h3>
            			</div>           
            		</div>

            			<div class= "col-md-3 col-md-push-4">
            				<h3>Price: 23,50€</h3>
            			</div>
            				<div class= "col-md-5 col-md-push-4">
            						<h3 style="color:#33CC33">Available Items: #</h3>
            				</div>		
            </div>	
            	
            	<div class= "row">
            		<div class= "col-md-4">
            			<div class="ratings">
                                <p> Rating:
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                </p>
                            </div>
            		</div>
            	</div>
            	<div class = "row">
            		<div class = "col-md-4">
            			<h3>Supplier name</h3>
            		</div>
            		<div class = "col-md-12 panel">
            				<h3>Description:</h3>
            			<div class="panel-body" style="overflow:auto;">	
            				This is
            				a general
            				test
            				description 
            				for the
            				description panel
            				to see
            				if
            				it works
            				properly
            				This is
            				a general
            				test
            				description 
            				for the
            				description panel
            				to see
            				if
            				it works
            				properly
            				This is
            				a general
            				test
            				description 
            				for the
            				description panel
            				to see
            				if
            				it works
            				properly
            				This is
            				a general
            				test
            				description 
            				for the
            				description panel
            				to see
            				if
            				it works
            				properly
            				This is
            				a general
            				test
            				description 
            				for the
            				description panel
            				to see
            				if
            				it works
            				properly
            				This is
            				a general
            				test
            				description 
            				for the
            				description panel
            				to see
            				if
            				it works
            				properly
            				
            			</div>
            		</div>	
            	</div>
            	<a class="btn btn-warning btn-sm" type="submit" href="#edit_product_supplier" data-toggle="modal">Edit Product</a>
         	</div>
 
       </div>
       
<jsp:include page="editProduct.jsp" />           

<jsp:include page="footer.jsp"></jsp:include>

</body>
</html>