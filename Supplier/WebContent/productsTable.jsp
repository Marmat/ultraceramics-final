<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashSet"%>
<%@page import="java.util.Set"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ page import="model.*"%>
<jsp:useBean id="productsuppliedList" scope="request" class="model.ProductsuppliedList" />


<div class="row" id="searchBar">
	<div class="col-md-3">
		<% 
			String header = request.getParameter("category");
			if(header==null || header.compareTo("all")==0)
				header = "All Products";
			String onSale = request.getParameter("onSale");
			if(onSale!=null)
				header = "On Sale";
		%>
		<h2><%=header %></h2>
	</div>
	<div class="col-md-9">
		<div id="searchDiv" style="margin-top: 20px">
			<p style="float: left; padding-top: 6px;">Search for:</p>
			<form style="float: left; padding-left: 20px;" method="POST" action="searchProduct.html">
				<input id = "searchValue" name="searchValue" type=text>
				<select id = "searchCategory" name="searchCategory" class="selectpicker"
					style="margin-left: 10px">
					<option value="name" selected="selected">Name</option>
					<option value="quantity" selected="selected">Quantity</option>
					<option value="sellPrice" selected="selected">Price</option>
					<option value="minPrice" selected="selected">MinPrice</option>
					<option value="maxPrice" selected="selected">MaxPrice</option>
				</select>
				<button style="margin-left: 10px;" class="btn btn-default" type = "submit">Search!</button>
			</form>
		</div>
	</div>
</div>

<div class="table-scrollbar">
	<table class="table table-stripped table-bordered text-center">
		<thead>
		<%
			String categoryName = (String) request.getParameter("category");
			String sortByNameLink, sortByPriceLink, sortByMinPriceLink, sortByMaxPriceLink, sortByOnSaleLink, sortByQuantityLink;
			sortByNameLink = sortByPriceLink = sortByMinPriceLink = sortByMaxPriceLink = sortByOnSaleLink = sortByQuantityLink = "#";
			if(productsuppliedList.size() != 0){
				session.setAttribute("productsuppliedList", productsuppliedList);
				sortByNameLink = "sort?mode=name&ascendent=true"; // + categoryName;
				sortByPriceLink = "sort?mode=price&ascendent=true"; // + categoryName;
				sortByMinPriceLink = "sort?mode=minPrice&ascendent=true"; // + categoryName;
				sortByMaxPriceLink = "sort?mode=maxPrice&ascendent=true";// + categoryName;
				sortByOnSaleLink = "sort?mode=onSale&ascendent=true" ;//+ categoryName;
				sortByQuantityLink = "sort?mode=quantity&ascendent=true";// + categoryName;
			}
		%>
			<tr>
				<th><a href=<%=sortByNameLink %>>Name</a></th>
				<th><a href=<%=sortByPriceLink %>>Price</a></th>
				<th><a href=<%=sortByMinPriceLink %>>MinPrice</a></th>
				<th><a href=<%=sortByMaxPriceLink %>>MaxPrice</a></th>
				<th><a href=<%=sortByOnSaleLink %>>On sale?</a></th>
				<th><a href=<%=sortByQuantityLink %>>Quantity</a></th>
				<th>Tools</th>
			</tr>
		</thead>
		<tbody>
		<%
			Supplier user = (Supplier) session.getAttribute("user");
			int ID = user.getId();
			
			if(productsuppliedList.size()!=0) {
				for (Productsupplied ps : productsuppliedList) {
					if(ps.getActive()==true){
					/*
					List<Productsupplied> supplied = new ArrayList<Productsupplied>();
					supplied = product.getProductsupplieds();
					for (Productsupplied ps : supplied) {
						if (ps.getSupplier().getId() == ID) {
							*/
							String productLink = "product?productID="+ ps.getProduct().getIdProduct()+"&supplierID=" + ID;
							//print data
			%>
							<tr>
								<td><a href=<%=productLink %>><%=ps.getProduct().getName()%></a></td>
								<td><%=ps.getSellPrice() %></td>
								<td><%=ps.getMinPrice() %></td>
								<td><%=ps.getMaxPrice() %></td>
								<td><%=ps.getOnSale() %></td>
								<td><%=ps.getQuantity() %></td>
								<%
									String link = "?productID=" + ps.getProduct().getIdProduct();
									String editLink = "editProduct.html" + link;
									String deleteLink = "doDeleteProduct.html" + link;
								%>
								<td><a href=<%=editLink %> data-toggle="modal">
										<button class="btn btn-default ">
											<span class="glyphicon glyphicon-edit"></span> Edit
										</button>
								</a> <a href=<%=deleteLink %> data-toggle="modal">
										<button class="btn btn-default">
											<span class="glyphicon glyphicon-trash"></span> Delete
										</button>
								</a></td>
							</tr>							
			<%				}
						}
					}
				/*}
			}		*/	
		%>
		</tbody>
	</table>
</div>


<jsp:include page="deleteProduct.jsp" />