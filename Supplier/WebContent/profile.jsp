<%@page import="model.Supplier"%>
<%
	Supplier user = (Supplier) session.getAttribute("user");

	String string = user.getAddress();
	//string = string.replaceAll(" ", "_");
	String[] parts = string.split(",");
	String address = null;
	String zip = null;
	String city = null;
	if(parts.length==3){
	 address = parts[0];
	 zip = parts[1];
		city = parts[2];
	}
	
%>
<html>
<head>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/styles.css" rel="stylesheet">
<link href="css/simple-sidebar.css" rel="stylesheet">
</head>
<body>

	<jsp:include page="/header.jsp" />

	 <div class="container">
	 <div class="row">
	 	<div class="col-md-2">
	 		<div class="row">
	 			<jsp:include page="leftBarProfile.jsp"></jsp:include>
	 		</div>
	 	</div>
	 </div>
  <div class="col-md-7 col-md-offset-3 jumbotron">
 <form id="profileForm" class="form-horizontal profileForm">
					<!-- This should be a POST but as we dont save anything it is not added -->
					
					<fieldset>
						<legend>Supplier Information</legend>
						<!-- First Name-->
						<div class="form-group">
							<label class="col-md-2 control-label">First
								Name</label>
							<div class="col-md-4 form-control-static">
								<%=user.getName() %>
						
							</div>
						</div>

						<!-- Email-->
						<div class="form-group">
							<label class="col-md-2  control-label">Email</label>
							<div class="col-md-8 form-control-static">
								<%=user.getEmail() %>
							</div>
						</div>
						
						<!-- Address-->
						<div class="form-group">
							<label class="col-md-2  control-label">Address</label>
							<div class="col-md-8 form-control-static">
								<%=address %>
							</div>
						</div>
						
						<!-- City-->
						<div class="form-group">
							<label class="col-md-2  control-label">City</label>
							<div class="col-md-8 form-control-static">
								<%=city %>
							</div>
						</div>
						
						<!-- ZIP-->
						<div class="form-group">
							<label class="col-md-2  control-label">ZIP</label>
							<div class="col-md-8 form-control-static">
								<%=zip %>
							</div>
						</div>

						
						</fieldset>
						
						
					<a href = "#modalProfile" data-toggle="modal"><button class="btn btn-default">Edit</button></a>
				</form>
				
				</div>
				</div>
				<jsp:include page="editProfile.jsp"></jsp:include>
	<jsp:include page="/footer.jsp" />
	<script
		src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="js/bootstrap.js"></script>

</body>
</html>