<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Register</title>
</head>
<body>

	<jsp:include page="header_login.jsp"></jsp:include>

	<script>
		$(document)
				.ready(
						function() {
							$('.registerForm')
									.bootstrapValidator(
											{
												// To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
												message : 'This value is not valid',
												live : 'enabled',
												excluded : ':hidden',

												feedbackIcons : {
													valid : 'glyphicon glyphicon-ok-circle',
													invalid : 'glyphicon glyphicon-remove-circle',
													validating : 'glyphicon glyphicon-refresh'
												},
												fields : {
													email1 : {
														group : '.col-md-8',
														validators : {
															notEmpty : {
																message : 'The email address is required and cannot be empty'
															},
															emailAddress : {
																message : 'The email address is not a valid'
															}
														}
													},
													email2 : {
														group : '.col-md-8',
														validators : {
															notEmpty : {
																message : 'The email address verification is required and cannot be empty'
															},
															emailAddress : {
																message : 'The email address verification is not a valid email address'
															},
															identical : {
																field : 'email1',
																message : 'The email address and the verification must be the same'
															}

														}
													},
													passwordinput1 : {
														group : '.col-md-8',
														validators : {
															notEmpty : {
																message : 'The passord is required and cannot be empty'
															}
														}
													},
													passwordinput2 : {
														group : '.col-md-8',
														validators : {
															notEmpty : {
																message : 'The verification of the passord is required and cannot be empty'
															},
															identical : {
																field : 'passwordinput1',
																message : 'The password and its verification must be the same'
															}
														}
													},
													zip : {
														group : '.col-md-3',
														validators : {
															numeric : {
																message : 'The  ZIP code has to be numeric'
															},
															stringLength : {
																min : 5,
																max : 5,
																message : 'The ZIP code has to have length 5'
															}

														}
													}
												}
											});
						});
	</script>




	<!--Register modal-->

	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h1 class="text-left">Register</h1>
			</div>
			<div class="modal-body">


				<form id="registrationForm" class="form-horizontal registerForm"
					action="doRegister.html" method="POST">
					<!-- This should be a POST but as we dont save anything it is not added -->

					<fieldset>

						<!-- Name-->
						<div class="form-group">
							<div class="group">
								<label class="col-md-2 control-label">Name*</label>
								<div class="col-md-4">
									<input name="name" class="form-control" type="text" />

								</div>
							</div>
						</div>


						<!-- Email-->
						<div class="form-group">
							<label class="col-md-3  control-label" for="email1">Email*</label>
							<div class="col-md-8">
								<input id="email1" name="email1" placeholder="asdf@asdf.com"
									class="form-control input-md" type="text">
							</div>
						</div>

						<!-- Email Verification-->
						<div class="form-group">
							<label class="col-md-3  control-label" for="email2">Verify
								your email*</label>
							<div class="col-md-8 ">
								<input id="email2" name="email2" placeholder=""
									class="form-control input-md" type="text">
							</div>
						</div>

						<!-- Password input-->
						<div class="form-group">
							<label class="col-md-3 control-label" for="passwordinput1">Password*</label>
							<div class="col-md-8">
								<input id="passwordinput1" name="passwordinput1"
									placeholder="Select your password"
									class="form-control input-md" type="password">
							</div>
						</div>

						<!-- Password verification-->
						<div class="form-group">
							<label class="col-md-3 control-label" for="passwordinput2">Verify
								your password*</label>
							<div class="col-md-8">
								<input id="passwordinput2" name="passwordinput2" placeholder=""
									class="form-control input-md" type="password">
							</div>
						</div>

						<hr>

						<h3>
							<small>Address Information</small>
						</h3>

						<!-- Address-->
						<div class="form-group">
							<label class="col-md-3 col-md-pull-1 control-label" for="address">Address</label>
							<div class="col-md-9 col-md-pull-1">
								<input id="address" name="address" placeholder="Fake str 123"
									class="form-control input-md" type="text"> <span
									class="help-block">Please enter your address</span>
							</div>
						</div>

						<!-- City-->
						<div class="form-group">
							<label class="col-md-2 control-label" for="city">City</label>
							<div class="col-md-4">
								<input id="city" name="city" placeholder="Madrid"
									class="form-control input-md" type="text"> <span
									class="help-block">Please enter the city where you live</span>
							</div>


							<!-- ZIP-->

							<label class="col-md-3 col-md-pull-1 control-label" for="zip">ZIP
								Code</label>
							<div class="col-md-3 col-md-pull-1 ">
								<input id="zip" name="zip" placeholder="28123"
									class="form-control input-md" type="text"> <span
									class="help-block">Please enter your zip code</span>
							</div>
						</div>

						<!-- Button (Double) -->
						<div class="form-group">
							<label class="col-md-4 control-label" for="register"></label>
							<div class="modal-footer">
								<!-- <a href="login.html"><button id="cancel" name="cancel" class="btn btn-inverse">Cancel</button></a> -->
								<button id="register" name="register" type="submit" class="btn btn-primary">Register</button>
							</div>
						</div>

					</fieldset>
				</form>


			</div>


		</div>
	</div>


</body>
</html>