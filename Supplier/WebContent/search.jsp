<%@page import="model.Category"%>
<%@page import="java.util.List"%>
<%@page import="db.CategoryManager"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class = "modal fade" id = "search_product_supplier" role = "dialog">					
					<div class = "modal-dialog">
							<div class = "modal-content">
								<form class = "form-horizontal" method="POST" action="searchProduct.html">
									<div class = "modal-header">
										<h4>Search product</h4>
									</div>
									<div class = "modal-body">
									
										<div class = "form-group">
											<label for = "name" class = "col-lg-2 control-label">Product name:</label>
											<div class = "col-lg-10">
												<input id="name" name="name" class="form-control input-md" type="text">
											</div>
										</div>
								   
										<div class = "form-group">
											<label for = "minPrice" class = "col-md-2 control-label">Min price:</label>
											<div class = "col-md-4">
												<input id="minPrice" name="minPrice" class="form-control input-md" type="text">
											</div>
											<label for = "maxPrice" class = "col-md-2 control-label">Max price:</label>
											<div class = "col-md-4">
												<input id="maxPrice" name="maxPrice" class="form-control input-md" type="text">
											</div>
										</div>
										
										<div class = "form-group">
											<label for = "quantity" class = "col-md-2 control-label">Quantity:</label>
											<div class = "col-md-4">
												<input id="quantity" name="quantity" class="form-control input-md" type="text">
											</div>
											
										</div>
										
										<div class = "form-group">
											<label for = "productType" class = "col-lg-2 control-label">Product type:</label>
											<div class = "col-lg-4">
												<select class="selectpicker" style="margin-top:10px" id="searchCategory" name="searchCategory">
														<option value="" selected="selected">Select category</option>
													<%
														CategoryManager manager = new CategoryManager();
									                	List<Category> categories = manager.getCategoryList();
									                	for(Category category : categories){
									                		String name = category.getName().toString();
									                %>
									                	<option value=<%=name %> ><%=name %></option>
									                     <% 	} %>
														
														
												</select> 
											</div>
											
										</div>
										

								   
									</div>
									<div class = "modal-footer">
										<a class = "btn btn-default" data-dismiss = "modal">Cancel</a>    
										<button class = "btn btn-primary" type = "submit">Save</button>
									</div>
								</form>
							</div>
					</div>
					
</div>					