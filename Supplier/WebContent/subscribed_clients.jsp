<%@page import="java.util.Calendar"%>
<%@page import="model.Product"%>
<%@page import="model.Productsupplied"%>
<%@page import="db.ProductManager"%>
<%@page import="model.PurchasePK"%>
<%@page import="java.util.Date"%>
<%@page import="model.Purchase"%>
<%@page import="java.util.List"%>
<%@page import="model.Order"%>
<%@page import="model.Client"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	
	<jsp:useBean id="productsuppliedList" scope="request" class="model.ProductsuppliedList" />
	<jsp:useBean id="clientList" scope="request" class="model.ClientList" />
	<html>
<head>


<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Ultraceramics</title>

</head>

<body>

	<jsp:include page="header.jsp"></jsp:include>

	<!-- Page Content -->
	<div class="container">

		<div class="row">
			<%
			ProductManager pm = new ProductManager();
			for (Productsupplied product : productsuppliedList) {

		//		int productID = key.getProductID();
				
			%>

			<div class="col-md-12 panel height:500px">
				<div class="panel-body" style="overflow: auto;">
					

					
					<p>Product:</p>
					
					<%=product.getProduct().getIdProduct()%>

					<div class="table-scrollbar">
						<table class="table table-stripped">
							<thead>
								<tr>
									<th>Clients</th>
								</tr>
							</thead>
							<tbody>
								<%
							
								 for (Client client : clientList) {
										
								%>
								
								<tr>
									<td><%=client.getId()%></td>
								</tr>

								<%
									}
								%>

							</tbody>
						</table>
					</div>
					
							<%
								}
							%>
					
				</div>
				
			</div>





		</div>
	</div>

	<jsp:include page="footer.jsp"></jsp:include>
</body>

</html>
