<html>
<head>
<title>Ultraceramics</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/styles.css" rel="stylesheet">
<link href="css/simple-sidebar.css" rel="stylesheet">

<jsp:useBean id="productsuppliedList" scope="request" class="model.ProductsuppliedList" />
<jsp:useBean id="subsNumber" scope="request" class="model.IntList" />
<jsp:useBean id="subscribers" scope="request" class="model.StringList" />


</head>
<body>

	<jsp:include page="/header.jsp" />

	<div class="container">
		<div class="row">

			<div class="col col-md-2">
				<jsp:include page="left_bar_subs.jsp" />
			</div>
			<div class="col col-md-10">
				<%@page import="java.util.List"%>
				<%@page import="java.util.ArrayList"%>
				<%@page import="java.util.HashSet"%>
				<%@page import="java.util.Set"%>
				<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
				<%@ page import="model.*"%>
				<%//ProductsuppliedList productsuppliedList = (ProductsuppliedList) session.getAttribute("productsuppliedList"); %>
				<%//int[] subsNumber = (int[]) session.getAttribute("subsNumber"); %>
				<%//String[] subscribers = (String[]) session.getAttribute("subscribers"); %>
				
				<div class="row">
					<div class="col-md-3">
						<% 
							String header = request.getParameter("category");
							if(header==null || header.compareTo("all")==0)
								header = "All Products";
							String onSale = request.getParameter("onSale");
							if(onSale!=null)
								header = "On Sale";
						%>
						<h2><%=header %></h2>
					</div>
				</div>
								
				<div class="table-scrollbar">
					<table class="table table-stripped table-bordered text-center">
						<thead>
							<tr>
								<th>Product</th>
								<th>Subscriptions</th>
								<th>subscribers</th>
							</tr>
						</thead>
						<tbody>
											<%
												int i =0;
												if(productsuppliedList.size()!=0) {
													for (Productsupplied ps : productsuppliedList){
														if(ps.getActive()==true){
											%>
															<tr> 
																<td><p class="text-left"><%=ps.getProduct().getName() %></p></td>
																<td><p class="text-left"><%=subsNumber.get(i) %></p></td>
																<td><p class="text-left"><%=subscribers.get(i) %></p></td>
															</tr>	
											<%
														}
													i++;
													}
												}
											%>						
						</tbody>
					</table>
				</div>
				
			</div>

		</div>
	</div>
	<jsp:include page="/footer.jsp" />
	<script
		src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="js/bootstrap.js"></script>

</body>
</html>
