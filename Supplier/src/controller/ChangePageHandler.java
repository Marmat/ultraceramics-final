package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.*;
import db.CategoryManager;
import db.ProductManager;
import db.SupplierManager;

public class ChangePageHandler implements RequestHandler {

	/**
	 * Verifies each case and redirects on the good one
	 */
	HttpSession session = null;

	@Override
	public String handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		session = request.getSession();
		String path = request.getServletPath();
		
			if(checkLogin()==true){
				switch (path) {
				case "/logout.html":
					return doLogout();
				case "/profile.html":
					return "/profile.jsp";
				case "/editProduct.html":
					return editProduct(request,response);
				case "/doEditProduct.html":
					return doEditProduct(request,response);
				case "/doAddProduct.html":
					return doAddProductsupplied(request,response);
				case "/doDeleteProduct.html":
					return doDeleteProduct(request,response);
				case "/product.html":
					return "/product.jsp";
				case "/searchProduct.html":
					return doSearch(request);
				case "/category.html":
					return manageCategory(request);
				case "/editProfile.html":
					return doEditProfile(request, response);
				case "/subscriptions.html":
					return subscriptions(request);
				case "/sort":
					return sort(request);
				}
				return "/category.html";
			}
		
			switch (path) {
			case "/doLogin.html":
				return login(request.getParameter("username"), request.getParameter("password"));
			case "/login.html":
				return "/login.jsp";
			case "/register.html":
				return "/register.jsp";			
			case "/doRegister.html":
				return register(request, response);
			}
		return "/login_supplier.jsp";

	}

	private boolean checkLogin() {
		if (session.getAttribute("user") == null) {
			return false;
		}
		return true;
	}
	
	private String sort(HttpServletRequest request) {
		ProductManager pm = new ProductManager();
		ProductsuppliedList productList = (ProductsuppliedList) session.getAttribute("productsuppliedList");
		String mode = request.getParameter("mode");
		String ascendendParameter = request.getParameter("ascendent");
		boolean ascendent = ascendendParameter.equals("true");
		request.setAttribute("productsuppliedList", pm.orderProducts(productList, mode, ascendent));
		return "/products.jsp";
	}
	
	private String login(String username, String password) {
		if ((password.length() == 0) || (username.length() == 0)) {
			session.setAttribute("error", true);
			return "/login.jsp";
		} else {
			SupplierManager sm = new SupplierManager();
			Supplier supplier = sm.login(username, password);
			if (supplier == null) {
				session.setAttribute("error", true);
				return "/login.jsp";
			}
			session.setAttribute("user", supplier);
			return "/category.html?category=all";
		}
	}

	private String doLogout() {
		session.setAttribute("user", null);
		return "/login.jsp";
	}
	
	private String editProduct(HttpServletRequest request,HttpServletResponse response){
		String productID = (String) request.getParameter("productID");
		Supplier user = (Supplier) session.getAttribute("user");
		String supplierID = user.getId()+"";
		request.setAttribute("productID", productID);
		ProductManager pm = new ProductManager();
		Productsupplied product = pm.retriveProductToEdit(productID, supplierID);
		request.setAttribute("productSuppliedToEdit", product);
		return "/editProduct.jsp";
	}
	
	private String doEditProduct(HttpServletRequest request,HttpServletResponse response){
		System.out.println("\n\n#########  doEditProduct  ##########\n\n");
		String quantity = (String) request.getParameter("quantity");
		String productID = (String) request.getParameter("productID");
		Supplier user = (Supplier) session.getAttribute("user");
		
		if(quantity.isEmpty()==true)
			return "/category.html";
		String supplierID = user.getId()+"";
		ProductManager pm = new ProductManager();
		pm.editProductSuppliedQuantity(supplierID, productID, quantity);
		System.out.println("\n\n#########  END doEditProduct  ##########\n\n");
		return "/category.html";
	}
	
	private String doAddProductsupplied(HttpServletRequest request,HttpServletResponse response){
		System.out.println("\n\n#########  doAddProduct  ##########\n\n");
		String description = (String) request.getParameter("description");
		String name = (String) request.getParameter("name");
		String minPrice = (String) request.getParameter("minPrice");
		String maxPrice = (String) request.getParameter("maxPrice");
		String quantity = (String) request.getParameter("quantity");
		String category = (String) request.getParameter("category");
		
		System.out.println("\n\n#########  description = " + description + "  ##########\n\n");
		System.out.println("\n\n#########  name = " + name + "  ##########\n\n");
		System.out.println("\n\n#########  minPrice = " + minPrice + "  ##########\n\n");
		System.out.println("\n\n#########  maxPrice = " + maxPrice + "  ##########\n\n");
		System.out.println("\n\n#########  quantity = " + quantity + "  ##########\n\n");
		System.out.println("\n\n#########  category = " + category + "  ##########\n\n");
		
		
		Supplier user = (Supplier) session.getAttribute("user");
		ProductManager pm = new ProductManager();
		CategoryManager cm = new CategoryManager();
		
		System.out.println("\n\n#########  create_new_Productsupplied  ##########\n\n");
		Productsupplied newProductsupplied = new Productsupplied();
		newProductsupplied.setActive(true);
		newProductsupplied.setFixedRate(0);
		newProductsupplied.setMaxPrice(maxPrice);
		newProductsupplied.setMinPrice(minPrice);
		newProductsupplied.setOnSale(false);
		newProductsupplied.setPercentage(0);
		newProductsupplied.setProduct(null);
		newProductsupplied.setPurchases(null);
		newProductsupplied.setQuantity(quantity);
		newProductsupplied.setSellPrice(maxPrice);
		newProductsupplied.setSupplier(user);
		System.out.println("\n\n#########  END create_new_Productsupplied  ##########\n\n");
		
		Product product =pm.getProduct(name);
		if(product == null){
			System.out.println("\n\n#########  create_new_Product  ##########\n\n");
			product = new Product();
			product.setCategory(cm.getCategory(category));
			product.setClients(null);
			product.setDescription(description);
			product.setImage("no_image");
			product.setName(name);
			ProductsuppliedList productsupplieds = new ProductsuppliedList();
			productsupplieds.add(null);
			product.setProductsupplieds(productsupplieds);
			System.out.println("\n\n#########  addProduct_to_DB  ##########\n\n");
			pm.addProduct(product);
			System.out.println("\n\n#########  END create_new_Product  ##########\n\n");
		}
		else{
			product.getProductsupplieds().add(newProductsupplied);
		}
		
		ProductsuppliedPK pk = new ProductsuppliedPK();
		pk.setProductID(pm.getProduct(name).getIdProduct());
		pk.setSupplierID(user.getId());
		System.out.println("\n\n#########  supplierID = "+pk.getSupplierID()+"  ##########\n\n");
		System.out.println("\n\n#########  productID = "+pk.getProductID()+"  ##########\n\n");		
		newProductsupplied.setId(pk);
		newProductsupplied.setProduct(pm.getProduct(name));
		pm.AddProductsupplied(newProductsupplied);
		
		System.out.println("\n\n#########  END doAddProduct  ##########\n\n");
		return "/category.html";
	}
	
	private String doDeleteProduct(HttpServletRequest request,HttpServletResponse response){
		System.out.println("\n\n#########  doDeleteProduct  ##########\n\n");
		String productID = (String) request.getParameter("productID");
		Supplier user = (Supplier) session.getAttribute("user");
		String supplierID = user.getId()+"";
		
		ProductManager pm = new ProductManager();
		Productsupplied productsupplied = pm.retriveProductToEdit(productID, supplierID);
		
		String category = productsupplied.getProduct().getCategory().getName();
		System.out.println("\n\n#########  category: "+category+"  ##########\n\n");
		
		pm.deleteProductsupplied(supplierID, productID);
		System.out.println("\n\n#########  END doDeleteProduct  ##########\n\n");
		return "/category.html?category=" + category;
	}

	private String register(HttpServletRequest request,	HttpServletResponse response) {
		System.out.println("\n\n#########  register  ##########\n\n");
		SupplierManager sm = new SupplierManager();
		Supplier supplier = new Supplier();

		String name = (String) request.getParameter("name");
		String email1 = (String) request.getParameter("email1");
		String email2 = (String) request.getParameter("email2");
		String password1 = (String) request.getParameter("passwordinput1");
		String password2 = (String) request.getParameter("passwordinput2");
		String address = (String) request.getParameter("address");
		String city = (String) request.getParameter("city");
		String zip = (String) request.getParameter("zip");
		
		if(email1.compareTo(email2)!= 0 || password1.compareTo(password2)!=0)
			return "/error.jsp";
		
		Supplier existingSupplier = sm.getSupplier(email1);
		if(existingSupplier != null){
			System.out.println("\n\n#########  email_is_already_in_DB  ##########\n\n");
			return "/error.jsp"; 
		}
		
		if(address.isEmpty()) address = "null";
		if(city.isEmpty()) city = "null";
		if(zip.isEmpty()) zip = "null";
		
		supplier.setName(name);
		supplier.setEmail(email1);
		supplier.setPassword(password1);
		supplier.setAddress(address+","+zip+","+city);
		supplier.setActive(true);
		
		System.out.println("\n\n#########  name: "+supplier.getName()+"  ##########\n\n");
		System.out.println("\n\n#########  email: "+supplier.getEmail()+"  ##########\n\n");
		System.out.println("\n\n#########  address: "+supplier.getAddress()+"  ##########\n\n");
		System.out.println("\n\n#########  password: "+supplier.getPassword()+"  ##########\n\n");
		System.out.println("\n\n#########  active: "+supplier.getActive()+"  ##########\n\n");

		if (sm.register(supplier)) {
			return "/login.jsp";
		}
		System.out.println("\n\n#########  END_register  ##########\n\n");
		return "/error.jsp";
	}
	
	private String doEditProfile(HttpServletRequest request, HttpServletResponse response) {
		
		System.out.println("\n\n########  edit_profile_function ########\n\n");
		
		SupplierManager sm = new SupplierManager();
		Supplier oldSupplier = (Supplier) session.getAttribute("user");
		
		Supplier newSupplier = new Supplier();
		newSupplier.setActive(oldSupplier.getActive());
		newSupplier.setAddress(oldSupplier.getAddress());
		newSupplier.setEmail(oldSupplier.getEmail());
		newSupplier.setId(oldSupplier.getId());
		newSupplier.setName(oldSupplier.getName());
		newSupplier.setPassword(oldSupplier.getPassword());
		newSupplier.setProductsupplieds(oldSupplier.getProductsupplieds());
		
		System.out.println("\n\n########  get_session_attributes ########\n\n");
		
		String name = (String) request.getParameter("name");
		String email1 = (String) request.getParameter("email1");
		String email2 = (String) request.getParameter("email2");
		String password1 = (String) request.getParameter("passwordinput1");
		String password2 = (String) request.getParameter("passwordinput2");
		String address = (String) request.getParameter("address");
		String city = (String) request.getParameter("city");
		String zip = (String) request.getParameter("zip");
		String originalPassword = (String) request.getParameter("originalPassword");		
		
		System.out.println("\n\n########  get_form_attributes ########\n\n");
		
		String oldAddress = (String) request.getParameter("oldAddress");
		String oldCity = (String) request.getParameter("oldCity");
		String oldZip = (String) request.getParameter("oldZip");
		
		System.out.println("\n\n########  get_old_form_attributes ########\n\n");
		System.out.println("\n\n########  original_password: " + originalPassword + " ########\n\n");
		System.out.println("\n\n########  old_password: " + oldSupplier.getPassword() + " ########\n\n");
		if(   !(originalPassword.isEmpty()) &&  !(oldSupplier.getPassword().isEmpty())   )
		{
			if( originalPassword.compareTo(oldSupplier.getPassword())==0 )
			{
				System.out.println("\n\n########  password correct ########\n\n");
				if(name.isEmpty()) name = oldSupplier.getName();
				System.out.println("\n\n########  email1: " + email1 + " ########\n\n");
				System.out.println("\n\n########  email2: " + email2 + " ########\n\n");
				if(   !(email1.isEmpty()) &&  !(email2.isEmpty())   )
					if( email1.compareTo(email2)==0 )
					{
						System.out.println("\n\n######## UPDATE_EMAIL ########\n\n");
						if(email1.isEmpty() == false )
							newSupplier.setEmail(email1);
						
					}
				System.out.println("\n\n########  password1: " + password1 + " ########\n\n");
				System.out.println("\n\n########  password2: " + password2 + " ########\n\n");
				if(   !(password1.isEmpty()) &&  !(password2.isEmpty())   )
					if( password1.compareTo(password2)==0 )
					{
						System.out.println("\n\n######## UPDATE_PASSWORD ########\n\n");
						if(password1.isEmpty() == false)
							newSupplier.setPassword(password1);
					}
				if(address.isEmpty()) address = oldAddress;
				if(city.isEmpty()) city = oldCity;
				if(zip.isEmpty()) zip = oldZip;
				String compoundAddress = address+","+zip+","+city; compoundAddress = compoundAddress.replaceAll("_", " ");
				newSupplier.setAddress(compoundAddress);
				
				if(name.isEmpty() == false)
					newSupplier.setName(name);
				
				session.removeAttribute("user");
				session.setAttribute("user", newSupplier);
				if (sm.editSupplier(oldSupplier.getId(),newSupplier) != null) {
					return "/profile.jsp";
				}
			}
		}
		System.out.println("\n\n########  password INcorrect ########\n\n");
		// WE MUST CHANGE IT TO ERROR PAGE
		return "/profile.jsp";
	}
	
	private String manageCategory(HttpServletRequest request) {
		System.out.println("\n\n########  manageCategory ########\n\n");
		Supplier user = (Supplier) session.getAttribute("user");
		String categoryName = (String) request.getParameter("category");
		String onSale = (String) request.getParameter("onSale");
		String sortBy = (String) request.getParameter("sortBy");
		String sortOrder = (String) session.getAttribute("sortOrder");
		
		if(sortOrder == null)
			sortOrder = "ASC";
		
		ProductManager pm = new ProductManager();
		String supplierID = user.getId()+"";
		
		if(onSale == null){
			if(categoryName == null){
				categoryName = "all";
			}
			if(categoryName.compareTo("all") == 0){
				ProductsuppliedList productsuppliedList = pm.retriveProductsuppliedListFromAllCategories(supplierID);
				request.setAttribute("productsuppliedList", productsuppliedList);	
				/*if(sortBy != null){
					System.out.println("\n\n########  sortBySwitch ########\n\n");
					System.out.println("\n\n########  sortOrder: "+sortOrder+" ########\n\n");
					if(sortBy.compareTo("name")==0)
						session.setAttribute("productsuppliedList", pm.retriveProductsuppliedListFromCategorySort(supplierID,categoryName,"p.product.name",sortOrder));
					else if(sortBy.compareTo("price")==0)
						session.setAttribute("productsuppliedList", pm.retriveProductsuppliedListFromCategorySort(supplierID,categoryName,"p.sellPrice",sortOrder));
					else if(sortBy.compareTo("minPrice")==0)
						session.setAttribute("productsuppliedList", pm.retriveProductsuppliedListFromCategorySort(supplierID,categoryName,"p.minPrice",sortOrder));
					else if(sortBy.compareTo("maxPrice")==0)
						session.setAttribute("productsuppliedList", pm.retriveProductsuppliedListFromCategorySort(supplierID,categoryName,"p.maxPrice",sortOrder));
					else if(sortBy.compareTo("onSale")==0)
						session.setAttribute("productsuppliedList", pm.retriveProductsuppliedListFromCategorySort(supplierID,categoryName,"p.onSale",sortOrder));
					else if(sortBy.compareTo("quantity")==0)
						session.setAttribute("productsuppliedList", pm.retriveProductsuppliedListFromCategorySort(supplierID,categoryName,"p.quantity",sortOrder));
							
					if(sortOrder.compareTo("DESC")==0)
						sortOrder = "ASC";
					else
						sortOrder = "DESC";
					session.setAttribute("sortOrder", sortOrder);
					System.out.println("\n\n########  sortOrder: "+sortOrder+" ########\n\n");
				}*/
				System.out.println("\n\n########  END_manageCategory ########\n\n");
			}
			else{
				if(sortBy == null){
					ProductsuppliedList productsuppliedList = pm.retriveProductsuppliedListFromCategory(supplierID,categoryName);
					request.setAttribute("productsuppliedList", productsuppliedList);		
					System.out.println("\n\n########  END_manageCategory ########\n\n");
				}
				else{
					System.out.println("##################################################YES YOU FOUND THE ERROR######################################");
					/*
					System.out.println("\n\n########  sortBySwitch ########\n\n");
					System.out.println("\n\n########  sortOrder: "+sortOrder+" ########\n\n");
					if(sortBy.compareTo("name")==0)
						session.setAttribute("productsuppliedList", pm.retriveProductsuppliedListFromCategorySort(supplierID,categoryName,"p.product.name",sortOrder));
					else if(sortBy.compareTo("price")==0)
						session.setAttribute("productsuppliedList", pm.retriveProductsuppliedListFromCategorySort(supplierID,categoryName,"p.sellPrice",sortOrder));
					else if(sortBy.compareTo("minPrice")==0)
						session.setAttribute("productsuppliedList", pm.retriveProductsuppliedListFromCategorySort(supplierID,categoryName,"p.minPrice",sortOrder));
					else if(sortBy.compareTo("maxPrice")==0)
						session.setAttribute("productsuppliedList", pm.retriveProductsuppliedListFromCategorySort(supplierID,categoryName,"p.maxPrice",sortOrder));
					else if(sortBy.compareTo("onSale")==0)
						session.setAttribute("productsuppliedList", pm.retriveProductsuppliedListFromCategorySort(supplierID,categoryName,"p.onSale",sortOrder));
					else if(sortBy.compareTo("quantity")==0)
						session.setAttribute("productsuppliedList", pm.retriveProductsuppliedListFromCategorySort(supplierID,categoryName,"p.quantity",sortOrder));
							
					if(sortOrder.compareTo("DESC")==0)
						sortOrder = "ASC";
					else
						sortOrder = "DESC";
					session.setAttribute("sortOrder", sortOrder);
					System.out.println("\n\n########  sortOrder: "+sortOrder+" ########\n\n");
				*/}
			}
			
		}else{
			ProductsuppliedList productsuppliedList = pm.retriveProductsuppliedListOnSale(supplierID);
			request.setAttribute("productsuppliedList", productsuppliedList);		
			System.out.println("\n\n########  END_manageCategory ########\n\n");
		}
		return "/products.jsp";
	}
	
	private String subscriptions(HttpServletRequest request) {
		System.out.println("\n\n########  subscriptions  ########\n\n");
		Supplier user = (Supplier) session.getAttribute("user");
		ProductManager pm = new ProductManager();
		String categoryName = (String) request.getParameter("category");
		String onSale = (String) request.getParameter("onSale");
		String supplierID = user.getId()+"";
		ProductsuppliedList productsuppliedList = new ProductsuppliedList();
		
		if(onSale == null){
			if(categoryName == null){
				categoryName = "all";
			}
			if(categoryName.compareTo("all") == 0){
				productsuppliedList = pm.retriveProductsuppliedListFromAllCategories(supplierID);
				//session.setAttribute("productsuppliedList", productsuppliedList);		
			}
			else{
				
					productsuppliedList = pm.retriveProductsuppliedListFromCategory(supplierID,categoryName);
					//session.setAttribute("productsuppliedList", productsuppliedList);		
				
			}
			
		}else{
			productsuppliedList = pm.retriveProductsuppliedListOnSale(supplierID);
			//session.setAttribute("productsuppliedList", productsuppliedList);		
		}
		
		//ProductsuppliedList productsuppliedList = pm.retriveProductsuppliedListFromAllCategories(user.getId()+"");
		System.out.println("\n\n########  gotProductSuppliedList  ########\n\n");
		System.out.println("\n\n########  productSuppliedList SIZE: "+productsuppliedList.size()+" ########\n\n");
		
		
		IntList subsNumber = new IntList();
		StringList subscribers = new StringList();
		
		for(int i=0;i<productsuppliedList.size();i++){
			String productName = productsuppliedList.get(i).getProduct().getIdProduct()+"";
			subsNumber.add(pm.getSubsNumber(productName));
			subscribers.add(pm.getSubscribers(productName));
			System.out.println("\n\n########  productName: "+productName+" ########\n\n");
			System.out.println("\n\n########  subsNumber: "+subsNumber.get(i)+" ########\n\n");
		}		
		
		request.setAttribute("productsuppliedList", productsuppliedList);
		request.setAttribute("subsNumber", subsNumber);
		request.setAttribute("subscribers", subscribers);
		
		System.out.println("\n\n########  END_subscriptions  ########\n\n");
		return "subscriptions.jsp";
	}
	
	private String doSearch(HttpServletRequest request) {
		Supplier user = (Supplier) session.getAttribute("user");
		String searchValue = (String) request.getParameter("searchValue");
		String searchCategory = (String) request.getParameter("searchCategory");
		
		System.out.println("\n\n########  searchValue: "+searchValue+" ########\n\n");
		System.out.println("\n\n########  searchCategory: "+searchCategory+" ########\n\n");
		
		ProductManager pm = new ProductManager();
		System.out.println("\n\n########  doSearch ########\n\n");
		switch(searchCategory){
			case "name":{
				request.setAttribute("productsuppliedList", pm.retriveProductsuppliedSearched(user.getId(),"p.product.name",searchValue));
				return "/products.jsp";
			}
			case "quantity":{
				request.setAttribute("productsuppliedList", pm.retriveProductsuppliedSearched(user.getId(),"p.quantity",searchValue));
				return "/products.jsp";
			}
			case "sellPrice":{
				request.setAttribute("productsuppliedList", pm.retriveProductsuppliedSearched(user.getId(),"p.sellPrice",searchValue));
				return "/products.jsp";
			}
			case "minPrice":{
				request.setAttribute("productsuppliedList", pm.retriveProductsuppliedSearched(user.getId(),"p.minPrice",searchValue));
				return "/products.jsp";
			}
			case "maxPrice":{
				request.setAttribute("productsuppliedList", pm.retriveProductsuppliedSearched(user.getId(),"p.maxPrice",searchValue));
				return "/products.jsp";
			}
		}
		System.out.println("\n\n########  END doSearch ########\n\n");
		return "/products.jsp";
	}

}
