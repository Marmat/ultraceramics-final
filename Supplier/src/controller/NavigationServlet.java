package controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class NavigationServlet
 */

@WebServlet({ "/NavigationServlet", "/doLogin.html", "/products.html",
		"/product.html", "/profile.html", "/logout.html", "/addProduct.html",
		"/register.html", "/login.html", "/doRegister.html",
		"/editProfile.html", "/category.html", "/editProduct.html",
		"/doEditProduct.html", "/doDeleteProduct.html", "/doAddProduct.html", "/searchProduct.html", "/subscriptions.html" , "/sort" })
public class NavigationServlet extends HttpServlet {
	public static final String[] navigationPages = { "/doLogin.html",
			"/products.html", "/product.html", "/profile.html", "/logout.html",
			"/addProduct.html", "/register.html", "/login.html",
			"/doRegister.html", "/editProfile.html", "/category.html",
			"/editProduct.html", "/doEditProduct.html",
			"/doDeleteProduct.html", "/doAddProduct.html", "/searchProduct.html", "/subscriptions.html" , "/sort"};
	private static final long serialVersionUID = 1L;

	private Map<String, RequestHandler> pageMap;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public NavigationServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		mapPages();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String path = request.getServletPath();
		if (path != null) {
			RequestHandler handler = pageMap.get(path);
			if (handler != null) {
				String view = handler.handleRequest(request, response);
				RequestDispatcher rdp = request.getRequestDispatcher(view);
				rdp.forward(request, response);
			} else {
				/* Error page */
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String path = request.getServletPath();
		System.out.println("qui " + path);
		if (path != null) {
			RequestHandler handler = pageMap.get(path);
			if (handler != null) {
				String view = handler.handleRequest(request, response);
				RequestDispatcher rdp = request.getRequestDispatcher(view);
				rdp.forward(request, response);
			} else {
				/* Error page */
			}
		}
	}

	/*
	 * This method is used for mapping the controller. In the array
	 * navigationPages (on the top of the class) there are the pages to link to
	 * NavigationController. If you need to add a page, just add it there, it
	 * will be automatically mapped with the for each
	 */
	private void mapPages() {
		pageMap = new HashMap<String, RequestHandler>();
		for (String page : navigationPages) {
			pageMap.put(page, new ChangePageHandler());
		}
	}

}
