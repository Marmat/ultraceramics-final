package db;


import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import model.Category;



public class CategoryManager {
	private Properties properties;
	private String persistanceUnit = "Model";
	EntityManagerFactory emf;

	public CategoryManager() {
		setupProperties();
	}

	public List<Category> getCategoryList(){
		EntityManager em = emf.createEntityManager();
		Query query = (Query) em.createQuery("SELECT c FROM Category c ORDER BY c.name");
		List<?> queryResult = query.getResultList();
		List<Category> resultList = new ArrayList<Category>();
		for(Object o : queryResult){
			resultList.add((Category)o);
		}
		em.close();
		return resultList;
		
		
	}

	private void setupProperties() {
		properties = new Properties();
		/*
		 * properties.put(PersistenceUnitProperties.CLASSLOADER, this.getClass()
		 * .getClassLoader());
		 */
		/*properties.put(PersistenceUnitProperties.CLASSLOADER,
				this.getClass().getClassLoader());
		properties.put(PersistenceUnitProperties.ECLIPSELINK_PERSISTENCE_UNITS,
				persistanceUnit);*/
		emf = Persistence.createEntityManagerFactory(persistanceUnit,
				properties);
	}
	
	public Category getCategory(String name) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		
		String queryText = "SELECT c FROM Category c WHERE c.name = '"+name+"'";
		Query query = em.createQuery(queryText);
		List<?> queryResultList = query.getResultList();
		if(queryResultList.size()==0){
			return null;
		}
		Category category = (Category) queryResultList.get(0);
		
		em.getTransaction().commit();
		em.close();
		return category;
	}
}
