package db;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Properties;
import java.util.TreeSet;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import model.GenericProductInterface;
import model.ProductList;
import model.Productsupplied;
import model.ProductsuppliedPK;
import model.Subscription;
import model.Supplier;
import model.Product;
import model.ProductsuppliedList;

import org.eclipse.persistence.config.PersistenceUnitProperties;


public class ProductManager  {
	private Properties properties;
	private String persistanceUnit = "Model";
	EntityManagerFactory emf;
	
	public ProductManager(){

		setupProperties();
	}
	
	
	public ProductsuppliedList orderProducts(ProductsuppliedList productList, String mode, boolean ascendent){
		Comparator<Productsupplied> comparator;
		switch (mode) {
		case "name":
			comparator = new CompareNames(ascendent);
			break;
		case "price":
			comparator = new ComparePrice(ascendent);
			break;
		case "minPrice":
			comparator = new CompareMinPrice(ascendent);
			break;
		case "maxPrice":
			comparator = new CompareMaxPrice(ascendent);
			break;
		case "onSale":
			comparator = new CompareOnSale(ascendent);
			break;
		case "quantity":
			comparator = new CompareQuantity(ascendent);
			break;
		default:
			comparator = new CompareNames(ascendent);
		}
		TreeSet<Productsupplied> orderedSet = new TreeSet<Productsupplied>(comparator);
		orderedSet.addAll(productList);
		System.out.println("ORDERED:");
		for(GenericProductInterface product : orderedSet){
			System.out.println(product.getProductName());
		}
		ProductsuppliedList returnList = new ProductsuppliedList();
		returnList.addAll(orderedSet);
		System.out.println("ORDERED:");
		for(GenericProductInterface product : returnList){
			System.out.println(product.getProductName());
		}
		return returnList;
	}
	public Productsupplied retriveProductToEdit(String productID, String  supplierID){
		ProductsuppliedPK key = new ProductsuppliedPK();
		key.setProductID(Integer.parseInt(productID));
		key.setSupplierID(Integer.parseInt(supplierID));
		
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		
		Productsupplied productsuppliedToEdit = em.find(Productsupplied.class, key);
		
		em.getTransaction().commit();
		em.close();
		return productsuppliedToEdit;
	}
	
	public ProductsuppliedList retriveProductsuppliedListFromCategory(String supplierID, String categoryName){
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		
		String queryText = "SELECT p FROM Productsupplied p WHERE p.product.category.name = '"+categoryName+"' AND p.supplier.id = '"+supplierID+"'";
		Query query = em.createQuery(queryText);
		List<?> queryResultList = query.getResultList();
		ProductsuppliedList returnList = new ProductsuppliedList();
		for(Object o:queryResultList){
			returnList.add((Productsupplied) o);
		}
		em.getTransaction().commit();
		em.close();
		return returnList;
	}
	
	public ProductsuppliedList retriveProductsuppliedListFromCategorySort(String supplierID, String categoryName, String sortBy, String value){
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		
		String queryText;
		
		if(categoryName.compareTo("all")==0){
			queryText = "SELECT p FROM Productsupplied p WHERE p.supplier.id = '"+supplierID+"' ORDER BY "+sortBy+" "+value;
		}
		else{
		queryText = "SELECT p FROM Productsupplied p "
				+ "WHERE p.product.category.name = '"+categoryName+"' AND p.supplier.id = '"+supplierID+"' ORDER BY "+sortBy+" "+value;
		}
		System.out.println("\n\n########  retriveProductsuppliedListFromCategorySort  ########\n\n");
		System.out.println("\n\n########  queryText: "+queryText+" ########\n\n");
		Query query = em.createQuery(queryText);
		List<?> queryResultList = query.getResultList();
		ProductsuppliedList returnList = new ProductsuppliedList();
		for(Object o:queryResultList){
			returnList.add((Productsupplied) o);
		}
		em.getTransaction().commit();
		em.close();
		return returnList;
	}
	
	public ProductsuppliedList retriveProductsuppliedListFromAllCategories(String supplierID){
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		
		String queryText = "SELECT p FROM Productsupplied p WHERE p.supplier.id = '"+supplierID+"'";
		Query query = em.createQuery(queryText);
		List<?> queryResultList = query.getResultList();
		ProductsuppliedList returnList = new ProductsuppliedList();
		for(Object o:queryResultList){
			returnList.add((Productsupplied) o);
		}
		em.getTransaction().commit();
		em.close();
		return returnList;
	}
	
	public ProductsuppliedList retriveProductsuppliedListOnSale(String supplierID){
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		
		String queryText = "SELECT p FROM Productsupplied p WHERE p.onSale = 'true' AND p.supplier.id = :sID";
		Query query = em.createQuery(queryText);
		query.setParameter("sID", Integer.parseInt(supplierID));
		
		List<?> queryResultList = query.getResultList();
		ProductsuppliedList returnList = new ProductsuppliedList();
		for(Object o:queryResultList){
			returnList.add((Productsupplied) o);
		}
		em.getTransaction().commit();
		em.close();
		return returnList;
	}
	
	
	public String getSubscribers(String productID){
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		
		String queryText = "SELECT s FROM Subscription s WHERE s.product.idProduct = '"+productID+"'";
		Query query = em.createQuery(queryText);
		
		List<?> queryResultList = query.getResultList();
		String clients = "";
		
		for(Object o:queryResultList){
			Subscription subscription = (Subscription) o;
			clients = clients + subscription.getClient().getEmail() +", ";
		}
		
		em.getTransaction().commit();
		em.close();
		return clients;
	}
	
	public int getSubsNumber(String productID){
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		
		String queryText = "SELECT s FROM Subscription s WHERE s.product.idProduct = '"+productID+"'";
		Query query = em.createQuery(queryText);
		
		List<?> queryResultList = query.getResultList();
		int number = queryResultList.size();
		
		em.getTransaction().commit();
		em.close();
		return number;
	}
	
	public ProductsuppliedList retriveProductsuppliedSearched(int supplierID, String type ,String value){
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		
		String queryText = "SELECT p FROM Productsupplied p WHERE p.supplier.id ='"+supplierID+"' AND "+type+" = '"+value+"'";
		System.out.println("\n\n########  searchQuery: "+queryText+" ########\n\n");
		Query query = em.createQuery(queryText);
		
		List<?> queryResultList = query.getResultList();
		ProductsuppliedList returnList = new ProductsuppliedList();
		for(Object o:queryResultList){
			returnList.add((Productsupplied) o);
		}
		System.out.println("\n\n########  list_size: "+returnList.size()+" ########\n\n");
		em.getTransaction().commit();
		em.close();
		return returnList;
	}
	
	public Product getProduct(String name) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		
		String queryText = "SELECT p FROM Product p WHERE p.name = '"+name+"'";
		Query query = em.createQuery(queryText);
		List<?> queryResultList = query.getResultList();
		if(queryResultList.size()==0){
			return null;
		}
		Product product = (Product) queryResultList.get(0);
		
		em.getTransaction().commit();
		em.close();
		return product;
	}
	
	public boolean addProduct(Product product) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		
		em.persist(product);
		
		em.getTransaction().commit();
		em.close();
		return true;
	}
	
	
	

	
	private void setupProperties() {
		properties = new Properties();
		/*
		 * properties.put(PersistenceUnitProperties.CLASSLOADER, this.getClass()
		 * .getClassLoader());
		 */
		properties.put(PersistenceUnitProperties.CLASSLOADER,
				this.getClass().getClassLoader());
		properties.put(PersistenceUnitProperties.ECLIPSELINK_PERSISTENCE_UNITS,
				persistanceUnit);
		emf = Persistence.createEntityManagerFactory(persistanceUnit,
				properties);
	}
	
	
	public boolean editProductSuppliedQuantity(String supplierID, String productID, String quantity) {
		ProductsuppliedPK key = new ProductsuppliedPK();
		key.setProductID(Integer.parseInt(productID));
		key.setSupplierID(Integer.parseInt(supplierID));
		
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		
		Productsupplied productsuppliedToEdit = em.find(Productsupplied.class, key);
		productsuppliedToEdit.setQuantity(quantity);
		
		em.getTransaction().commit();
		em.close();
		return true;
	}
	
	public boolean deleteProductsupplied(String supplierID, String productID) {
		ProductsuppliedPK key = new ProductsuppliedPK();
		key.setProductID(Integer.parseInt(productID));
		key.setSupplierID(Integer.parseInt(supplierID));
		
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		
		Productsupplied productsuppliedToDelete = em.find(Productsupplied.class, key);
		productsuppliedToDelete.setActive(false);
		
		em.getTransaction().commit();
		em.close();
		return true;
	}
	
	public boolean AddProductsupplied(Productsupplied productsuppliedToAdd) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		
		em.persist(productsuppliedToAdd);
		
		em.getTransaction().commit();
		em.close();
		return true;
	}
private class CompareNames implements Comparator<Productsupplied>{
		
		private boolean ascendent;
		
		public CompareNames(boolean ascendent){
			this.ascendent = ascendent;
		}

		@Override
		public int compare(Productsupplied o1,
				Productsupplied o2) {
			int compare = o1.getProductName().compareTo(o2.getProductName());
			if (!ascendent) {
				compare = -compare;
			}
			return compare;
		}
	}
	
	private class ComparePrice implements Comparator<Productsupplied>{
		private boolean ascendent;
		
		public ComparePrice(boolean ascendent){
			this.ascendent = ascendent;
		}
		@Override
		public int compare(Productsupplied o1, Productsupplied o2) {
			int compare = Integer.parseInt(o1.getSellPrice()) - Integer.parseInt(o2.getSellPrice());
			if(compare==0){
				return new CompareNames(ascendent).compare(o1, o2);
			}
			if(!ascendent){
				compare = - compare;
			}
			return compare;
		}
		
	}
	
	private class CompareMinPrice implements Comparator<Productsupplied>{
		
		private boolean ascendent;
		
		public CompareMinPrice(boolean ascendent){
			this.ascendent = ascendent;
		}

		@Override
		public int compare(Productsupplied o1,
				Productsupplied o2) {
			int compare = Integer.parseInt(o1.getMinPrice()) - Integer.parseInt(o2.getMinPrice());
			if(compare==0){
				return new CompareNames(ascendent).compare(o1, o2);
			}
			if (!ascendent) {
				compare = -compare;
			}
			return compare;
		}
	}
	
	private class CompareMaxPrice implements Comparator<Productsupplied>{
		
		private boolean ascendent;
		
		public CompareMaxPrice(boolean ascendent){
			this.ascendent = ascendent;
		}

		@Override
		public int compare(Productsupplied o1,
				Productsupplied o2) {
			int compare = Integer.parseInt(o1.getMaxPrice()) - Integer.parseInt(o2.getMaxPrice());
			if(compare==0){
				return new CompareNames(ascendent).compare(o1, o2);
			}
			if (!ascendent) {
				compare = -compare;
			}
			return compare;
		}
	}
	
	private class CompareOnSale implements Comparator<Productsupplied>{
		
		private boolean ascendent;
		
		public CompareOnSale(boolean ascendent){
			this.ascendent = ascendent;
		}

		@Override
		public int compare(Productsupplied o1,
				Productsupplied o2) {
			int os1 = (o1.getOnSale()) ? 1 : 0;
			int os2 = (o1.getOnSale()) ? 1 : 0;
			int compare = os1 - os2;
			if(compare==0){
				return new CompareNames(ascendent).compare(o1, o2);
			}
			if (!ascendent) {
				compare = -compare;
			}
			return compare;
		}
	}
	
	private class CompareQuantity implements Comparator<Productsupplied>{
			
			private boolean ascendent;
			
			public CompareQuantity(boolean ascendent){
				this.ascendent = ascendent;
			}
	
			@Override
			public int compare(Productsupplied o1,
					Productsupplied o2) {
				int compare = Integer.parseInt(o1.getQuantity()) - Integer.parseInt(o2.getQuantity());
				if(compare==0){
					return new CompareNames(ascendent).compare(o1, o2);
				}
				if (!ascendent) {
					compare = -compare;
				}
				return compare;
			}
		}
}
