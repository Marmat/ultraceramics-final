package db;

import java.util.List;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import model.Productsupplied;
import model.ProductsuppliedPK;
import model.Supplier;

import org.eclipse.persistence.config.PersistenceUnitProperties;

public class SupplierManager {
	private Properties properties;
	private String persistanceUnit = "Model";
	EntityManagerFactory emf;

	public SupplierManager() {
		setupProperties();
	}

	public boolean register(Supplier supplier) {
		System.out.println("\n\n########  register_SupplierManager ########\n\n");
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		System.out.println("\n\n########  befoerePersist_register_SupplierManager ########\n\n");
		em.persist(supplier);
		System.out.println("\n\n########  afterPersist_register_SupplierManager ########\n\n");
		em.getTransaction().commit();
		em.close();
		System.out.println("\n\n########  END_register_SupplierManager ########\n\n");
		return true;
	}

	public Supplier login(String email, String password) {
		EntityManager em = emf.createEntityManager();
		Query query = em.createQuery("SELECT s FROM Supplier s WHERE s.email='"
				+ email + "' AND s.password='" + password + "'");
		List<?> queryResult = query.getResultList();
		if (queryResult.size() == 0) {
			return null;
		}
		Supplier supplier = (Supplier) queryResult.get(0);
		em.close();
		return supplier;
	}

	public Supplier editSupplier(int supplierID, Supplier newSupplier) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		
		Supplier oldSupplier = em.find(Supplier.class, supplierID);
		oldSupplier.setActive(newSupplier.getActive());
		oldSupplier.setAddress(newSupplier.getAddress());
		oldSupplier.setEmail(newSupplier.getEmail());
		oldSupplier.setId(newSupplier.getId());
		oldSupplier.setName(newSupplier.getName());
		oldSupplier.setPassword(newSupplier.getPassword());
		oldSupplier.setProductsupplieds(newSupplier.getProductsupplieds());
		
		em.getTransaction().commit();
		em.close();
		return oldSupplier;
	}
	
	public Supplier getSupplier(int supplierID) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		
		Supplier supplier = em.find(Supplier.class, supplierID);
		
		em.getTransaction().commit();
		em.close();
		return supplier;
	}
	
	public Supplier getSupplier(String email) {
		EntityManager em = emf.createEntityManager();
		Query query = em.createQuery("SELECT s FROM Supplier s WHERE s.email='"+ email + "'");
		List<?> queryResult = query.getResultList();
		if (queryResult.size() == 0) {
			return null;
		}
		Supplier supplier = (Supplier) queryResult.get(0);
		em.close();
		return supplier;
	}

	private void setupProperties() {
		properties = new Properties();
		/*
		 * properties.put(PersistenceUnitProperties.CLASSLOADER, this.getClass()
		 * .getClassLoader());
		 */
		properties.put(PersistenceUnitProperties.CLASSLOADER, this.getClass()
				.getClassLoader());
		properties.put(PersistenceUnitProperties.ECLIPSELINK_PERSISTENCE_UNITS,
				persistanceUnit);
		emf = Persistence.createEntityManagerFactory(persistanceUnit,
				properties);
	}
}
