
<%@page import="model.Category"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Ultraceramics</title>
</head>
<body>

<jsp:include page="header.jsp"></jsp:include>
<div class="container">
	<div class="row">
	
		<div class= "col-md-2">
		<jsp:include page="leftBarCategory.jsp"></jsp:include>
		</div>
		
         <div class="col-md-9 panel">
         	<jsp:include page="catalogue.jsp"></jsp:include>
         </div> 		
	</div>
</div>

<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>