<%@page import="java.util.Set"%>
<%@page import="java.util.Map"%>
<%@page import="model.*"%>
<%@page import="utils.Cart"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:useBean id="productExceeded" scope="request" class="model.Productsupplied" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Cart</title>

<link href="css/penesenvinagre.css" rel="stylesheet">
</head>
<body>
	<%
		Cart cart;
			Object o = session.getAttribute("cart");
			if (o == null) {
		cart = new Cart();
		session.setAttribute("cart", cart);
			} else {
		cart = (Cart) o;
			}
			double total = 0;
	%>
	<jsp:include page="header.jsp"></jsp:include>

	<div class="container text-center">

		<div class="col-md-5 col-sm-12">
			<div class="bigcart"></div>
			<h1>Your shopping cart</h1>
		</div>
		<div class="col-md-7">
			<%	
				boolean exceeded = false;
				if(productExceeded.getProduct()!=null){
					exceeded = true;
			%>
					<p style="text-color:red">Error: the product <%=productExceeded.getProduct().getName() %>
						has only <%=productExceeded.getQuantity() %> unity available 
					</p>
			<%	}
			%>
			<table class="table table-striped">
				<tbody>
					<tr>
						<th>Quantity</th>
						<th>Product</th>
						<th>Price (€)</th>
						<th></th>
						<%
							Map<Productsupplied,Integer> map = cart.getProductMap();
											Set<Productsupplied> productSet = map.keySet();
											int i=0;
											for (Productsupplied product : productSet) {
												double sellPrice = Double.parseDouble(product.getSellPrice());
												//total += map.get(product)*sellPrice;
						%>
					
					<tr>
						<%
							String idNumber="numberItem"+i;
											String idPrice = "price"+i;
											String changeAmount = "changeAmount("+product.getId().getProductID()+","+product.getId().getSupplierID()+","+idNumber+")";
											String amount = map.get(product)+"";
						%>
						<td><input id=<%=idNumber%> type="number" name="points"
							min="1" max="100" step="1" value=<%=amount%>
							onChange=<%=changeAmount%>></td>
						<% String link = "product?productID="
														+ product.getId().getProductID() + "&supplierID="
														+ product.getId().getSupplierID();
						%>
						<td><a href=<%=link %>><%=product.getProduct().getName()%></a></td>
						<td id=<%=idPrice%> style="color: #FFCC00;"><%=sellPrice+"€"%></td>
						<%
							String parameters = "removeFromCart?productID="
														+ product.getId().getProductID() + "&supplierID="
														+ product.getId().getSupplierID();
						%>
						<td><a class="btn btn-danger btn-sm" type="submit"
							href=<%=parameters%>>Remove</a></td>
					</tr>
					<%
						i++;
									}
					%>
				</tbody>

			</table>
			<!-- 			<div class="col-md-8 col-md-push-3" style="padding-bot: 10px;"> -->
			<!-- 				 </div> -->
			<form class="col-md-12"style="padding-left:100px" name="discount" method="POST" action="addDiscount">
				Insert here your discount code:   
				<%
					String discountCode = cart.getDiscountCode();
					if(discountCode==null) {%>
						<input type="text" name="discountCode" id="discountCode"/>
					<%} else { %>
						<input type="text" name="discountCode" id="discountCode" value=<%=discountCode %>/>
					<%} %>
				<button class="btn btn-success btn-sm" type="submit" >Insert Code</button> 
			</form>
			<div class="col-md-6 col-md-push-6" >
				<hr>
				<span style="color: #D7DF01; margin-right: 20px;"><b>Total:
						<%=cart.getTotal()+"€"%></b></span> 
				<%if(!exceeded){ %>
				<a class="btn btn-success btn-sm" type="submit" href="payment.html">Payment</a> 
				<%} else { %>
				<a class="btn btn-success btn-sm disabled" type="submit" href="payment.html">Payment</a> 
				<% } %>
					<a class="btn btn-primary btn-sm" type="submit" href="index.html">Keep
					Buying</a>
			</div>
		</div>
	</div>
	<jsp:include page="footer.jsp"></jsp:include>
	<!-- JavaScript includes -->

	<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/customjs.js"></script>
	<script>
		function changeAmount(productID,supplierID,quantityID){
			var value = $(quantityID).val();
			var site = 'changeAmount?productID=';
			site = site.concat(productID,'&supplierID=',supplierID,'&value=',value);
			window.location.replace(site);
		}
	</script>
</body>



</html>