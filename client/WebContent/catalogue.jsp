<%@page import="db.ProductManager"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashSet"%>
<%@page import="java.util.Set"%>
<%@page import="java.lang.Math"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="model.*"%>

<jsp:useBean id="productList" scope="request" class="model.ProductList" />
<jsp:useBean id="productsuppliedList" scope="request"
	class="model.ProductsuppliedList" />
<jsp:useBean id="pag" scope="request" class="java.lang.String" />

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Ultraceramics</title>
</head>
<body>
	<div class="row">
		Sort by:
		<a href="sort?mode=name&ascendent=true">Name</a>
		<a href="sort?mode=price&ascendent=true">Price</a>
		<a href="sort?mode=supplier&ascendent=true">Supplier</a>
		<br/>
	</div>
	<div class="row">
		<%
			if (productList.size() != 0) {
				productsuppliedList = productList.getBestSuppliedProductList();
			}
			session.setAttribute("productsuppliedList", productsuppliedList);
			{
				for (GenericProductInterface productSupplied : productsuppliedList) {
					if(productSupplied instanceof Productsupplied){
						Productsupplied ps;
						ps = (Productsupplied) productSupplied;
					Product product = ps.getProduct();
					Supplier supplier = ps.getSupplier();
					String productLink = "product?productID="
							+ product.getIdProduct();
					productLink += "&supplierID=" + supplier.getId();
		%>


		<div class="col-sm-4 col-lg-4 col-md-4">
			<div class="thumbnail">
				<img src="<%=product.getImage() %>" alt="">
				<div class="caption">
					
					<h4>
						<a href=<%=productLink%>><%=product.getName()%></a>
					</h4>
					<h4 class="pull-right"><%=ps.getSellPrice() + "€"%></h4>
					<p><%=product.getDescription()%></p>
					<p style="color: #31B404"><%=ps.getQuantity()%>
						Items Left
					</p>
				</div>
				<div class="ratings">
					<p class="pull-right"><%=supplier.getName()%></p>
					<p>
						<span class="glyphicon glyphicon-star"></span> <span
							class="glyphicon glyphicon-star"></span> <span
							class="glyphicon glyphicon-star"></span> <span
							class="glyphicon glyphicon-star"></span> <span
							class="glyphicon glyphicon-star"></span>
					</p>
				</div>
				<%
					String parameters = "addToCart?productID="
									+ product.getIdProduct();
							parameters += "&supplierID=" + supplier.getId();
				%>
				<a class="btn btn-success btn-sm" type="submit" href=<%=parameters%>
					data-toggle="modal">Add to chart</a>
				
				
				<%
					ProductManager pm = new ProductManager();
					Client user = (Client) session.getAttribute("user");
					String clientID ="";
					String productID = product.getIdProduct()+"";
					if(user!=null){
						clientID = user.getId()+"";
					}
					String removeSUB = "removeSub.html?productID="+ product.getIdProduct() + "&clientID=" + clientID;
					String addSUB = "subscribe?productID=" + productID + "&clientID=" + clientID;
					if(session.getAttribute("user")!=null)
					if(pm.isProductSubscribed(productID, clientID)==true){
				%>
						<a class="btn btn-danger btn-sm" type="submit" href=<%=removeSUB%> data-toggle="modal">Unsubscribe</a>
				<%
					}
					else{
				%>
						<a class="btn btn-warning btn-sm" type="submit" href=<%=addSUB%> data-toggle="modal">Subscribe</a>
				<%	
					}
				%>
				
			</div>
		</div>


		<%
			}
				}
			}
		%>
	</div>


</body>
</html>