
<%@page import="model.Category"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Ultraceramics</title>
</head>
<body>

<jsp:include page="header.jsp"></jsp:include>
<jsp:useBean id="productList" scope="request" class="model.ProductList" />
<div class="container">
	<div class="row">
	
		<div class= "col-md-2">
		<jsp:include page="leftBarCategory.jsp"></jsp:include>
		</div>
		<%
		String categoryName;
		String categoryLink;
		try{
			categoryName = request.getParameter("category");
			categoryLink = "category.html?category="+categoryName;
		} catch (Exception e){
			categoryName = "";
			categoryLink = "index.html";
		}
		%>
		<div class="col-md-9" style="margin-left:-15px; margin-right:-15px">
          			<ol class="breadcrumb">
          				<li><a href= "index.html">Home</a></li>
          				<li class= "active"><a href=<%=categoryLink %>><%=categoryName %></a></li>
          			</ol>
         </div>
         <div class="col-md-9 panel">
         	<jsp:include page="catalogue.jsp"></jsp:include>
         </div> 		
	</div>
</div>

<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>