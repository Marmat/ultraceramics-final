<%@page import="model.Client"%>
<%
	Client user = (Client) session.getAttribute("user");

	String string = user.getAddress();
	string = string.replaceAll(" ", "_");
	String[] parts = string.split(",");
	String address = parts[0];
	String zip = parts[1];
	String city = parts[2];
	
%>
	<script>
		$(document).ready(function() {
							$('#editProfile').bootstrapValidator({
												// To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
												message: 'This value is not valid',
												live: 'enabled',
												submitbutton : '#save',
												feedbackIcons : {
													valid : 'glyphicon glyphicon-ok-circle',
													invalid : 'glyphicon glyphicon-remove-circle',
													validating : 'glyphicon glyphicon-refresh'
												},
												fields : {
													firstname : {													
														message : 'The first name is not valid',													
														group:'.col-md-4',
														validators : {
															stringLength : {
																min : 2,
																max : 30,
																message : 'The first name must be more than 2 and less than 30 characters long'
															},
															regexp : {
																regexp : /^[a-zA-Z]+$/,
																message : 'The first name can only consist of alphabetical'
															}
														}
													},													
													secondname : {
														group:'.col-md-4',
														message : 'The second name is not valid',
														validators : {
															
															stringLength : {
																min : 2,
																max : 30,
																message : 'The second name must be more than 2 and less than 30 characters long'
															},
															regexp : {
																regexp : /^[a-zA-Z]+$/,
																message : 'The second name can only consist of alphabetical'
															}
														}
													},													
													email1 : {
														group:'.col-md-8',
														validators : {
															
															emailAddress : {
																message : 'The email address is not a valid'
															}
														}
													},													
													email2 : {
														group:'.col-md-8',
														validators : {
															
															emailAddress : {
																message : 'The email address verification is not a valid email address'
															},
															identical:{
																field: 'email1',
																message: 'The email address and the verification must be the same'
															}
															
														}
													},
													passwordinput1:{
														group:'.col-md-8',
														validators:{
															identical: {
																field: 'passwordinput2',
																message: 'The new password and the verification have to be the same'
															}
														}
													},
													passwordinput2:{
														group:'.col-md-8',
														enabled: false,
														validators:{
															notEmpty: {
																message : 'The verification of the passord is required and cannot be empty'
															},
															identical: {
									                            field: 'passwordinput1',
									                            message: 'The new password and its verification must be the same'
									                        }
														}
													},
													zip:{
														group:'.col-md-3',
														validators:{
															numeric : {
																message : 'The  ZIP code has to be numeric'
															},
															stringLength : {
																min : 5,
																max : 5,
																message : 'The ZIP code has to have length 5'
															}
															
														}
													}
												}
											});
							
							$('#editProfile').on('keyup', function(){
								var isEmpty = $(this).val() == '';
								$('#editProfile').bootstrapValidator('enableFieldValidators', 'passwordinput1', !isEmpty)
												.bootstrapValidator('enableFieldValidators', 'passwordinput2', !isEmpty);
								if ($(this).val().length ==1){
									$('#editProfile').bootstrapValidator('validateField','passwordinput2');
									$('#editProfile').bootstrapValidator('validateField','passwordinput1');
								}
								
								
							});
							$('#modalProfile').on('shown.bs.modal', function(){
								$('#editProfile').bootstrapValidator('resetForm',true);
							});
							
						});
	</script>

<!--Edit Profile modal-->
<div class="modal fade" id ="modalProfile" role="dialog" tabindex="-1" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
				<h1 class="text-left">Edit Profile</h1>
			</div>
			<div class="modal-body">


				<form id="editProfile" class="form-horizontal editProfile" action="editProfile.html" method="POST">
					<!-- This should be a POST but as we dont save anything it is not added -->
						<input id="oldAddress" name="oldAddress" type="hidden" value= <%=address %> >
  						<input id="oldCity" name="oldCity" type="hidden" value= <%=city %> >
  						<input id="oldZip" name="oldZip" type="hidden" value= <%=zip %> >
					
					<fieldset>

						<!-- First Name-->
						<div class="form-group">
							<label class="col-md-2 control-label">First
								Name</label>
							<div class="col-md-4">
								<input name="firstname" class="form-control" type="text" placeholder=<%=user.getFirstName()%> />
						
							</div>


							<!-- Second Name-->

							<label class="col-md-2 control-label" for="secondname">Second
								Name</label>
							<div class="col-md-4">
								<input name="secondname" placeholder=<%=user.getSecondName()%>
									class="form-control input-md" type="text">
							</div>
						</div>

						<!-- Email-->
						<div class="form-group">
							<label class="col-md-3  control-label" for="email1">New Email</label>
							<div class="col-md-8">
								<input id="email1" name="email1" placeholder=<%=user.getEmail()%>
									class="form-control input-md" type="text">
							</div>
						</div>

						<!-- Email Verification-->
						<div class="form-group">
							<label class="col-md-3  control-label" for="email2">Verify
								your new email</label>
							<div class="col-md-8 ">
								<input id="email2" name="email2" placeholder=""
									class="form-control input-md"  type="text">
							</div>
						</div>

						<!-- Password input-->
						<div class="form-group">
							<label class="col-md-3 control-label" for="passwordinput1">New Password</label>
							<div class="col-md-8">
								<input id="passwordinput1" name="passwordinput1"
									placeholder="Select your new password"
									class="form-control input-md"type="password">
							</div>
						</div>

						<!-- Password verification-->
						<div class="form-group">
							<label class="col-md-3 control-label" for="passwordinput2">Verify
								your new password</label>
							<div class="col-md-8">
								<input id="passwordinput2" name="passwordinput2" placeholder=""
									class="form-control input-md" type="password">
							</div>
						</div>

						</fieldset>
						
						<fieldset>
					

						<legend>
							<small>Shipping Information</small>
						</legend>

						<!-- Address-->
						<div class="form-group">
							<label class="col-md-3 col-md-pull-1 control-label" for="address">Address</label>
							<div class="col-md-9 col-md-pull-1">
								<input id="address" name="address" placeholder=<%=address %>
									class="form-control input-md" type="text">
								
							</div>
						</div>

						<!-- City-->
						<div class="form-group">
							<label class="col-md-2 control-label" for="city">City</label>
							<div class="col-md-4">
								<input id="city" name="city" placeholder=<%=city %>
									class="form-control input-md" type="text"> 
							</div>


							<!-- ZIP-->

							<label class="col-md-3 col-md-pull-1 control-label" for="zip">ZIP
								Code</label>
							<div class="col-md-3 col-md-pull-1 ">
								<input id="zip" name="zip" placeholder=<%=zip %>
									class="form-control input-md" type="text"> 
							</div>
						</div>
						
						</fieldset>
					
					
						
						
						<!-- Password verify changes-->
						<div class="form-group">
							<label class="col-md-3 control-label" for="passwordorg">To apply any change please enter your actual password</label>
							<div class="col-md-8">
								<input id="passwordorig" name="passwordorig"
									placeholder="Enter your actual password"
									class="form-control input-md"type="password">
							</div>
						</div>
						
						
						<div class="modal-footer">
						<!-- Button (Double) -->
				
							<div class="col-md-8">
								<button id="cancel" name="cancel" class="btn btn-inverse" data-dissmis="modal">Cancel</button>
								<button id="save" name="save" type="submit" class="btn btn-primary">Save</button>
							</div>
						</div>
						
						
							</form>
							</div>
						
							
						</div>

					
			

			</div>


		</div>


