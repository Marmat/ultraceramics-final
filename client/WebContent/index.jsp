<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Ultraceramics</title>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
	
    <jsp:include page="header.jsp"></jsp:include>

    <!-- Page Content -->
    <div class="container">

        <div class="row">
        	
        	<div class = "col-md-2">
        
			<div class = "row">
            <jsp:include page="leftBarCategory.jsp"></jsp:include>
            </div>
            <div class = "row">
			<jsp:include page ="searchPanel.jsp"></jsp:include>
			</div>
        	</div>
        	
			<div class= "col-md-9">
			<div class="row carousel-holder">

                    <div class="col-md-12">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example-generic" data-slide-to="0" class=""></li>
                                <li class="" data-target="#carousel-example-generic" data-slide-to="1"></li>
                                <li class="active" data-target="#carousel-example-generic" data-slide-to="2"></li>
                            </ol>
                            <div class="carousel-inner">
                                <div class="item">
                                    <img class="slide-image" src="http://leobattistelli.com/wp-content/uploads/2011/04/IMG_5678-800x300.jpg" alt="">
                                </div>
                                <div class="item">
                                    <img class="slide-image" src="http://www.rampiniceramics.com/img/header/ORT001.jpg" alt="">
                                </div>
                                <div class="item active">
                                    <img class="slide-image" src="http://www.azaharasantiago.com/wp-content/uploads/2012/04/Happy-days_0111-800x300.jpg" alt="">
                                </div>
                            </div>
                            <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                            </a>
                        </div>
                    </div>

                </div>
        	<jsp:include page ="catalogue.jsp"></jsp:include>
        	</div>
        	</div>
        </div>

		<jsp:include page="footer.jsp"></jsp:include>
</body>

</html>
