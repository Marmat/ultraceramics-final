<%@page import="model.Category"%>
<%@page import="java.util.List"%>
<%@page import="db.CategoryManager"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Ultraceramics</title>
</head>
<body>


                <p class="lead">Category name</p>
                <div class="list-group">
                <a href="category.html?category=onSale" class="list-group-item" style="text-align:left; background-color:#003366; color:#FFFFFF ">On Sale</a>
                <%
					if(session.getAttribute("user")!=null){ 
				%>
                <a id="subscribedOnSale" href="category.html?category=subscribedOnSale" class="list-group-item" style="text-align:left">Subscribed OnSale</a>
                <a id="subscribed" href="category.html?category=subscribed" class="list-group-item" style="text-align:left">Subscribed</a>
                <br>
                <%} %>
                <%
                	CategoryManager manager = new CategoryManager();
                	List<Category> categories = manager.getCategoryList();
                	for(Category category : categories){
                		String name = category.getName();
                		String link = "category.html?category="+name;
                %>
                	
                    <a href=<%=link %> class="list-group-item" style="text-align:left"><%=name%></a>
                     <% 	} %>
                </div>


</body>
</html>