<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Log-in</title>
</head>
	<body>
	<jsp:include page= "header.jsp"></jsp:include>
<!--login modal-->

  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <button onclick = "window.location.href = document.referrer" type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h1 class="text-center">Login</h1>
 
      </div>
      <div class="modal-body">
          <form class="form col-md-12 center-block"method="POST" action="doLogin.html">
            <div class="form-group">
              <input name="username" type="text" class="form-control input-lg" placeholder="Username">
            </div>
            <div class="form-group">
              <input name="password" type="password" class="form-control input-lg" placeholder="Password">
            </div>
            <div class="form-group">
              <button type = "submit" class="btn btn-primary btn-lg btn-block">Sign In</button>
              <span onclick = "location.href = 'Register.jsp'" class="pull-right"><a href="#">Register</a></span><span><a href="#">Need help?</a></span>
            </div>
          </form>
      </div>
      <div class="modal-footer">
          <div class="col-md-12">
          <button onclick = "window.location.href = document.referrer" class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
		  </div>	
      </div>
  
  </div>
</div>
	<!-- script references -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>	
	
	</body>
</html>