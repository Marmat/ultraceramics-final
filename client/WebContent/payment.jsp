<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Payment</title>

</head>



<body>

	<jsp:include page="header.jsp"></jsp:include>

	<script>
		$(document).ready(function() {
							$('#paymentForm').bootstrapValidator(
											{
									
												message : 'This value is not valid',
												live : 'enabled',
												excluded : ':hidden',
												submitbutton : '#button1id',
												feedbackIcons : {
													valid : 'glyphicon glyphicon-ok-circle',
													invalid : 'glyphicon glyphicon-remove-circle',
													validating : 'glyphicon glyphicon-refresh'
												},
												fields : {
													addressShip : {
														message : 'The shipping address is not valid',
														group : '.col-md-9',
														validators : {
															notEmpty : {
																message : 'The shipping address is required and cannot be left empty'
															},
															regexp : {
																regexp : /^[a-zA-Z0-9]+$/,
																message : 'The shipping address can only consist of alphabetical and number'
															}
														}
													},
													cityShip : {
														message : 'The shipping address is not valid',
														group : '.col-md-4',
														validators : {
															notEmpty : {
																message : 'The shipping address is required and cannot be left empty'
															},
															regexp : {
																regexp : /^[a-zA-Z]+$/,
																message : 'The shipping address can only consist of alphabetical '
															}
														}
													},
													zipShip : {
														message : 'The shipping address is not valid',
														group : '.col-md-3',
														validators : {
															notEmpty : {
																message : 'The shipping address is required and cannot be left empty'
															},
															numeric : {
																message : 'The shipping ZIP code has to be numeric'
															},
															stringLength : {
																min : 5,
																max : 5,
																message : 'The ZIP code has to have length 5'
															}
														}
													},
													addressBill : {
														message : 'The billing address is not valid',
														group : '.col-md-9',
														validators : {
															notEmpty : {
																message : 'The billing address is required and cannot be left empty'
															},
															regexp : {
																regexp : /^[a-zA-Z0-9]+$/,
																message : 'The billing address can only consist of alphabetical and number'
															}
														}
													},
													cityBill : {
														message : 'The billing city is not valid',
														group : '.col-md-4',
														validators : {
															notEmpty : {
																message : 'The billing city is required and cannot be left empty'
															},
															regexp : {
																regexp : /^[a-zA-Z]+$/,
																message : 'The billing city can only consist of alphabetical characters'
															}
														}
													},
													zipBill : {
														message : 'The billing ZIP code is not valid',
														group : '.col-md-3',
														validators : {
															notEmpty : {
																message : 'The billing ZIP code is required and cannot be left empty'
															},
															numeric : {
																message : 'The billing ZIP code has to be numeric'
															},
															stringLength : {
																min : 5,
																max : 5,
																message : 'The billing ZIP code has to have length 5'
															}
														}
													},
													/*cardnum : {
														validators : {
															creditCard : {
																message : 'The credit card number is not valid'
															}
														}
													},*/
													/*cvc : {
														group : '.col-md-3',
														validators : {
															cvv : {
																creditCardField : 'cardnum',
																message : 'The CVV number is not valid'
															}
														}
													}*/
												}
											});

							$('input[type$="radio"]').on('change', function() {
								var test = $(this).attr("value");
								$("#detailsCard").removeClass("show");
								

								if (test == 1) {
									$("#detailsCard").show();
								
								}
								if (test == 2) {
									$("#detailsCard").hide();
									
								
							
								}

							});

						});
	</script>

	




	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close">
					<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
				</button>
				<h1 class="text-left">Payment</h1>
			</div>
			<div class="modal-body">
				<form id="paymentForm" class="form-horizontal registerForm" method="POST" action="doPayment">
					<fieldset>

						<legend>Shipping Information</legend>


						<!-- Address-->
						<div class="form-group">
							<label class="col-md-3 col-md-pull-1 control-label">Address</label>
							<div class="col-md-9 col-md-pull-1">
								<input name="addressShip" class="form-control" type="text">
							</div>
						</div>

						<!-- City-->
						<div class="form-group">
							<label class="col-md-2 control-label" for="cityShip">City</label>
							<div class="col-md-4">
								<input id="cityShip" name="cityShip" placeholder="Madrid"
									class="form-control input-md" type="text"> <span
									class="help-block" id="helpCity">Please enter the city
									where you live</span>
							</div>


							<!-- ZIP-->

							<label class="col-md-3 col-md-pull-1 control-label" for="zipShip">ZIP
								Code</label>
							<div class="col-md-3 col-md-pull-1">
								<input id="zipShip" name="zipShip" placeholder="28123"
									class="form-control input-md" type="text"> <span
									class="help-block">Please enter your zip code</span>
							</div>
						</div>
					</fieldset>



					<!-- Billing Info-->
					<fieldset>
						<legend>Billing Information</legend>


						<!-- Address-->
						<div class="form-group">
							<label class="col-md-3 col-md-pull-1 control-label"
								for="addressBill">Address</label>
							<div class="col-md-9 col-md-pull-1">
								<input id="addressBill" name="addressBill"
									placeholder="Fake str 123" class="form-control input-md"
									type="text"> <span class="help-block">Please
									enter your address</span>
							</div>
						</div>

						<!-- City-->
						<div class="form-group">
							<label class="col-md-2 control-label" for="cityBill">City</label>
							<div class="col-md-4">
								<input id="cityBill" name="cityBill" placeholder="Madrid"
									class="form-control input-md" type="text"> <span
									class="help-block">Please enter the city where you live</span>
							</div>


							<!-- ZIP-->

							<label class="col-md-3 col-md-pull-1 control-label" for="zipBill">ZIP
								Code</label>
							<div class="col-md-3 col-md-pull-1 ">
								<input id="zipBill" name="zipBill" placeholder="28123"
									class="form-control input-md" type="text"> <span
									class="help-block">Please enter your zip code</span>
							</div>
						</div>
					</fieldset>



					<fieldset>

						<legend>Payment Information</legend>
						<!-- Payment method -->
						<div class="form-group">
							<label class="col-md-4 control-label" for="radios">Payment
								Method</label>
							<div class="col-md-4">
								<div class="radio">
									<label for="radios-0"> <input name="radios"
										id="radios-0" value="1" checked="checked" type="radio">
										Credit Card
									</label>
								</div>
								<div class="radio">
									<label for="radios-1"> <input name="radios"
										id="radios-1" value="2" type="radio"> On Delivery
									</label>
								</div>
							
							</div>
						</div>

						<div id="detailsCard" class="show">

							<!-- Card type -->
							<div class="form-group">
								<label class="col-md-4 control-label" for="cardtype">Credit
									Card Type *</label>
								<div class="col-md-4">
									<select id="cardtype" name="cardtype" class="form-control">
										<option value="0">--Please Select--</option>
										<option value="1">American Express</option>
										<option value="2">Visa</option>
										<option value="3">MasterCard</option>
									</select>
								</div>
							</div>

							<!-- Card Number-->
							<div class="form-group">
								<label class="col-md-4 control-label" for="cardnum">Credit
									Card Number *</label>
								<div class="col-md-6">
									<input id="cardnum" name="cardnum" placeholder=""
										class="form-control input-md" required="" type="text">
									<span class="help-block">Please enter your credit card
										number</span>
								</div>
							</div>


							<!-- CVC-->
							<div class="form-group">
								<label class="col-md-1 control-label" for="cvc">CVC*</label>
								<div class="col-md-3">
									<input id="cvc" name="cvc" placeholder="123"
										class="form-control input-md" required="" type="text">
									<span class="help-block">The code that appears on the
										rear part of your credit card</span>
								</div>


								<!-- Expiration Date-->

								<label class="col-md-2 control-label" for="expmonth">Expiration
									Date *</label>
								<div class="col-md-3">
									<select id="expmonth" name="expmonth" class="form-control">
										<option value="0">Month</option>
										<option value="1">01</option>
										<option value="2">02</option>
										<option value="3">03</option>
										<option value="4">04</option>
										<option value="5">05</option>
										<option value="6">06</option>
										<option value="7">07</option>
										<option value="8">08</option>
										<option value="9">09</option>
										<option value="10">10</option>
										<option value="11">11</option>
										<option value="12">12</option>
									</select>
								</div>

								<div class="col-md-3">
									<select id="expyear" name="expyear" class="form-control">
										<option value="0">Year</option>
										<option value="1">2014</option>
										<option value="2">2015</option>
										<option value="3">2016</option>
										<option value="4">2017</option>
										<option value="5">2018</option>
										<option value="6">2019</option>
										<option value="7">2020</option>
										<option value="8">2021</option>
										<option value="9">2022</option>
										<option value="10">2023</option>
										<option value="11">2024</option>

									</select>
								</div>

							</div>
						</div>
						

					</fieldset>




					<!-- Button (Double) -->
					<div class="form-group">
						<label class="col-md-9 control-label" for="button1id"></label>
						<div class="col-md-3">
							<button id="button1id" name="button1id" class="btn btn-primary" type="submit">Proceed</button>
						</div>
					</div>

				</form>
					<a href="cart.html"><button id="button2id" name="button2id" class="btn btn-inverse">Back to cart</button></a>
			</div>

		</div>
	</div>

<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>