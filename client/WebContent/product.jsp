<%@ page import="model.*"%>
<%@page import="db.ProductManager"%>
<%@page import="model.Productsupplied"%>
<%@page import="model.Supplier"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.HashSet"%>
<%@page import="model.Product"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:useBean id="selectedProduct" scope="request"
	class="model.Productsupplied" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Ultraceramics</title>
</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>
	<%
		Product product = selectedProduct.getProduct();
		String categoryName;
		String categoryLink;
		try {
			categoryName = product.getCategory().getName();
			categoryLink = "category.html?category=" + categoryName;
		} catch (Exception e) {
			categoryName = "";
			categoryLink = "index.html";
		}
		Set<String[]> otherSuppliers = new HashSet<String[]>();
		for (Productsupplied productsupplied : product
				.getProductsupplieds()) {
			Supplier supplier = productsupplied.getSupplier();
			if (supplier != selectedProduct.getSupplier()) {
				String productLink = "product?productID="
						+ product.getIdProduct();
				productLink += "&supplierID=" + supplier.getId();
				String[] stringValues = new String[] { supplier.getName(),
						productLink };
				otherSuppliers.add(stringValues);
			}
		}
	%>
	<div class="container">
		<div class="row">
			<div class = "col-md-2">
			<jsp:include page="leftBarCategory.jsp"></jsp:include>
			</div>
			<div class="col-md-9" style="margin-left: -15px; margin-right: -15px">
				<ol class="breadcrumb">
					<li><a href="index.html">Home</a></li>
					<li><a href=<%=categoryLink%>><%=categoryName%></a></li>
					<li class="active"><%=product.getName()%></li>
				</ol>
			</div>
			<div class="col-md-9 jumbotron">

				<div class="row">
					<div class="span"> 
						<div class="col-md-5">
							<img src="<%=product.getImage()%>" alt="" class="panel">
						</div>

						<div class="col-md-5 col-md-push-2">
							<h3><%=product.getName()%></h3>
						</div>
						<!-- <div class="col-md-4">
						<h3>Product Type: #Type1</h3>
					</div>-->
					</div>

					<div class="col-md-7 col-md-push-2">
						<h3><%=selectedProduct.getSellPrice() + "€"%></h3>
					</div>
					<div class="col-md-7 col-md-push-2">
						<h3 style="color: #33CC33">
							Available Items:
							<%=selectedProduct.getQuantity()%></h3>
					</div>

					<div class="col-md-6 col-md-push-2">
						<h3>
							Supplier:
							<%=selectedProduct.getSupplier().getName()%></h3>
					</div>
					<div class="col-md-12 panel">
						<h3>Description:</h3>
						<div class="panel-body" style="overflow: auto;">
							<%=product.getDescription()%>
						</div>
					</div>
				</div>


				<%
					String parameters = "addToCart?productID=" + product.getIdProduct();
					parameters += "&supplierID="
							+ selectedProduct.getSupplier().getId();
					String subscriptionParameters = "subscribe?productID=" + product.getIdProduct();
					subscriptionParameters += "&supplierID="
							+ selectedProduct.getSupplier().getId();
				%>
		<div class="row">		
			<div class ="col-md-4 col-md-offset-8">
				<a class="btn btn-success" type="submit"
					href=<%=parameters%> data-toggle="modal">Add to chart</a>
				
				<%
					ProductManager pm = new ProductManager();
					Client user = (Client) session.getAttribute("user");
					String clientID ="";
					String productID = product.getIdProduct()+"";
					if(user!=null){
						clientID = user.getId()+"";
					}
					String removeSUB = "removeSub.html?productID="+ product.getIdProduct() + "&clientID=" + clientID;
					String addSUB = "subscribe?productID=" + productID + "&clientID=" + clientID;
					if(session.getAttribute("user")!=null)
					if(pm.isProductSubscribed(productID, clientID)==true){
				%>
						<a class="btn btn-danger" type="submit" href=<%=removeSUB%> data-toggle="modal">Unsubscribe</a>
				<%
					}
					else{
				%>
						<a class="btn btn-warning" type="#" href=<%=subscriptionParameters %> data-toggle="modal"> Subscribe</a>
				<%	
					}
				%>
				
				
					
			</div>
		</div>	
				<div class="row">
					<p style="margin-top:20px">You don't like this product? Look to other suppliers:</p>
					<p>
					<%
						int i=1;
						for (String[] otherSupplier : otherSuppliers) {
					%>
							<a href=<%=otherSupplier[1]%>><%=otherSupplier[0]%> </a>
					<%
							if(i!=otherSupplier.length){
								%>
								,
								<%
							}
							i++;
						}
					%>
					</p>
				</div>
			</div>
		</div>
	</div>


	<jsp:include page="footer.jsp"></jsp:include>

</body>
</html>