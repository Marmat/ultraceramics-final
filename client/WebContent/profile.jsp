
<%@page import="model.Client"%>
<html>
<head>

<title>Profile</title>
</head>
<body>
 <jsp:include page="header.jsp"></jsp:include>
 
 <div class="container">
 
 	<div class= "col-md-2">
 		<jsp:include page="leftBarProfile.jsp"></jsp:include>
 	</div>
  <div class="col-md-7 col-md-offset-1 jumbotron">
  <%Client user = (Client) session.getAttribute("user"); %>
 <form id="profileForm" class="form-horizontal profileForm">
					<!-- This should be a POST but as we dont save anything it is not added -->
					
					<fieldset>
						<legend>Client Information</legend>
						<!-- First Name-->
						<div class="form-group">
							<label class="col-md-2 control-label">First
								Name</label>
							<div class="col-md-4 form-control-static">
								<%=user.getFirstName() %>
						
							</div>
						</div>
						<div class="form-group">
							<!-- Second Name-->

							<label class="col-md-2 control-label" >Second
								Name</label>
							<div class="col-md-4 form-control-static">
								<%=user.getSecondName() %>
							</div>
						</div>

						<!-- Email-->
						<div class="form-group">
							<label class="col-md-2  control-label">Email</label>
							<div class="col-md-8 form-control-static">
								<%=user.getEmail() %>
							</div>
						</div>

						
						</fieldset>
						
						<fieldset>
					

						<legend>
							<small>Shipping Information</small>
						</legend>

						<!-- Address-->
						<div class="form-group">
							<label class="col-md-2 control-label">Address</label>
							<div class="col-md-9  form-control-static">
								<%=user.getAddress() %>
							</div>
						</div>

						<!-- 
						<div class="form-group">
							<label class="col-md-2 control-label" >City</label>
							<div class="col-md-4 form-control-static">
								Madrid
							</div>
							</div>
							
							
							<div class="form-group">						

							<label class="col-md-2 control-label">ZIP
								Code</label>
							<div class="col-md-3 form-control-static">
								28123
							</div>
						</div> -->


					</fieldset>
					<a href = "#modalProfile" data-toggle="modal"><button class="btn btn-default">Edit</button></a>
				</form>
				
				</div>
				</div>
				 <jsp:include page="editProfile.jsp"></jsp:include>
				 <jsp:include page="footer.jsp"></jsp:include>
 
 
 
 
 
</body>
</html>