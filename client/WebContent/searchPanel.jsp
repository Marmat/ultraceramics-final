<%@page import="model.Category"%>
<%@page import="java.util.List"%>
<%@page import="db.CategoryManager"%>
<div class="panel">
	<div class="panel-body">
		<h5>Search panel</h5>
		<form class="form col-md-2 left-block" method="POST"
			action="search" style= "text-align: left;">
			<div class="form-group">
				Product name:<input style="width: 140px;" type="text" name="productName" id="productName" />
				Category: <select id="selectCategory" name="selectCategory">
					<option value="any" selected>Any category</option>
					<%
						CategoryManager manager = new CategoryManager();
						for (Category category : manager.getCategoryList()) {
							String value = category.getName();
					%>
					<option value=<%=value%>>
						<%=value%>
					</option>
					<%
						}
					%>
				</select> Supplier name:<input style="width: 140px;" type="text" name="supplierName"
					id="productName" />
			</div>
			<div class="form-group">
				Price <select id="priceType" name="priceType">

					<option value="above" selected>Above</option>
					<option value="over">Over</option>
					<option value="exact">Exact</option>
				</select> <input style="width: 140px;" type="text" name="price" id="price" />
			</div>
			<div class ="radio">
				<label>	
				<input type="radio" name="selection" id="and" value="and" checked>All inputs 
				</label>
			</div>	
			<div class ="radio">
				<label>
				<input type="radio" name="selection" id="or" value="or">Some inputs 
				</label>
			</div>
			<br/>
				<br/>
				<button type="submit" class="btn btn-primary btn-sm"
					style="margin-top: 10px">Search</button>
		</form>
	</div>
</div>


<!-- 					<div class = "col-md-5"> -->
<!-- 					<div class ="input-group"> -->
<!-- 					<input type = "text" placeholder = "col-md-2" class = "form-control"> -->
<!-- 					<span class="input-group-addon">%</span> -->
<!-- 					</div> -->
					
<!-- 					</div> -->