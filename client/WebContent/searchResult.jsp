<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Search result</title>
</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>
	<div class = "container">
	<div class="row">
		<div class="col-md-2">
			<div class="row">
			<jsp:include page="leftBarCategory.jsp"></jsp:include>
			</div>
			<div class="row">
			<jsp:include page="searchPanel.jsp"></jsp:include>
			</div>
	</div>
	<div class= "col-md-9">
		<jsp:include page="catalogue.jsp"></jsp:include>
	</div>	
	</div>
	</div>
	<div class="row">
		<jsp:include page="footer.jsp"></jsp:include>
	</div>
</body>
</html>