<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page import="java.lang.String"%>
<jsp:useBean id="discountCode" scope="request" class="java.lang.String" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

<jsp:include page="header.jsp"></jsp:include>
<div class ="jumbotron row">
<div class="panel col-md-12">
<div class= "row">
<div class="col-md-12">
<h3 style="text-align: center; color: yellow;">CONGRATULATIONS YOU EARNED A DISCOUNT TICKET</h3>
</div>
</div>
<div class="row">
<p style="text-align: center; font-size: 40px;"> <b>Your code is:</b>
<span style="color:#31B404">  <%=discountCode %></span>
</p>
</div>
</div>
	<a class="btn btn-primary btn-lg" href="index.html">Go Back</a>
</div>
</body>
<jsp:include page="footer.jsp"></jsp:include>
</html>