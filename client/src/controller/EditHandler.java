package controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.ejb.EJB;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import utils.Cart;
import utils.ProductAmountExceededException;
import db.ClientManager;
import db.OrderManager;
import db.ProductManager;
import ejbModule.TicketInterface;
import model.Client;
import model.OrderList;
import model.Paymenttype;
import model.Product;
import model.ProductList;
import model.Productsupplied;
import model.ProductsuppliedList;
import model.ProductsuppliedPK;
import model.Supplier;

public class EditHandler implements RequestHandler {
	private HttpSession session;
	private static final String targetURL = "http://localhost:8080/bank/bankRequest/authorization/order";

	private TicketInterface codeGenerator;

	@Override
	public String handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		session = request.getSession(true);
		String path = request.getServletPath();

		switch (path) {
		case "/doLogin.html":
			return login(request.getParameter("username"),
					request.getParameter("password"));
		case "/login.html":
			return "/login.jsp";
		case "/register.html":
			return "/register.jsp";
		case "/category.html":
			return manageCategory(request);
		case "/payment.html":
			return "/payment.jsp";
		case "/cart.html":
			return "/cart.jsp";
		case "/editProfile.html":
			return doEditProfile(request, response);
		case "/profile.html":
			return "/profile.jsp";
		case "/product":
			return loadProduct(request);
		case "/about.html":
			return "/about.jsp";
		case "/contact.html":
			return "/contact.jsp";
		case "/logout.html":
			return doLogout();
		case "/doRegister.html":
			return register(request, response);
		case "/index.html":
			loadAllProducts(request, response);
			return "/index.jsp";
		case "/addToCart":
			return addToCart(request);
		case "/removeFromCart":
			return removeFromCart(request);
		case "/changeAmount":
			return changeAmount(request);
		case "/doPayment":
			return doPayment(request);
		case "/search":
			return searchProduct(request);
		case "/orders":
			return loadOrders(request);
		case "/changePage":
			return changePage(request);
		case "/subscribed.html":
			return loadSuscribed(request);
		case "/subscribe":
			return subscribe(request);
		case "/sort":
			return sort(request);
		case "/addDiscount":
			return addDiscount(request);
		case "/showTicket.html":
			return "/showTicket.jsp";
		case "/removeSub.html":
			return removeSub(request); 
		case "/":
			loadAllProducts(request, response);
		}

		return "/error.jsp";
	}

	private String addDiscount(HttpServletRequest request) {
		System.out.println("here");
		Cart cart = (Cart) session.getAttribute("cart");
		System.out.println("here");
		String discountCode = request.getParameter("discountCode");
		System.out.println("here");
		cart.addDiscount(discountCode);
		System.out.println("here");
		return "/cart.jsp";
	}

	private String sort(HttpServletRequest request) {
		ProductManager pm = new ProductManager();
		ProductsuppliedList productList = (ProductsuppliedList) session
				.getAttribute("productsuppliedList");
		String mode = request.getParameter("mode");
		String ascendendParameter = request.getParameter("ascendent");
		boolean ascendent = ascendendParameter.equals("true");
		request.setAttribute("productsuppliedList",
				pm.orderProducts(productList, mode, ascendent));
		return "searchResult.jsp";
	}

	private String subscribe(HttpServletRequest request) {
		int productID = Integer.parseInt(request.getParameter("productID"));

		ProductManager pm = new ProductManager();
		Product product = pm.retriveProductFromIDs(productID);
		pm.subscribe(product,(Client) session.getAttribute("user"));
		return "/subscribed.html";
	}
	
	private String removeSub(HttpServletRequest request) {
		int productID = Integer.parseInt(request.getParameter("productID"));
		int clientID = Integer.parseInt(request.getParameter("clientID"));
		ProductManager pm = new ProductManager();
		pm.removeSubscription(productID,clientID);
		return "/subscribed.html";
	}

	private String loadSuscribed(HttpServletRequest request) {
		ProductManager pm = new ProductManager();
		Client client = (Client) (session.getAttribute("user"));
		ProductList products = pm.retriveProductSuscribed(client.getId());
		request.setAttribute("productList", products);
		return "/Subscriptions.jsp";
	}

	private String changePage(HttpServletRequest request) {
		String pag = request.getParameter("pag");
		request.setAttribute("pag", pag);
		return "/catalogue.jsp";
	}

	private String loadOrders(HttpServletRequest request) {
		OrderManager om = new OrderManager();
		Client client = (Client) (session.getAttribute("user"));
		int clientID = client.getId();
		OrderList orders = om.getOrdersOfClient(clientID);
		request.setAttribute("orderList", orders);
		return "/history.jsp";
	}

	private String searchProduct(HttpServletRequest request) {
		String searchType = request.getParameter("selection");
		String productName = request.getParameter("productName");
		String supplierName = request.getParameter("supplierName");
		String category = request.getParameter("selectCategory");
		String priceType = request.getParameter("priceType");
		Integer price;
		try {
			price = Integer.parseInt(request.getParameter("price"));
		} catch (Exception e) {
			price = null;
		}
		ProductManager pm = new ProductManager();
		ProductsuppliedList productList;
		System.out.println(searchType);
		if (searchType.equals("and")) {
			productList = pm.searchProductAnd(productName, supplierName,
					category, priceType, price);
		} else {
			productList = pm.searchProductOr(productName, supplierName,
					category, priceType, price);
		}
		request.setAttribute("productsuppliedList", productList);
		return "/searchResult.jsp";
	}

	private String doPayment(HttpServletRequest request) {

		String creditCardNumber = request.getParameter("cardnum");
		Cart cart = (Cart) session.getAttribute("cart");
		String shippingInformation = request.getParameter("addressShip") + ", "
				+ request.getParameter("cityShip") + ", "
				+ request.getParameter("zipShip");
		String billingInformation = request.getParameter("addressBill") + ", "
				+ request.getParameter("cityBill") + ", "
				+ request.getParameter("zipBill");
		;
		Context context;
		try {
			context = new InitialContext();
			codeGenerator = (TicketInterface) context
					.lookup("java:global/ultraceramics/ejbModule/TicketEJB");
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Paymenttype pt = new Paymenttype();
		pt.setId(1);
		pt.setType("CREDIT CARD");
		pt.setInfo("CREDIT CARD INFO");
		if ((creditCardNumber != null) && (cart != null)) {
			Client client = (Client) session.getAttribute("user");
			String orderCode = codeGenerator.generateOrderCode();
			if (checkCreditCardBank(creditCardNumber, cart.getTotal(),
					orderCode)) {
				String ticket = cart.sendOrder(billingInformation,
						shippingInformation, pt, client, orderCode);
				cart = new Cart();
				session.setAttribute("cart", cart);
				if (ticket != null) {
					request.setAttribute("discountCode", ticket);
					return "/showTicket.html";
				}
				return "/index.html";
			} else {
				return "/payment.html";
			}

		}
		return null;
	}

	private String changeAmount(HttpServletRequest request) {
		Cart cart = (Cart) session.getAttribute("cart");
		int productID = Integer.parseInt(request.getParameter("productID"));
		int supplierID = Integer.parseInt(request.getParameter("supplierID"));
		int amount;
		try{
			amount = Integer.parseInt(request.getParameter("value"));
		} catch (NumberFormatException e){
			amount = 1;
		}
		try {
			cart.updateProductAmount(productID, supplierID, amount);

		} catch (ProductAmountExceededException e) {
			request.setAttribute("productExceeded", e.getProductExceeded());

		}
		return "/cart.jsp";
	}

	private String removeFromCart(HttpServletRequest request) {
		int productID = Integer.parseInt(request.getParameter("productID"));
		int supplierID = Integer.parseInt(request.getParameter("supplierID"));
		Cart cart = (Cart) session.getAttribute("cart");
		ProductsuppliedPK key = new ProductsuppliedPK();
		key.setProductID(productID);
		key.setSupplierID(supplierID);
		cart.removeProduct(key);
		return "/cart.html";
	}

	private String addToCart(HttpServletRequest request) {
		if (session.getAttribute("user") == null) {
			return "/login.jsp";
		}
		int productID = Integer.parseInt((request.getParameter("productID")));
		int supplierID = Integer.parseInt((request.getParameter("supplierID")));
		ProductsuppliedPK key = new ProductsuppliedPK();
		key.setProductID(productID);
		key.setSupplierID(supplierID);
		Cart cart = (Cart) session.getAttribute("cart");
		if (cart == null) {
			cart = new Cart();
			session.setAttribute("cart", cart);
		}
		cart.addProduct(key);
		return "/cart.html";
	}

	private void loadAllProducts(HttpServletRequest request,
			HttpServletResponse response) {
		ProductManager manager = new ProductManager();
		ProductList productList = manager.retriveProducts();
		request.setAttribute("productList", productList);

	}

	private String doLogout() {
		System.out.println("####### doLogout #######");
		session.setAttribute("user", null);
		return "/index.jsp";
	}

	private String manageCategory(HttpServletRequest request) {
		String categoryName = (String) request.getParameter("category");
		Client user = (Client) session.getAttribute("user");
		String userID="";
		if(user!=null){
			userID = user.getId()+"";
		}
		System.out.println("category name: " + categoryName);
		ProductManager manager = new ProductManager();
		ProductList productList = manager.retriveProductsFromCategory(userID,categoryName);
		request.setAttribute("productList", productList);
		session.setAttribute("category", categoryName);
		return "/category.jsp";
	}

	private String login(String username, String password) {
		if ((password.length() == 0) || (username.length() == 0)) {
			session.setAttribute("error", true);
			return "/login.jsp";
		} else {
			ClientManager cm = new ClientManager();
			Client client = cm.login(username, password);
			if (client == null) {
				session.setAttribute("error", true);
				return "/login.jsp";
			}
//			session.setAttribute("username", username);
			session.setAttribute("user", client);
		System.out.println(session.getAttribute("user"));
			return "/index.html";
		}
	}

	private String loadProduct(HttpServletRequest request) {
		int productID = Integer.parseInt(request.getParameter("productID"));
		int supplierID = Integer.parseInt(request.getParameter("supplierID"));
		ProductManager pm = new ProductManager();
		Productsupplied product = pm.retriveProductFromIDs(productID,
				supplierID);
		request.setAttribute("selectedProduct", product);
		return "/product.jsp";

	}

	private String register(HttpServletRequest request,
			HttpServletResponse response) {
		ClientManager cm = new ClientManager();
		Client client = new Client();
		String username = (String) request.getParameter("username");
		String password = (String) request.getParameter("passwordinput1");
		String email = (String) request.getParameter("email1");
		String firstName = (String) request.getParameter("firstname");
		String secondName = (String) request.getParameter("secondname");
		String address = (String) request.getParameter("address");
		String city = (String) request.getParameter("city");
		String zip = (String) request.getParameter("zip");
		
		if(address.isEmpty()) address = "null";
		if(city.isEmpty()) city = "null";
		if(zip.isEmpty()) zip = "null";

		client.setAddress(address+","+zip+","+city);
		
		
		client.setUsername(username);
		client.setPassword(password);
		client.setEmail(email);
		client.setFirstName(firstName);
		client.setSecondName(secondName);
		
		
		if (cm.register(client)) {
			return "/login.jsp";
		}
		return "/error.jsp";
	}

	private boolean checkCreditCardBank(String ccNumber, double amount,
			String orderCode) {
		try {
			URL targetUrl = new URL(targetURL);
			HttpURLConnection httpConnection = (HttpURLConnection) targetUrl
					.openConnection();
			httpConnection.setDoOutput(true);
			httpConnection.setRequestMethod("POST");
			httpConnection.setRequestProperty("Content-Type",
					"application/json");
			String input = "{\"creditCardNumber\":\"" + ccNumber
					+ "\",\"amount\":" + amount + ",\"orderCode\":\""
					+ orderCode + "\"}";
			OutputStream outputStream = httpConnection.getOutputStream();
			outputStream.write(input.getBytes());
			outputStream.flush();
			BufferedReader responseBuffer = new BufferedReader(
					new InputStreamReader(httpConnection.getInputStream()));
			String output;
			System.out.println("output from Server");
			boolean returnValue = false;
			while ((output = responseBuffer.readLine()) != null) {
				System.out.println("################ " + output
						+ " ###############");
				if (output
						.equals("{\"operationCode\":\"error\",\"response\":\"Reject\"}")) {
					returnValue = false;
				} else {
					returnValue = true;
				}
			}
			httpConnection.disconnect();
			return returnValue;
		} catch (Exception e) {
			System.out.println("###############ERROR###############");
			e.printStackTrace();
			return false;
		}
	}
	
	private String doEditProfile(HttpServletRequest request, HttpServletResponse response) {
			
			System.out.println("\n\n########  edit_profile_function ########\n\n");
			
			ClientManager cm = new ClientManager();
			Client oldClient = (Client) session.getAttribute("user");
			
			Client newClient = new Client();
			newClient.setAddress(oldClient.getAddress());
			newClient.setEmail(oldClient.getEmail());
			newClient.setId(oldClient.getId());
			newClient.setFirstName(oldClient.getFirstName());
			newClient.setSecondName(oldClient.getSecondName());
			newClient.setPassword(oldClient.getPassword());
			newClient.setOrders(oldClient.getOrders());
			newClient.setProducts(oldClient.getProducts());
			newClient.setSubscriptions(oldClient.getSubscriptions());
			newClient.setUsername(oldClient.getUsername());
			
			System.out.println("\n\n########  get_session_attributes ########\n\n");
			
			String firstname = (String) request.getParameter("firstname");
			String secondname = (String) request.getParameter("secondname");
			String email1 = (String) request.getParameter("email1");
			String email2 = (String) request.getParameter("email2");
			String password1 = (String) request.getParameter("passwordinput1");
			String password2 = (String) request.getParameter("passwordinput2");
			String address = (String) request.getParameter("address");
			String city = (String) request.getParameter("city");
			String zip = (String) request.getParameter("zip");
			String originalPassword = (String) request.getParameter("passwordorig");		
			
			System.out.println("\n\n########  get_form_attributes ########\n\n");
			
			String oldAddress = (String) request.getParameter("oldAddress");
			String oldCity = (String) request.getParameter("oldCity");
			String oldZip = (String) request.getParameter("oldZip");
			
			System.out.println("\n\n########  get_old_form_attributes ########\n\n");
			System.out.println("\n\n########  original_password: " + originalPassword + " ########\n\n");
			System.out.println("\n\n########  old_password: " + oldClient.getPassword() + " ########\n\n");
			if(   !(originalPassword.isEmpty()) &&  !(oldClient.getPassword().isEmpty())   )
			{
				if( originalPassword.compareTo(oldClient.getPassword())==0 )
				{
					System.out.println("\n\n########  password correct ########\n\n");
					if(firstname.isEmpty()) firstname = oldClient.getFirstName();
					if(secondname.isEmpty()) secondname = oldClient.getSecondName();
					System.out.println("\n\n########  email1: " + email1 + " ########\n\n");
					System.out.println("\n\n########  email2: " + email2 + " ########\n\n");
					if(   !(email1.isEmpty()) &&  !(email2.isEmpty())   )
						if( email1.compareTo(email2)==0 )
						{
							System.out.println("\n\n######## UPDATE_EMAIL ########\n\n");
							if(email1.isEmpty() == false )
								newClient.setEmail(email1);
							
						}
					System.out.println("\n\n########  password1: " + password1 + " ########\n\n");
					System.out.println("\n\n########  password2: " + password2 + " ########\n\n");
					if(   !(password1.isEmpty()) &&  !(password2.isEmpty())   )
						if( password1.compareTo(password2)==0 )
						{
							System.out.println("\n\n######## UPDATE_PASSWORD ########\n\n");
							if(password1.isEmpty() == false)
								newClient.setPassword(password1);
						}
					if(address.isEmpty()) address = oldAddress;
					if(city.isEmpty()) city = oldCity;
					if(zip.isEmpty()) zip = oldZip;
					String compoundAddress = address+","+zip+","+city; compoundAddress = compoundAddress.replaceAll("_", " ");
					newClient.setAddress(compoundAddress);
					
					if(firstname.isEmpty() == false)
						newClient.setFirstName(firstname);
					
					if(secondname.isEmpty() == false)
						newClient.setSecondName(secondname);
					
					session.removeAttribute("user");
					session.setAttribute("user", newClient);
					if (cm.editClient(oldClient.getId(),newClient) != null) {
						return "/profile.jsp";
					}
				}
			}
			System.out.println("\n\n########  password INcorrect ########\n\n");
			// WE MUST CHANGE IT TO ERROR PAGE
			return "/error.jsp";
		}
	
	}
