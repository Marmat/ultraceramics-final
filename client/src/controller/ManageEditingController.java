package controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.json.JsonObject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sun.org.mozilla.javascript.internal.json.JsonParser;

/**
 * Servlet implementation class ManageFormController
 */
@WebServlet(name = "ManageEditingController", urlPatterns = {
		"/ManageEditingController", "/doLogin.html", "/login.html",
		"/register.html", "/category.html", "/payment.html", "/cart.html",
		"/payment.html", "/editProfile.html", "/profile.html", "/product",
		"/about.html", "/contact.html", "/logout.html", "/doRegister.html",
		"/index.html", "/addToCart", "/removeFromCart", "/changeAmount",
		"/doPayment", "/search", "/orders", "/changePage", "/nextPage",
		"/prevPage", "/subscribed.html", "/subscribedOnSale.html" ,"/subscribe","/sort","/addDiscount","/showTicket.html", "/removeSub.html"})
public class ManageEditingController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Map<String, RequestHandler> pageMap;
	private String[] navigationPages = { "/login.html", "/register.html",
			"/category.html", "/payment.html", "/cart.html", "/payment.html",
			"/editProfile.html", "/profile.html", "/product", "/doLogin.html",
			"/about.html", "/contact.html", "/logout.html", "/doRegister.html",
			"/index.html", "/addToCart", "/removeFromCart", "/changeAmount",
			"/doPayment", "/search", "/orders", "/changePage", "/nextPage",
			"/prevPage", "/subscribed.html", "/subscribedOnSale.html","/subscribe","/sort","/addDiscount","/showTicket.html", "/removeSub.html" };

	
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ManageEditingController() {
		super();
		// TODO Auto-generated constructor stub
	}

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		mapPages();

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String path = request.getServletPath();
		System.out.println(path);
		if (path != null) {
			RequestHandler handler = pageMap.get(path);
			if (handler != null) {
				String view = handler.handleRequest(request, response);
				if (view != null) {
					RequestDispatcher rdp = request.getRequestDispatcher(view);
					rdp.forward(request, response);
				}
			} else {
				/* Error page */
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String path = request.getServletPath();
		System.out.println(path);
		if (path != null) {
			RequestHandler handler = pageMap.get(path);
			if (handler != null) {
				String view = handler.handleRequest(request, response);
				if (view != null) {
					RequestDispatcher rdp = request.getRequestDispatcher(view);
					rdp.forward(request, response);
				}
			} else {
				/* Error page */
			}
		}
	}

	private void mapPages() {
		pageMap = new HashMap<String, RequestHandler>();
		for (String page : navigationPages) {
			pageMap.put(page, new EditHandler());
		}
	}
	


}
