package db;


import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import model.Category;



public class CategoryManager {
	private Properties properties;
	private String persistanceUnit = "Model";
	EntityManagerFactory emf;

	public CategoryManager() {
		setupProperties();
	}

	public List<Category> getCategoryList(){
		EntityManager em = emf.createEntityManager();
		Query query = (Query) em.createQuery("SELECT c FROM Category c ORDER BY c.name");
		List<?> queryResult = query.getResultList();
		List<Category> resultList = new ArrayList<Category>();
		for(Object o : queryResult){
			resultList.add((Category)o);
		}
		em.close();
		emf.close();
		return resultList;
		
		
	}

	private void setupProperties() {
		properties = new Properties();
		/*
		 * properties.put(PersistenceUnitProperties.CLASSLOADER, this.getClass()
		 * .getClassLoader());
		 */
		/*properties.put(PersistenceUnitProperties.CLASSLOADER,
				this.getClass().getClassLoader());
		properties.put(PersistenceUnitProperties.ECLIPSELINK_PERSISTENCE_UNITS,
				persistanceUnit);*/
		emf = Persistence.createEntityManagerFactory(persistanceUnit,
				properties);
	}
}
