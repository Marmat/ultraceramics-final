package db;

import java.util.List;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import model.Client;
import model.Supplier;

import org.eclipse.persistence.config.PersistenceUnitProperties;

public class ClientManager {
	private Properties properties;
	private String persistanceUnit = "Model";
	EntityManagerFactory emf;

	public ClientManager() {
		setupProperties();
	}

	public boolean register(Client client) {
		// fill with the name of your
		// persistence unit

		// 2 Create the Entity Manager
		EntityManager em = emf.createEntityManager();
		// 3 Get one EntityTransaction and start it
		EntityTransaction et = em.getTransaction();
		et.begin();
		em.persist(client);
		et.commit();
		em.close();
		emf.close();
		return true;
	}

	public Client login(String username, String password) {
		EntityManager em = emf.createEntityManager();
		Query query = em
				.createQuery("SELECT c FROM Client c WHERE c.username='"
						+ username + "' AND c.password='" + password + "'");
		List<?> queryResult = query.getResultList();
		if (queryResult.size() == 0) {
			return null;
		}
		Client client = (Client) queryResult.get(0);
		em.close();
		emf.close();
		return client;
	}

	private void setupProperties() {
		properties = new Properties();
		/*
		 * properties.put(PersistenceUnitProperties.CLASSLOADER, this.getClass()
		 * .getClassLoader());
		 */
		/*properties.put(PersistenceUnitProperties.CLASSLOADER,
				this.getClass().getClassLoader());
		properties.put(PersistenceUnitProperties.ECLIPSELINK_PERSISTENCE_UNITS,
				persistanceUnit);*/
		emf = Persistence.createEntityManagerFactory(persistanceUnit,
				properties);
	}
	
	public Client editClient(int clientID, Client newClient) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		
		Client oldClient = em.find(Client.class, clientID);
		oldClient.setAddress(newClient.getAddress());
		oldClient.setEmail(newClient.getEmail());
		oldClient.setId(newClient.getId());
		oldClient.setFirstName(newClient.getFirstName());
		oldClient.setSecondName(newClient.getSecondName());
		oldClient.setPassword(newClient.getPassword());
		oldClient.setOrders(newClient.getOrders());
		oldClient.setProducts(oldClient.getProducts());
		oldClient.setSubscriptions(oldClient.getSubscriptions());
		oldClient.setUsername(oldClient.getUsername());
		
		em.getTransaction().commit();
		em.close();
		return oldClient;
	}
}
