package db;

import java.util.List;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import model.OrderList;
import model.Order;

public class OrderManager {
	private Properties properties;
	private String persistanceUnit = "Model";
	EntityManagerFactory emf;

	public OrderManager() {
		setupProperties();
	}


	public OrderList getOrdersOfClient(int clientID) {
		EntityManager em = emf.createEntityManager();
		Query query = em
				.createQuery("SELECT o FROM Order o WHERE o.client.id="+clientID + " ORDER BY o.date");
		List<?> queryResult = query.getResultList();
		if (queryResult.size() == 0) {
			return null;
		}
		OrderList orders = new OrderList();
		for(Object o : queryResult){
			Order order = (Order) o;
			orders.add(order);
		}
		em.close();
		emf.close();
		return orders;
	}

	private void setupProperties() {
		properties = new Properties();
		/*
		 * properties.put(PersistenceUnitProperties.CLASSLOADER, this.getClass()
		 * .getClassLoader());
		 */
		/*properties.put(PersistenceUnitProperties.CLASSLOADER,
				this.getClass().getClassLoader());
		properties.put(PersistenceUnitProperties.ECLIPSELINK_PERSISTENCE_UNITS,
				persistanceUnit);*/
		emf = Persistence.createEntityManagerFactory(persistanceUnit,
				properties);
	}
}
