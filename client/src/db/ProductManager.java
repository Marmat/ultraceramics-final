package db;

import java.util.Comparator;
import java.util.List;
import java.util.Properties;
import java.util.TreeSet;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.transaction.Transaction;

import model.Client;
import model.GenericProductInterface;
import model.Product;
import model.ProductList;
import model.ProductListInterface;
import model.ProductsuppliedList;
import model.Productsupplied;
import model.ProductsuppliedPK;
import model.Subscription;

import org.eclipse.persistence.config.PersistenceUnitProperties;



public class ProductManager {
	private Properties properties;
	private String persistanceUnit = "Model";
	EntityManagerFactory emf;

	public ProductManager() {

		setupProperties();
	}

	public ProductList retriveProducts() {
		EntityManager em = emf.createEntityManager();
		String queryText = "SELECT p FROM Product p";
		Query selectQuery = em.createQuery(queryText);
		List<?> result;
		result = selectQuery.getResultList();
		ProductList returnList = new ProductList();
		for (Object o : result) {
			returnList.add((Product) o);
		}
		em.close();
		emf.close();
		return returnList;
	}

	public boolean isProductSubscribed(String productID, String clientID){
		System.out.println("###### isProductSubscribed ######");
		System.out.println("###### productID: "+productID+"\t clientID: "+clientID+" ######");
		EntityManager em = emf.createEntityManager();
		String queryText = "SELECT p FROM Product p JOIN Subscription s "
				+ "WHERE (s.client.id='"+clientID+"' AND p.idProduct=s.product.idProduct AND p.idProduct='"+productID+"') ";
		System.out.println("###### query: "+queryText+" ######");
		List<?> result;
		Query selectQuery = em.createQuery(queryText);
		result = selectQuery.getResultList();
		ProductList returnList = new ProductList();

		for (Object o : result) {
			returnList.add((Product) o);
		}
		System.out.println("###### productID: "+productID+"\t clientID: "+clientID+"\t SUB? = "+returnList.size()+" ######");
		em.close();
		System.out.println("###### END_isProductSubscribed ######");
		if(returnList.size()==0){
			return false;
		}
		else
			return true;
	}
	
	public ProductList retriveProductSuscribed(int clientID) {
		EntityManager em = emf.createEntityManager();
		String queryText = "SELECT s FROM Subscription s WHERE s.client.id = '"
				+ clientID + "'";
		
		List<?> result;
		Query selectQuery = em.createQuery(queryText);
		result = selectQuery.getResultList();
		ProductList returnList = new ProductList();

		for (Object o : result) {
			Subscription subscription = (Subscription) o;
			returnList.add(subscription.getProduct());
		}
		em.close();
		return returnList;
	}
	
	public ProductList retriveProductsFromCategory(String userID, String categoryName) {
		EntityManager em = emf.createEntityManager();
		
		String queryText;
		if(categoryName.compareTo("subscribedOnSale")==0){
			queryText = "SELECT p FROM Product p JOIN Subscription s, Productsupplied ps "
					+ "WHERE (s.client.id='"+userID+"' AND p.idProduct=s.product.idProduct) "
							+ "AND (ps.product=p AND ps.onSale=true)";
		}
		else if(categoryName.compareTo("subscribed")==0){
			queryText = "SELECT p FROM Product p JOIN Subscription s "
					+ "WHERE (s.client.id='"+userID+"' AND p.idProduct=s.product.idProduct) ";
		}
		else if(categoryName.compareTo("onSale")==0){
			queryText = "SELECT p FROM Product p JOIN Productsupplied ps "
					+ "WHERE (ps.product=p AND ps.onSale=true) ";
		}
		else{
			queryText = "SELECT p FROM Product p WHERE p.category.name = '"+ categoryName + "'";
		}
		
		List<?> result;
		Query selectQuery = em.createQuery(queryText);
		result = selectQuery.getResultList();
		ProductList returnList = new ProductList();

		for (Object o : result) {
			returnList.add((Product) o);
		}
		em.close();
		return returnList;
	}

	public Productsupplied retriveProductFromIDs(int productID, int supplierID) {
		EntityManager em = emf.createEntityManager();
		ProductsuppliedPK key = new ProductsuppliedPK();
		key.setProductID(productID);
		key.setSupplierID(supplierID);
		Productsupplied product = em.find(Productsupplied.class, key);
		return product;
	}
	
	public Product retriveProductFromIDs(int productID) {
		EntityManager em = emf.createEntityManager();
		
	
		Product product = em.find(Product.class, productID);
		return product;
	}
	
	public boolean removeSubscription(int productID, int clientID) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		
		
		System.out.println("######### removeSubscription #########");
		System.out.println("######### productID: "+productID+" #########");
		System.out.println("######### clientID: "+clientID+" #########");
		//find subscription "sub"
		String queryText = "SELECT s FROM Subscription s "
				+ "WHERE s.client.id='"+clientID+"' AND s.product.idProduct='"+productID+"'";
		List<?> result;
		Query selectQuery = em.createQuery(queryText);
		result = selectQuery.getResultList();
		Subscription sub = new Subscription();
		sub = (Subscription) result.get(0);
		System.out.println("######### sub: "+sub+" #########");
		
		//delete subscription
		em.remove(sub);
		System.out.println("######### removed #########");
		em.getTransaction().commit();
		em.close();
		System.out.println("######### END_removeSubscription #########");
		
		return true;
	}

	public ProductsuppliedList searchProductAnd(String productName,
			String supplierName, String category, String priceType, Integer price) {
		EntityManager em = emf.createEntityManager();
		String queryText = "SELECT p FROM Product p";
		System.out.println(productName+"  "+supplierName+"  "+category);
		queryText = "SELECT ps FROM Productsupplied ps JOIN  ps.product p, ps.supplier s WHERE ";
		boolean name = false;
		boolean supplier = false;
		boolean cat = false;
		boolean priceSel = false;
		if(productName.length()>0){
			queryText += "p.name='"+productName+"' ";
			name = true;
		}
		if(supplierName.length()>0){
			if(name){
				queryText += "AND ";
			}
			queryText += "s.name='"+supplierName+"' ";
			supplier = true;
		}
		if(!category.equals("any")){
			if(name||supplier){
				queryText += "AND ";
			}
			queryText += "p.category.name = '"+ category+"' ";
			cat = true;
		}
		if(price!=null){
			if(name||supplier||cat){
				queryText += "AND ";
			}
			String operator="";
			switch(priceType){
			case "above":
				operator = "<";
				break;
			case "over" :
				operator = ">";
				break;
			case "exact" :
				operator = "=";
			}
			queryText += "ps.sellPrice "+operator+" "+price+" ";
			priceSel = true;
		}
		if((!cat)&&(!supplier)&&(!name)&&(!priceSel)){
			queryText = "SELECT ps FROM Productsupplied ps JOIN  ps.product p, ps.supplier s";
		}

		return performQuery(em, queryText);

	}
	
	public void subscribe(Product product,Client client){
		EntityManager em = emf.createEntityManager();
		EntityTransaction et = em.getTransaction();
		try{
		et.begin();
		Subscription subscription = new Subscription();
		subscription.setClient(client);
		subscription.setProduct(product);
		em.persist(subscription);
		et.commit();
		} catch (Exception e){
			
		} finally {
			em.close();
		}
	}

	public ProductsuppliedList searchProductOr(String productName, String supplierName,
			String category,String priceType,Integer price) {
		EntityManager em = emf.createEntityManager();
		String queryText = "SELECT p FROM Product p";
		System.out.println(productName+"  "+supplierName+"  "+category);
		queryText = "SELECT ps FROM Productsupplied ps JOIN  ps.product p, ps.supplier s WHERE ";
		boolean name = false;
		boolean supplier = false;
		boolean cat = false;
		boolean priceSel = false;
		if(productName.length()>0){
			queryText += "p.name='"+productName+"' ";
			name = true;
		}
		if(supplierName.length()>0){
			if(name){
				queryText += "OR ";
			}
			queryText += "s.name='"+supplierName+"' ";
			supplier = true;
		}
		if(!category.equals("any")){
			if(name||supplier){
				queryText += "OR ";
			}
			queryText += "p.category.name = '"+ category+"' ";
			cat = true;
		}
		if(price!=null){
			if(name||supplier||cat){
				queryText += "OR ";
			}
			String operator="";
			switch(priceType){
			case "above":
				operator = "<";
				break;
			case "over" :
				operator = ">";
				break;
			case "exact" :
				operator = "=";
			}
			queryText += "ps.sellPrice "+operator+" "+price+" ";
			priceSel = true;
		}
		if((!cat)&&(!supplier)&&(!name)&&(!priceSel)){
			queryText = "SELECT ps FROM Productsupplied ps JOIN  ps.product p, ps.supplier s";
		}

		return performQuery(em, queryText);

	}
	
	public ProductsuppliedList orderProducts(ProductsuppliedList productList, String mode, boolean ascendent){
		Comparator<Productsupplied> comparator;
		switch (mode) {
		case "name":
			comparator = new CompareNames(ascendent);
			break;
		case "price":
			comparator = new ComparePrice(ascendent);
			break;
		case "supplier":
			comparator = new CompareSuppliers(ascendent);
			break;
		default:
			comparator = new CompareNames(ascendent);
		}
		TreeSet<Productsupplied> orderedSet = new TreeSet<Productsupplied>(comparator);
		orderedSet.addAll(productList);
		System.out.println("ORDERED:");
		for(GenericProductInterface product : orderedSet){
			System.out.println(product.getProductName());
		}
		ProductsuppliedList returnList = new ProductsuppliedList();
		returnList.addAll(orderedSet);
		System.out.println("ORDERED:");
		for(GenericProductInterface product : returnList){
			System.out.println(product.getProductName());
		}
		return returnList;
	}

	private ProductsuppliedList performQuery(EntityManager em, String queryText){
		List<?> result;
		Query selectQuery = em.createQuery(queryText);
		result = selectQuery.getResultList();
		ProductsuppliedList returnList = new ProductsuppliedList();
		System.out.println(result.size());
		for (Object o : result) {
			returnList.add((Productsupplied) o);
		}
		em.close();
		return returnList;
	}
	private void setupProperties() {
		properties = new Properties();
		/*
		 * properties.put(PersistenceUnitProperties.CLASSLOADER, this.getClass()
		 * .getClassLoader());
		 */
		/*
		 * properties.put(PersistenceUnitProperties.CLASSLOADER,
		 * this.getClass().getClassLoader());
		 * properties.put(PersistenceUnitProperties
		 * .ECLIPSELINK_PERSISTENCE_UNITS, persistanceUnit);
		 */
		emf = Persistence.createEntityManagerFactory(persistanceUnit,
				properties);
	}
	
	private class CompareNames implements Comparator<Productsupplied>{
		
		private boolean ascendent;
		
		public CompareNames(boolean ascendent){
			this.ascendent = ascendent;
		}

		@Override
		public int compare(Productsupplied o1,
				Productsupplied o2) {
			int compare = o1.getProductName().compareTo(o2.getProductName());
			if (!ascendent) {
				compare = -compare;
			}
			return compare;
		}
	}
	
	private class ComparePrice implements Comparator<Productsupplied>{
		private boolean ascendent;
		
		public ComparePrice(boolean ascendent){
			this.ascendent = ascendent;
		}
		@Override
		public int compare(Productsupplied o1, Productsupplied o2) {
			int compare = Integer.parseInt(o1.getSellPrice()) - Integer.parseInt(o2.getSellPrice());
			if(!ascendent){
				compare = - compare;
			}
			return compare;
		}
		
	}
	
	private class CompareSuppliers implements Comparator<Productsupplied>{
		
		private boolean ascendent;
		
		public CompareSuppliers(boolean ascendent){
			this.ascendent = ascendent;
		}

		@Override
		public int compare(Productsupplied o1,
				Productsupplied o2) {
			int compare = o1.getSupplier().getName().compareTo(o2.getSupplier().getName());
			if(compare==0){
				return new CompareNames(ascendent).compare(o1, o2);
			}
			if (!ascendent) {
				compare = -compare;
			}
			return compare;
		}
	}

}
