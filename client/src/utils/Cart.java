package utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.ejb.EJB;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;












import jersey.repackaged.com.google.common.base.Supplier;

import org.eclipse.persistence.config.PersistenceUnitProperties;

import model.Client;
import model.Discount;
import model.Order;
import model.Paymenttype;
import model.Productsupplied;
import model.ProductsuppliedPK;
import model.Purchase;
import model.PurchasePK;
import ejbModule.*;



public class Cart {
	private Properties properties;
	private String persistanceUnit = "Model";
	private EntityManagerFactory emf;
	
	
	private TicketInterface tf;
	
	//@Resource(name="jms/ConnectionFactory")
	private ConnectionFactory jmsFactory;
	//@Resource(name="jms/ultraceramicsOrderQueue")
	private Destination destination;

	private Map<Productsupplied, Integer> productSet;
	
	private Discount discount = null;

	public Cart() {
		productSet = new HashMap<Productsupplied, Integer>();
		setupProperties();
		Context context;
		try {
			context = new InitialContext();
			jmsFactory = (ConnectionFactory) context.lookup("jms/OrdersCF");
			destination = (Destination) context.lookup("jms/OrdersQ");
			
			tf = (TicketInterface) context.lookup("java:global/ultraceramics/ejbModule/TicketEJB");
			
			//System.out.println(tf.generateTicket(100));
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	public void addProduct(ProductsuppliedPK productID) {
		EntityManager em = emf.createEntityManager();
		Productsupplied product = em.find(Productsupplied.class, productID);
		System.out.println(product.getProduct().getName());
		productSet.put(product, 1);
		em.close();
	}

	public void removeProduct(ProductsuppliedPK productKey) {
		EntityManager em = emf.createEntityManager();
		Productsupplied product = selectProduct(productKey);
		productSet.remove(product);
		em.close();
	}

	public Set<Productsupplied> getProductSet() {
		return this.productSet.keySet();
	}

	public Map<Productsupplied, Integer> getProductMap() {
		return this.productSet;
	}

	public boolean updateProductAmount(int productID, int supplierID, int amount)
			throws ProductAmountExceededException {
		Productsupplied product = getProductFromIDs(productID, supplierID);
		if (product != null) {
			productSet.put(product, amount);
			checkAmountInStorage(product);
			return true;
		}
		return false;

	}

	public double getTotal() {
		double total = 0;
		for (Productsupplied product : productSet.keySet()) {
			double price = Double.parseDouble(product.getSellPrice());
			total += price * productSet.get(product);
		}
		if(discount!=null){
			double discountAmount = discount.getAmount();
			double tenPercent = total/10;
			if(discountAmount<tenPercent){
				total -= discountAmount;
			} else {
				total -= tenPercent;
			}
		}
		return total;
	}
	
	public String getDiscountCode(){
		if(discount!=null){
			return this.discount.getDiscountCode();
		} else {
			return null;
		}
	}

	private Productsupplied getProductFromIDs(int productID, int supplierID) {
		for (Productsupplied product : productSet.keySet()) {
			ProductsuppliedPK key = product.getId();
			if ((key.getProductID() == productID)
					&& (key.getSupplierID() == supplierID)) {
				return product;
			}
		}
		return null;
	}

	private Productsupplied selectProduct(ProductsuppliedPK productKey) {
		Productsupplied product = new Productsupplied();
		for (Productsupplied ps : productSet.keySet()) {
			ProductsuppliedPK key = ps.getId();
			if ((key.getProductID() == productKey.getProductID())
					&& (key.getSupplierID() == productKey.getSupplierID())) {
				product = ps;
			}
		}
		return product;
	}

	public String sendOrder(String billingAddres, String shipAddress,
			Paymenttype paymenttype, Client user, String orderCode) {
		setupProperties();
		EntityManager em = emf.createEntityManager();
		
		Order order = new Order();
		order.setBillingAddres(billingAddres);
		order.setClient(user);
		order.setDate(new Date());
		order.setPaymenttype(paymenttype);
		order.setShipAddress(shipAddress);
		order.setOrderCode(orderCode);
		if(discount!=null){
			discount.setOrder(order);
			order.setDiscount(discount);
		}
		List<Purchase> purchaseList = new ArrayList<Purchase>();
		System.out.println(order.getOrderID());
		for (Productsupplied product : productSet.keySet()) {
			Productsupplied productFinded = em.find(Productsupplied.class,
					product.getId());
			Purchase purchase = new Purchase();
			PurchasePK key = new PurchasePK();
			purchase.setOrder(order);
			key.setOrderID(order.getOrderID());
			System.out.println(key.getOrderID());
			purchase.setPrice(BigDecimal.valueOf(Double.parseDouble(product
					.getSellPrice())));
			purchase.setProductsupplied(product);
			key.setProductID(product.getId().getProductID());
			key.setSupplierID(product.getId().getSupplierID());
			purchase.setId(key);
			purchase.setQuantity(productSet.get(product));
			purchase.setPercentage(product.getPercentage());
			purchase.setFixedRate(product.getFixedRate());
			purchaseList.add(purchase);
			int amount = Integer.parseInt(product.getQuantity());
			amount -= productSet.get(product);
			productFinded.setQuantity(amount + "");
		}
		order.setPurchases(purchaseList);
		try {
			Connection connection = jmsFactory.createConnection();
			connection.start();
			Session session = connection.createSession(false,
					javax.jms.TopicSession.AUTO_ACKNOWLEDGE);
			ObjectMessage message = session.createObjectMessage();
			message.setObject(order);
			MessageProducer mp = session.createProducer(destination);
			mp.send(message);
			System.out.println(order);
			String ticket = tf.generateTicket(order.getDiscountedPrice());
			System.out.println(ticket);
			session.close();
			connection.close();
			return ticket;
			
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}
	
	public boolean addDiscount(String discountCode){
		EntityManager em = emf.createEntityManager();
		try{
		Discount discount = em.find(Discount.class, discountCode);
		if(discount==null||discount.getOrder()!=null){
			
			this.discount = null;
			System.out.println("false");
			return false;
		}
		String date = discountCode.substring(6, 14);
		System.out.println(date);
		Calendar today = Calendar.getInstance();
		Calendar expiry = Calendar.getInstance();
		int year = Integer.parseInt(date.substring(0, 4));
		int month = Integer.parseInt(date.substring(4, 6));
		int day = Integer.parseInt(date.substring(6,8));
		expiry.set(Calendar.DATE, day);
		expiry.set(Calendar.MONTH, month);
		expiry.set(Calendar.YEAR,year);
		//System.out.println(expiry.getTime());
		expiry.add(Calendar.MONTH, 1);
		if(today.compareTo(expiry)>0){
			this.discount = null;
			System.out.println("false");
			return false;
		}
		this.discount = discount;
		System.out.println("true");
		return true;
		} catch (Exception e){
			this.discount = null;
			System.out.println("false");
			return false;
		}
	}

	/*
	 * This function checks if the amount is exceeded, in that case throws a
	 * ProductAmoundExceededException passing the product that generated it as
	 * parameter.
	 */
	private void checkAmountInStorage(Productsupplied product)
			throws ProductAmountExceededException {

		if (Integer.parseInt(product.getQuantity()) < productSet.get(product)) {
			throw new ProductAmountExceededException(product);
		}

	}

	private void setupProperties() {
		properties = new Properties();
		/*
		 * properties.put(PersistenceUnitProperties.CLASSLOADER, this.getClass()
		 * .getClassLoader());
		 * 
		 * /*properties.put(PersistenceUnitProperties.CLASSLOADER,
		 * this.getClass().getClassLoader());
		 */
		properties.put(PersistenceUnitProperties.ECLIPSELINK_PERSISTENCE_UNITS,
				persistanceUnit);
		emf = Persistence.createEntityManagerFactory(persistanceUnit,
				properties);
	}
}
