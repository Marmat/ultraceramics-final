package utils;

import model.Productsupplied;

public class ProductAmountExceededException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Productsupplied product;
	
	public ProductAmountExceededException(Productsupplied product){
		this.product = product;
	}
	
	public Productsupplied getProductExceeded(){
		return this.product;
	}

}
