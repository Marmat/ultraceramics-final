package ejbModule;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;


import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import model.Discount;


@Stateless(mappedName="TicketEJB",name="TicketEJB")
public class TicketEJB implements TicketInterface {

	
	public TicketEJB(){
		
	}
	
	public String generateOrderCode(){
		Calendar cal = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMddhhmmSSSa");
    	String ticket = "ORDER"+dateFormat.format(cal.getTime());
    	return ticket;
	}
	
	public String generateTicket(double orderAmount) {
    	EntityManagerFactory emf;
    	Properties properties = new Properties();
		
		/* properties.put(PersistenceUnitProperties.CLASSLOADER, this.getClass()
		 .getClassLoader());
		 
		properties.put(PersistenceUnitProperties.CLASSLOADER,
		 this.getClass().getClassLoader());
		
		properties.put(PersistenceUnitProperties.ECLIPSELINK_PERSISTENCE_UNITS,
				"Model"); */
		emf = Persistence.createEntityManagerFactory("Model",
				properties);
    	Calendar cal = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMddhhmmSSSa");
    	String ticket = "TICKET"+dateFormat.format(cal.getTime());
    	EntityManager em = emf.createEntityManager();
		EntityTransaction et = em.getTransaction();
		et.begin();
		Discount discount = new Discount();
    	if(orderAmount>50){
			if(orderAmount>100){
				ticket +="20";
			} else {
				ticket +="10";
			}
			discount.setDiscountCode(ticket);
			em.persist(discount);
			et.commit();
			em.close();
			emf.close();
			return ticket;
    	} else {
    		em.close();
    		emf.close();
    		return null;
    	}
	}

}
