package ejbModule;

import javax.ejb.Local;
import javax.ejb.Remote;

@Local
public interface TicketInterface {
	public String generateTicket(double orderAmount);
	public String generateOrderCode();
}
